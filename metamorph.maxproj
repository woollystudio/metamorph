{
	"name" : "metamorph",
	"version" : 1,
	"creationdate" : 3758278988,
	"modificationdate" : 3765884438,
	"viewrect" : [ 21.0, 104.0, 300.0, 500.0 ],
	"autoorganize" : 0,
	"hideprojectwindow" : 0,
	"showdependencies" : 0,
	"autolocalize" : 1,
	"contents" : 	{
		"patchers" : 		{
			"metamorph.player.hoa.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"metamorph.player.directout.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"randomFile.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"metamorph.player.eau.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"metamorph.player.pack.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"metamorph.main.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"metamorph.scenario.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"metamorph.trajectories.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"metamorph.trajectories.session.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"metamorph.trajectory.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"metamorph.random.fades.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 1,
	"viewmode" : 0,
	"includepackages" : 0
}
