{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 2,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 389.0, 108.0, 679.0, 806.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 285.0, 106.0, 81.0, 22.0 ],
					"text" : "print scenario"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1423.0, 622.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 193.5, 700.0, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[24]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "Scenario",
					"varname" : "live.text[6]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1423.0, 646.0, 278.0, 22.0 ],
					"text" : "load Project:patchers/metamorph.scenario.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 813.5, 268.0, 150.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 191.5, 440.5, 102.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "PACK player",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 224.25, 301.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 191.5, 478.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[27]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "newbuffer",
					"varname" : "live.text[22]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 224.25, 352.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 191.5, 544.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[18]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "open",
					"varname" : "live.text[24]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 224.25, 336.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 191.5, 522.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[29]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "stop",
					"varname" : "live.text[25]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 224.25, 319.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 191.5, 500.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[31]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "start",
					"varname" : "live.text[27]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 224.25, 386.5, 59.0, 22.0 ],
					"style" : "default",
					"text" : "newlib $1"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.52156862745098, 0.250980392156863, 1.0 ],
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 193.25, 427.0, 133.0, 22.0 ],
					"text" : "metamorph.player.pack"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 37.0, 82.0, 87.0, 22.0 ],
					"text" : "r fromScenario"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 37.0, 58.0, 89.0, 22.0 ],
					"text" : "s fromScenario"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1066.5, 605.0, 167.0, 22.0 ],
					"text" : "sprintf \\\"Project:sessions/%s\\\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 815.0, 357.0, 581.0, 280.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 43.0, 76.0, 29.5, 22.0 ],
									"text" : "!= 1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-20",
									"linecount" : 3,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 102.0, 39.0, 303.0, 52.0 ],
									"text" : "mc.adc~ 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 43.0, 39.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 79.0, 212.5, 279.0, 21.0 ],
									"style" : "rnbodefault",
									"text" : "External input (Live) to Panoramix"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 43.0, 131.5, 78.0, 22.0 ],
									"text" : "mc.gate~ 1 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 5,
									"outlettype" : [ "multichannelsignal", "multichannelsignal", "", "", "" ],
									"patching_rect" : [ 43.0, 155.5, 166.0, 22.0 ],
									"text" : "mc.omx.peaklim~ @chans 49"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-31",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 43.0, 208.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "rnbodefault",
								"default" : 								{
									"accentcolor" : [ 0.343034118413925, 0.506230533123016, 0.86220508813858, 1.0 ],
									"bgcolor" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"autogradient" : 0.0,
										"color" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
										"color1" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
										"color2" : [ 0.263682, 0.004541, 0.038797, 1.0 ],
										"proportion" : 0.39,
										"type" : "color"
									}
,
									"color" : [ 0.929412, 0.929412, 0.352941, 1.0 ],
									"elementcolor" : [ 0.357540726661682, 0.515565991401672, 0.861786782741547, 1.0 ],
									"fontname" : [ "Lato" ],
									"fontsize" : [ 12.0 ],
									"stripecolor" : [ 0.258338063955307, 0.352425158023834, 0.511919498443604, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 524.0, 640.0, 69.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Ext_input"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Light",
					"id" : "obj-8",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 268.0, 149.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 56.5, 700.0, 101.0, 21.0 ],
					"text" : "Patchers",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1423.0, 677.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 116.5, 700.0, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[23]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "Trajectories",
					"varname" : "live.text[5]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1423.0, 701.0, 299.0, 22.0 ],
					"text" : "load Project:patchers/metamorph.trajectories.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1423.0, 825.0, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1310.5, 672.5, 70.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 252.0, 389.0, 73.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "Monitoring",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "live.toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1283.5, 670.0, 25.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 225.0, 386.5, 25.0, 25.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_longname" : "live.toggle[2]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.toggle",
							"parameter_type" : 2
						}

					}
,
					"varname" : "live.toggle[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1283.5, 701.0, 81.0, 22.0 ],
					"text" : "monitoring $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Light",
					"id" : "obj-21",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 268.0, 149.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.0, 631.0, 101.0, 21.0 ],
					"text" : "loaded in buffer",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 268.0, 150.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.0, 440.5, 102.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "MC Player",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 835.0, 427.0, 129.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.0, 653.0, 102.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 664.0, 375.5, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.0, 544.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[19]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "open",
					"varname" : "live.text[16]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 664.0, 328.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.0, 478.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[20]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "newbuffer",
					"varname" : "live.text[17]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 664.0, 360.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.0, 522.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[21]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "stop",
					"varname" : "live.text[18]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 664.0, 344.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.0, 500.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[22]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "start",
					"varname" : "live.text[19]"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.52156862745098, 0.250980392156863, 1.0 ],
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 664.0, 427.0, 154.0, 22.0 ],
					"text" : "metamorph.player.directout"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Light",
					"id" : "obj-6",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 268.0, 149.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 327.0, 631.0, 101.0, 21.0 ],
					"text" : "loaded in buffer",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 523.0, 427.0, 129.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 327.0, 653.0, 102.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 825.0, 81.0, 22.0 ],
					"text" : "s 2panoramix"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.415686274509804, 0.462745098039216, 1.0, 1.0 ],
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 434.0, 175.0, 1029.0, 782.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 62.0, 640.0, 114.0, 22.0 ],
									"text" : "mc.*~ 1 @chans 17"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 174.25, 512.0, 29.5, 22.0 ],
									"text" : "!= 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 252.0, 141.0, 95.0, 22.0 ],
									"text" : "route monitoring"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 396.0, 558.0, 107.0, 22.0 ],
									"text" : "mc.*~ 0 @chans 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 62.0, 558.0, 114.0, 22.0 ],
									"text" : "mc.*~ 1 @chans 17"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "multichannelsignal" ],
									"patching_rect" : [ 317.0, 512.0, 98.0, 22.0 ],
									"text" : "mc.separate~ 33"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 317.0, 64.0, 79.0, 22.0 ],
									"text" : "r 2panoramix"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 349.0, 689.0, 150.0, 20.0 ],
									"text" : "Audio to recorder"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 94.0, 689.0, 150.0, 20.0 ],
									"text" : "Audio to dac~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 398.0, 64.0, 150.0, 20.0 ],
									"text" : "Messages"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 94.0, 69.0, 150.0, 20.0 ],
									"text" : "Audio from players"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-4",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 317.0, 684.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 62.0, 684.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 62.0, 64.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "multichannelsignal" ],
									"patching_rect" : [ 62.0, 512.0, 98.0, 22.0 ],
									"text" : "mc.separate~ 17"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 5,
									"outlettype" : [ "multichannelsignal", "multichannelsignal", "", "", "" ],
									"patching_rect" : [ 62.0, 376.0, 166.0, 22.0 ],
									"text" : "mc.omx.peaklim~ @chans 35"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 62.0, 312.0, 274.0, 22.0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"text" : "spat5.panoramix~ @inlets 49 @outlets 35 @mc 1"
								}

							}
, 							{
								"box" : 								{
									"data" : 									{
										"/options/name" : "Options",
										"/options/color" : [ 1.0, 0.0, 0.0, 1.0 ],
										"/options/annotation" : "",
										"/options/vumeters/rate" : 200.0,
										"/options/parallel/bus" : 1,
										"/options/parallel/tracks" : 1,
										"/options/lock" : 0,
										"/master/name" : "Master",
										"/master/color" : [ 0.0, 0.0, 0.0, 1.0 ],
										"/master/numio" : [ 35, 35 ],
										"/master/visible" : 1,
										"/master/trim" : 0.0,
										"/master/dynamics/attack" : 10.010000228881836,
										"/master/dynamics/release" : 30.0,
										"/master/dynamics/lookahead" : 0.0,
										"/master/dynamics/compressor/threshold" : -30.0,
										"/master/dynamics/compressor/ratio" : 1.0,
										"/master/dynamics/expander/threshold" : -60.0,
										"/master/dynamics/expander/ratio" : 1.0,
										"/master/dynamics/makeup" : 0.0,
										"/master/dynamics/link" : "multi mono",
										"/master/dynamics/bypass" : 1,
										"/master/dynamics/grid/visible" : 1,
										"/master/dynamics/curve/fill" : 1,
										"/master/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/master/dynamics/foreground/color" : [ 0.0, 0.0, 0.0, 0.600000023841858 ],
										"/master/dynamics/curve/color" : [ 0.0, 0.0, 0.0, 1.0 ],
										"/master/dynamics/curve/thickness" : 1.0,
										"/master/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/master/dynamics/title" : "Master",
										"/master/equalizer/bypass" : 1,
										"/master/equalizer/gain" : 0.0,
										"/master/equalizer/filter/1/active" : 0,
										"/master/equalizer/filter/1/freq" : 50.0,
										"/master/equalizer/filter/1/order" : 2,
										"/master/equalizer/filter/2/active" : 1,
										"/master/equalizer/filter/2/freq" : 100.0,
										"/master/equalizer/filter/2/gain" : 0.0,
										"/master/equalizer/filter/2/q" : 1.0,
										"/master/equalizer/filter/3/active" : 1,
										"/master/equalizer/filter/3/freq" : 500.0,
										"/master/equalizer/filter/3/gain" : 0.0,
										"/master/equalizer/filter/3/q" : 1.0,
										"/master/equalizer/filter/4/active" : 1,
										"/master/equalizer/filter/4/freq" : 1000.0,
										"/master/equalizer/filter/4/gain" : 0.0,
										"/master/equalizer/filter/4/q" : 1.0,
										"/master/equalizer/filter/5/active" : 1,
										"/master/equalizer/filter/5/freq" : 2000.0,
										"/master/equalizer/filter/5/gain" : 0.0,
										"/master/equalizer/filter/5/q" : 1.0,
										"/master/equalizer/filter/6/active" : 1,
										"/master/equalizer/filter/6/freq" : 5000.0,
										"/master/equalizer/filter/6/gain" : 0.0,
										"/master/equalizer/filter/6/q" : 1.0,
										"/master/equalizer/filter/7/active" : 1,
										"/master/equalizer/filter/7/freq" : 10000.0,
										"/master/equalizer/filter/7/gain" : 0.0,
										"/master/equalizer/filter/7/q" : 1.0,
										"/master/equalizer/filter/8/active" : 0,
										"/master/equalizer/filter/8/freq" : 16000.0,
										"/master/equalizer/filter/8/order" : 2,
										"/master/gain" : 0.0,
										"/master/gain/ramptime" : 20.0,
										"/master/mute" : 0,
										"/master/annotation" : "",
										"/master/levels/input/visible" : 1,
										"/master/levels/input/post" : 0,
										"/master/levels/input/mode" : "peak",
										"/master/levels/output/visible" : 1,
										"/master/levels/output/post" : 1,
										"/master/levels/output/mode" : "peak",
										"/master/channel/1/equalizer/bypass" : 1,
										"/master/channel/1/equalizer/gain" : 0.0,
										"/master/channel/1/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/2/equalizer/bypass" : 1,
										"/master/channel/2/equalizer/gain" : 0.0,
										"/master/channel/2/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/3/equalizer/bypass" : 1,
										"/master/channel/3/equalizer/gain" : 0.0,
										"/master/channel/3/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/4/equalizer/bypass" : 1,
										"/master/channel/4/equalizer/gain" : 0.0,
										"/master/channel/4/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/5/equalizer/bypass" : 1,
										"/master/channel/5/equalizer/gain" : 0.0,
										"/master/channel/5/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/6/equalizer/bypass" : 1,
										"/master/channel/6/equalizer/gain" : 0.0,
										"/master/channel/6/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/7/equalizer/bypass" : 1,
										"/master/channel/7/equalizer/gain" : 0.0,
										"/master/channel/7/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/8/equalizer/bypass" : 1,
										"/master/channel/8/equalizer/gain" : 0.0,
										"/master/channel/8/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/9/equalizer/bypass" : 1,
										"/master/channel/9/equalizer/gain" : 0.0,
										"/master/channel/9/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/10/equalizer/bypass" : 1,
										"/master/channel/10/equalizer/gain" : 0.0,
										"/master/channel/10/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/11/equalizer/bypass" : 1,
										"/master/channel/11/equalizer/gain" : 0.0,
										"/master/channel/11/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/12/equalizer/bypass" : 1,
										"/master/channel/12/equalizer/gain" : 0.0,
										"/master/channel/12/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/13/equalizer/bypass" : 1,
										"/master/channel/13/equalizer/gain" : 0.0,
										"/master/channel/13/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/14/equalizer/bypass" : 1,
										"/master/channel/14/equalizer/gain" : 0.0,
										"/master/channel/14/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/15/equalizer/bypass" : 1,
										"/master/channel/15/equalizer/gain" : 0.0,
										"/master/channel/15/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/16/equalizer/bypass" : 1,
										"/master/channel/16/equalizer/gain" : 0.0,
										"/master/channel/16/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/17/equalizer/bypass" : 1,
										"/master/channel/17/equalizer/gain" : 0.0,
										"/master/channel/17/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/18/equalizer/bypass" : 1,
										"/master/channel/18/equalizer/gain" : 0.0,
										"/master/channel/18/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/19/equalizer/bypass" : 1,
										"/master/channel/19/equalizer/gain" : 0.0,
										"/master/channel/19/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/20/equalizer/bypass" : 1,
										"/master/channel/20/equalizer/gain" : 0.0,
										"/master/channel/20/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/21/equalizer/bypass" : 1,
										"/master/channel/21/equalizer/gain" : 0.0,
										"/master/channel/21/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/22/equalizer/bypass" : 1,
										"/master/channel/22/equalizer/gain" : 0.0,
										"/master/channel/22/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/23/equalizer/bypass" : 1,
										"/master/channel/23/equalizer/gain" : 0.0,
										"/master/channel/23/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/24/equalizer/bypass" : 1,
										"/master/channel/24/equalizer/gain" : 0.0,
										"/master/channel/24/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/25/equalizer/bypass" : 1,
										"/master/channel/25/equalizer/gain" : 0.0,
										"/master/channel/25/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/26/equalizer/bypass" : 1,
										"/master/channel/26/equalizer/gain" : 0.0,
										"/master/channel/26/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/27/equalizer/bypass" : 1,
										"/master/channel/27/equalizer/gain" : 0.0,
										"/master/channel/27/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/28/equalizer/bypass" : 1,
										"/master/channel/28/equalizer/gain" : 0.0,
										"/master/channel/28/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/29/equalizer/bypass" : 1,
										"/master/channel/29/equalizer/gain" : 0.0,
										"/master/channel/29/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/30/equalizer/bypass" : 1,
										"/master/channel/30/equalizer/gain" : 0.0,
										"/master/channel/30/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/31/equalizer/bypass" : 1,
										"/master/channel/31/equalizer/gain" : 0.0,
										"/master/channel/31/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/32/equalizer/bypass" : 1,
										"/master/channel/32/equalizer/gain" : 0.0,
										"/master/channel/32/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/33/equalizer/bypass" : 1,
										"/master/channel/33/equalizer/gain" : 0.0,
										"/master/channel/33/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/34/equalizer/bypass" : 1,
										"/master/channel/34/equalizer/gain" : 0.0,
										"/master/channel/34/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/channel/35/equalizer/bypass" : 1,
										"/master/channel/35/equalizer/gain" : 0.0,
										"/master/channel/35/equalizer/filters/params" : [ 0, 50.0, 2, 1, 100.0, 0.0, 1.0, 1, 500.0, 0.0, 1.0, 1, 1000.0, 0.0, 1.0, 1, 2000.0, 0.0, 1.0, 1, 5000.0, 0.0, 1.0, 1, 10000.0, 0.0, 1.0, 0, 16000.0, 2 ],
										"/master/output/mode" : "everything",
										"/master/dim" : 0,
										"/master/lock" : 0,
										"/monitoring/name" : "Monitoring",
										"/monitoring/color" : [ 1.0, 1.0, 1.0, 1.0 ],
										"/monitoring/numio" : [ 2, 2 ],
										"/monitoring/visible" : 1,
										"/monitoring/trim" : 0.0,
										"/monitoring/dynamics/attack" : 10.010000228881836,
										"/monitoring/dynamics/release" : 30.0,
										"/monitoring/dynamics/lookahead" : 0.0,
										"/monitoring/dynamics/compressor/threshold" : -30.0,
										"/monitoring/dynamics/compressor/ratio" : 1.0,
										"/monitoring/dynamics/expander/threshold" : -60.0,
										"/monitoring/dynamics/expander/ratio" : 1.0,
										"/monitoring/dynamics/makeup" : 0.0,
										"/monitoring/dynamics/link" : "multi mono",
										"/monitoring/dynamics/bypass" : 1,
										"/monitoring/dynamics/grid/visible" : 1,
										"/monitoring/dynamics/curve/fill" : 1,
										"/monitoring/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/monitoring/dynamics/foreground/color" : [ 1.0, 1.0, 1.0, 0.600000023841858 ],
										"/monitoring/dynamics/curve/color" : [ 0.800000011920929, 0.800000011920929, 0.800000011920929, 1.0 ],
										"/monitoring/dynamics/curve/thickness" : 1.0,
										"/monitoring/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/monitoring/dynamics/title" : "Monitoring",
										"/monitoring/equalizer/bypass" : 1,
										"/monitoring/equalizer/gain" : 0.0,
										"/monitoring/equalizer/filter/1/active" : 0,
										"/monitoring/equalizer/filter/1/freq" : 50.0,
										"/monitoring/equalizer/filter/1/order" : 2,
										"/monitoring/equalizer/filter/2/active" : 1,
										"/monitoring/equalizer/filter/2/freq" : 100.0,
										"/monitoring/equalizer/filter/2/gain" : 0.0,
										"/monitoring/equalizer/filter/2/q" : 1.0,
										"/monitoring/equalizer/filter/3/active" : 1,
										"/monitoring/equalizer/filter/3/freq" : 500.0,
										"/monitoring/equalizer/filter/3/gain" : 0.0,
										"/monitoring/equalizer/filter/3/q" : 1.0,
										"/monitoring/equalizer/filter/4/active" : 1,
										"/monitoring/equalizer/filter/4/freq" : 1000.0,
										"/monitoring/equalizer/filter/4/gain" : 0.0,
										"/monitoring/equalizer/filter/4/q" : 1.0,
										"/monitoring/equalizer/filter/5/active" : 1,
										"/monitoring/equalizer/filter/5/freq" : 2000.0,
										"/monitoring/equalizer/filter/5/gain" : 0.0,
										"/monitoring/equalizer/filter/5/q" : 1.0,
										"/monitoring/equalizer/filter/6/active" : 1,
										"/monitoring/equalizer/filter/6/freq" : 5000.0,
										"/monitoring/equalizer/filter/6/gain" : 0.0,
										"/monitoring/equalizer/filter/6/q" : 1.0,
										"/monitoring/equalizer/filter/7/active" : 1,
										"/monitoring/equalizer/filter/7/freq" : 10000.0,
										"/monitoring/equalizer/filter/7/gain" : 0.0,
										"/monitoring/equalizer/filter/7/q" : 1.0,
										"/monitoring/equalizer/filter/8/active" : 0,
										"/monitoring/equalizer/filter/8/freq" : 16000.0,
										"/monitoring/equalizer/filter/8/order" : 2,
										"/monitoring/gain" : 0.0,
										"/monitoring/gain/ramptime" : 20.0,
										"/monitoring/mute" : 0,
										"/monitoring/annotation" : "",
										"/monitoring/levels/input/visible" : 1,
										"/monitoring/levels/input/post" : 0,
										"/monitoring/levels/input/mode" : "peak",
										"/monitoring/levels/output/visible" : 1,
										"/monitoring/levels/output/post" : 1,
										"/monitoring/levels/output/mode" : "peak",
										"/monitoring/dim" : 0,
										"/monitoring/itd/scaling" : 100.0,
										"/monitoring/itd/scaling/bypass" : 0,
										"/monitoring/hrtf" : "",
										"/monitoring/listener/orientation" : [ 0.0, 0.0, 0.0, 1.0 ],
										"/monitoring/routing/output/1/master" : 34,
										"/monitoring/routing/output/2/master" : 35,
										"/monitoring/lock" : 0,
										"/bus/indices" : 1,
										"/bus/1/format" : "HOA",
										"/bus/1/name" : "HOA Bus 1",
										"/bus/1/color" : [ 0.803921580314636, 0.521568655967712, 0.24705882370472, 1.0 ],
										"/bus/1/numio" : [ 16, 16 ],
										"/bus/1/visible" : 1,
										"/bus/1/trim" : 0.0,
										"/bus/1/dynamics/attack" : 10.010000228881836,
										"/bus/1/dynamics/release" : 30.0,
										"/bus/1/dynamics/lookahead" : 0.0,
										"/bus/1/dynamics/compressor/threshold" : -30.0,
										"/bus/1/dynamics/compressor/ratio" : 1.0,
										"/bus/1/dynamics/expander/threshold" : -60.0,
										"/bus/1/dynamics/expander/ratio" : 1.0,
										"/bus/1/dynamics/makeup" : 0.0,
										"/bus/1/dynamics/link" : "multi mono",
										"/bus/1/dynamics/bypass" : 1,
										"/bus/1/dynamics/grid/visible" : 1,
										"/bus/1/dynamics/curve/fill" : 1,
										"/bus/1/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/bus/1/dynamics/foreground/color" : [ 0.803921580314636, 0.521568655967712, 0.24705882370472, 0.600000023841858 ],
										"/bus/1/dynamics/curve/color" : [ 0.643137276172638, 0.415686279535294, 0.196078434586525, 1.0 ],
										"/bus/1/dynamics/curve/thickness" : 1.0,
										"/bus/1/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/bus/1/dynamics/title" : "HOA Bus 1",
										"/bus/1/equalizer/bypass" : 1,
										"/bus/1/equalizer/gain" : 0.0,
										"/bus/1/equalizer/filter/1/active" : 0,
										"/bus/1/equalizer/filter/1/freq" : 50.0,
										"/bus/1/equalizer/filter/1/order" : 2,
										"/bus/1/equalizer/filter/2/active" : 1,
										"/bus/1/equalizer/filter/2/freq" : 100.0,
										"/bus/1/equalizer/filter/2/gain" : 0.0,
										"/bus/1/equalizer/filter/2/q" : 1.0,
										"/bus/1/equalizer/filter/3/active" : 1,
										"/bus/1/equalizer/filter/3/freq" : 500.0,
										"/bus/1/equalizer/filter/3/gain" : 0.0,
										"/bus/1/equalizer/filter/3/q" : 1.0,
										"/bus/1/equalizer/filter/4/active" : 1,
										"/bus/1/equalizer/filter/4/freq" : 1000.0,
										"/bus/1/equalizer/filter/4/gain" : 0.0,
										"/bus/1/equalizer/filter/4/q" : 1.0,
										"/bus/1/equalizer/filter/5/active" : 1,
										"/bus/1/equalizer/filter/5/freq" : 2000.0,
										"/bus/1/equalizer/filter/5/gain" : 0.0,
										"/bus/1/equalizer/filter/5/q" : 1.0,
										"/bus/1/equalizer/filter/6/active" : 1,
										"/bus/1/equalizer/filter/6/freq" : 5000.0,
										"/bus/1/equalizer/filter/6/gain" : 0.0,
										"/bus/1/equalizer/filter/6/q" : 1.0,
										"/bus/1/equalizer/filter/7/active" : 1,
										"/bus/1/equalizer/filter/7/freq" : 10000.0,
										"/bus/1/equalizer/filter/7/gain" : 0.0,
										"/bus/1/equalizer/filter/7/q" : 1.0,
										"/bus/1/equalizer/filter/8/active" : 0,
										"/bus/1/equalizer/filter/8/freq" : 16000.0,
										"/bus/1/equalizer/filter/8/order" : 2,
										"/bus/1/gain" : -9.0,
										"/bus/1/gain/ramptime" : 20.0,
										"/bus/1/mute" : 0,
										"/bus/1/annotation" : "",
										"/bus/1/levels/input/visible" : 1,
										"/bus/1/levels/input/post" : 0,
										"/bus/1/levels/input/mode" : "peak",
										"/bus/1/levels/output/visible" : 1,
										"/bus/1/levels/output/post" : 1,
										"/bus/1/levels/output/mode" : "peak",
										"/bus/1/speaker/number" : 16,
										"/bus/1/speakers/correction/delay" : "manual",
										"/bus/1/speakers/correction/gain" : "auto",
										"/bus/1/speakers/aed" : [ -28.810800552368164, -3.357000112533569, 8.231280326843262, 28.810800552368164, -3.343100070953369, 8.231160163879395, -58.781600952148438, -5.918159961700439, 4.655320167541504, 58.781600952148438, -5.918159961700439, 4.655320167541504, -121.218002319335938, -5.918159961700439, 4.655320167541504, 121.218002319335938, -5.918159961700439, 4.655320167541504, -151.188995361328125, -3.343100070953369, 8.231160163879395, 151.188995361328125, -3.343100070953369, 8.231160163879395, -15.376299858093262, 18.854499816894531, 7.890689849853516, 15.376299858093262, 18.854499816894531, 7.890689849853516, -39.522598266601562, 39.337501525878906, 4.022799968719482, 39.522598266601562, 39.337501525878906, 4.022799968719482, -140.477005004882812, 39.337501525878906, 4.022799968719482, 140.477005004882812, 39.337501525878906, 4.022799968719482, -164.623992919921875, 18.854499816894531, 7.890689849853516, 164.623992919921875, 18.854499816894531, 7.890689849853516 ],
										"/bus/1/speaker/1/delay" : 0.140000000596046,
										"/bus/1/speaker/2/delay" : 0.000399999989895,
										"/bus/1/speaker/3/delay" : 8.520000457763672,
										"/bus/1/speaker/4/delay" : 8.140000343322754,
										"/bus/1/speaker/5/delay" : 8.699999809265137,
										"/bus/1/speaker/6/delay" : 8.270000457763672,
										"/bus/1/speaker/7/delay" : 0.409999996423721,
										"/bus/1/speaker/8/delay" : 0.219999998807907,
										"/bus/1/speaker/9/delay" : 1.100000023841858,
										"/bus/1/speaker/10/delay" : 1.039999961853027,
										"/bus/1/speaker/11/delay" : 10.119999885559082,
										"/bus/1/speaker/12/delay" : 9.930000305175781,
										"/bus/1/speaker/13/delay" : 10.270000457763672,
										"/bus/1/speaker/14/delay" : 10.159999847412109,
										"/bus/1/speaker/15/delay" : 1.370000004768372,
										"/bus/1/speaker/16/delay" : 1.269999980926514,
										"/bus/1/speaker/1/gain/db" : 0.0,
										"/bus/1/speaker/2/gain/db" : -0.0,
										"/bus/1/speaker/3/gain/db" : -4.949999809265137,
										"/bus/1/speaker/4/gain/db" : -4.949999809265137,
										"/bus/1/speaker/5/gain/db" : -4.949999809265137,
										"/bus/1/speaker/6/gain/db" : -4.949999809265137,
										"/bus/1/speaker/7/gain/db" : -0.0,
										"/bus/1/speaker/8/gain/db" : -0.0,
										"/bus/1/speaker/9/gain/db" : -0.370000004768372,
										"/bus/1/speaker/10/gain/db" : -0.370000004768372,
										"/bus/1/speaker/11/gain/db" : -6.21999979019165,
										"/bus/1/speaker/12/gain/db" : -6.21999979019165,
										"/bus/1/speaker/13/gain/db" : -6.21999979019165,
										"/bus/1/speaker/14/gain/db" : -6.21999979019165,
										"/bus/1/speaker/15/gain/db" : -0.370000004768372,
										"/bus/1/speaker/16/gain/db" : -0.370000004768372,
										"/bus/1/speaker/1/name" : "1",
										"/bus/1/speaker/2/name" : "2",
										"/bus/1/speaker/3/name" : "3",
										"/bus/1/speaker/4/name" : "4",
										"/bus/1/speaker/5/name" : "5",
										"/bus/1/speaker/6/name" : "6",
										"/bus/1/speaker/7/name" : "7",
										"/bus/1/speaker/8/name" : "8",
										"/bus/1/speaker/9/name" : "9",
										"/bus/1/speaker/10/name" : "10",
										"/bus/1/speaker/11/name" : "11",
										"/bus/1/speaker/12/name" : "12",
										"/bus/1/speaker/13/name" : "13",
										"/bus/1/speaker/14/name" : "14",
										"/bus/1/speaker/15/name" : "15",
										"/bus/1/speaker/16/name" : "16",
										"/bus/1/speakers/visible" : 0,
										"/bus/1/routing/output/1/master" : 1,
										"/bus/1/routing/output/2/master" : 2,
										"/bus/1/routing/output/3/master" : 3,
										"/bus/1/routing/output/4/master" : 4,
										"/bus/1/routing/output/5/master" : 5,
										"/bus/1/routing/output/6/master" : 6,
										"/bus/1/routing/output/7/master" : 7,
										"/bus/1/routing/output/8/master" : 8,
										"/bus/1/routing/output/9/master" : 9,
										"/bus/1/routing/output/10/master" : 10,
										"/bus/1/routing/output/11/master" : 11,
										"/bus/1/routing/output/12/master" : 12,
										"/bus/1/routing/output/13/master" : 13,
										"/bus/1/routing/output/14/master" : 14,
										"/bus/1/routing/output/15/master" : 15,
										"/bus/1/routing/output/16/master" : 16,
										"/bus/1/lock" : 0,
										"/bus/1/delay" : 0.0,
										"/bus/1/delay/bypass" : 0,
										"/bus/1/monitor" : 1,
										"/bus/1/interpolation/time" : 30.0,
										"/bus/1/decoder/type" : "basic",
										"/bus/1/decoder/method" : "energy-preserving",
										"/bus/1/decoder/norm" : "N3D",
										"/bus/1/decoder/crossover" : 700.0,
										"/bus/1/decoder/powercompensation" : 0,
										"/bus/1/yaw" : 0.0,
										"/bus/1/pitch" : 0.0,
										"/bus/1/roll" : 0.0,
										"/bus/1/blur/bypass" : 1,
										"/bus/1/blur" : 0.0,
										"/bus/1/focus/beam/number" : 1,
										"/bus/1/focus/dimension" : 3,
										"/bus/1/focus/zoom" : 142.85699462890625,
										"/bus/1/focus/dsp/bypass" : 1,
										"/bus/1/focus/background/color" : [ 0.82745099067688, 0.82745099067688, 0.82745099067688, 1.0 ],
										"/bus/1/focus/zoom/lock" : 0,
										"/bus/1/focus/beam/1/selectivity" : 0.0,
										"/bus/1/focus/beam/1/ae" : [ 0.0, 0.0 ],
										"/bus/1/focus/beam/1/color" : [ 1.0, 1.0, 1.0, 1.0 ],
										"/bus/1/focus/beam/1/visible" : 0,
										"/bus/1/decoder/phantom/zenith" : 0,
										"/bus/1/decoder/phantom/nadir" : 0,
										"/bus/1/routing/stream/1/master" : 18,
										"/bus/1/routing/stream/2/master" : 19,
										"/bus/1/routing/stream/3/master" : 20,
										"/bus/1/routing/stream/4/master" : 21,
										"/bus/1/routing/stream/5/master" : 22,
										"/bus/1/routing/stream/6/master" : 23,
										"/bus/1/routing/stream/7/master" : 24,
										"/bus/1/routing/stream/8/master" : 25,
										"/bus/1/routing/stream/9/master" : 26,
										"/bus/1/routing/stream/10/master" : 27,
										"/bus/1/routing/stream/11/master" : 28,
										"/bus/1/routing/stream/12/master" : 29,
										"/bus/1/routing/stream/13/master" : 30,
										"/bus/1/routing/stream/14/master" : 31,
										"/bus/1/routing/stream/15/master" : 32,
										"/bus/1/routing/stream/16/master" : 33,
										"/reverb/1/name" : "HOA Reverb 1",
										"/reverb/1/color" : [ 0.803921580314636, 0.521568655967712, 0.24705882370472, 1.0 ],
										"/reverb/1/numio" : [ 8, 16 ],
										"/reverb/1/visible" : 1,
										"/reverb/1/trim" : 0.0,
										"/reverb/1/dynamics/attack" : 10.010000228881836,
										"/reverb/1/dynamics/release" : 30.0,
										"/reverb/1/dynamics/lookahead" : 0.0,
										"/reverb/1/dynamics/compressor/threshold" : -30.0,
										"/reverb/1/dynamics/compressor/ratio" : 1.0,
										"/reverb/1/dynamics/expander/threshold" : -60.0,
										"/reverb/1/dynamics/expander/ratio" : 1.0,
										"/reverb/1/dynamics/makeup" : 0.0,
										"/reverb/1/dynamics/link" : "multi mono",
										"/reverb/1/dynamics/bypass" : 1,
										"/reverb/1/dynamics/grid/visible" : 1,
										"/reverb/1/dynamics/curve/fill" : 1,
										"/reverb/1/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/reverb/1/dynamics/foreground/color" : [ 0.803921580314636, 0.521568655967712, 0.24705882370472, 0.600000023841858 ],
										"/reverb/1/dynamics/curve/color" : [ 0.643137276172638, 0.415686279535294, 0.196078434586525, 1.0 ],
										"/reverb/1/dynamics/curve/thickness" : 1.0,
										"/reverb/1/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/reverb/1/dynamics/title" : "HOA Reverb 1",
										"/reverb/1/equalizer/bypass" : 1,
										"/reverb/1/equalizer/gain" : 0.0,
										"/reverb/1/equalizer/filter/1/active" : 0,
										"/reverb/1/equalizer/filter/1/freq" : 50.0,
										"/reverb/1/equalizer/filter/1/order" : 2,
										"/reverb/1/equalizer/filter/2/active" : 1,
										"/reverb/1/equalizer/filter/2/freq" : 100.0,
										"/reverb/1/equalizer/filter/2/gain" : 0.0,
										"/reverb/1/equalizer/filter/2/q" : 1.0,
										"/reverb/1/equalizer/filter/3/active" : 1,
										"/reverb/1/equalizer/filter/3/freq" : 500.0,
										"/reverb/1/equalizer/filter/3/gain" : 0.0,
										"/reverb/1/equalizer/filter/3/q" : 1.0,
										"/reverb/1/equalizer/filter/4/active" : 1,
										"/reverb/1/equalizer/filter/4/freq" : 1000.0,
										"/reverb/1/equalizer/filter/4/gain" : 0.0,
										"/reverb/1/equalizer/filter/4/q" : 1.0,
										"/reverb/1/equalizer/filter/5/active" : 1,
										"/reverb/1/equalizer/filter/5/freq" : 2000.0,
										"/reverb/1/equalizer/filter/5/gain" : 0.0,
										"/reverb/1/equalizer/filter/5/q" : 1.0,
										"/reverb/1/equalizer/filter/6/active" : 1,
										"/reverb/1/equalizer/filter/6/freq" : 5000.0,
										"/reverb/1/equalizer/filter/6/gain" : 0.0,
										"/reverb/1/equalizer/filter/6/q" : 1.0,
										"/reverb/1/equalizer/filter/7/active" : 1,
										"/reverb/1/equalizer/filter/7/freq" : 10000.0,
										"/reverb/1/equalizer/filter/7/gain" : 0.0,
										"/reverb/1/equalizer/filter/7/q" : 1.0,
										"/reverb/1/equalizer/filter/8/active" : 0,
										"/reverb/1/equalizer/filter/8/freq" : 16000.0,
										"/reverb/1/equalizer/filter/8/order" : 2,
										"/reverb/1/gain" : -18.0,
										"/reverb/1/gain/ramptime" : 20.0,
										"/reverb/1/mute" : 0,
										"/reverb/1/annotation" : "",
										"/reverb/1/levels/input/visible" : 1,
										"/reverb/1/levels/input/post" : 0,
										"/reverb/1/levels/input/mode" : "peak",
										"/reverb/1/levels/output/visible" : 1,
										"/reverb/1/levels/output/post" : 1,
										"/reverb/1/levels/output/mode" : "peak",
										"/reverb/1/lock" : 0,
										"/reverb/1/cluster/gain" : -6.0,
										"/reverb/1/cluster/filter/bypass" : 1,
										"/reverb/1/cluster/gain/low" : 0.0,
										"/reverb/1/cluster/gain/med" : 0.0,
										"/reverb/1/cluster/gain/high" : 0.0,
										"/reverb/1/cluster/freq/low" : 250.0,
										"/reverb/1/cluster/freq/high" : 4000.0,
										"/reverb/1/cluster/mute" : 0,
										"/reverb/1/cluster/delays" : [ 20.25, 31.363300323486328, 36.199901580810547, 44.422298431396484, 56.562801361083984, 64.264297485351562, 72.826698303222656, 83.419998168945312 ],
										"/reverb/1/tr0" : 1.995000004768372,
										"/reverb/1/trl" : 1.0,
										"/reverb/1/trm" : 1.0,
										"/reverb/1/trh" : 0.5,
										"/reverb/1/fl" : 250.0,
										"/reverb/1/fh" : 8000.0,
										"/reverb/1/air" : 1,
										"/reverb/1/reverb/delays" : [ 52.900001525878906, 72.111099243164062, 80.472099304199219, 94.685798645019531, 115.672996520996094, 128.985992431640625, 143.787994384765625, 162.100006103515625 ],
										"/lfe/name" : "LFE",
										"/lfe/color" : [ 1.0, 1.0, 0.0, 1.0 ],
										"/lfe/numio" : [ 1, 1 ],
										"/lfe/visible" : 1,
										"/lfe/trim" : 0.0,
										"/lfe/dynamics/attack" : 10.010000228881836,
										"/lfe/dynamics/release" : 30.0,
										"/lfe/dynamics/lookahead" : 0.0,
										"/lfe/dynamics/compressor/threshold" : -30.0,
										"/lfe/dynamics/compressor/ratio" : 1.0,
										"/lfe/dynamics/expander/threshold" : -60.0,
										"/lfe/dynamics/expander/ratio" : 1.0,
										"/lfe/dynamics/makeup" : 0.0,
										"/lfe/dynamics/link" : "multi mono",
										"/lfe/dynamics/bypass" : 1,
										"/lfe/dynamics/grid/visible" : 1,
										"/lfe/dynamics/curve/fill" : 1,
										"/lfe/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/lfe/dynamics/foreground/color" : [ 1.0, 1.0, 0.0, 0.600000023841858 ],
										"/lfe/dynamics/curve/color" : [ 0.800000011920929, 0.800000011920929, 0.0, 1.0 ],
										"/lfe/dynamics/curve/thickness" : 1.0,
										"/lfe/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/lfe/dynamics/title" : "LFE",
										"/lfe/equalizer/bypass" : 1,
										"/lfe/equalizer/gain" : 0.0,
										"/lfe/equalizer/filter/1/active" : 0,
										"/lfe/equalizer/filter/1/freq" : 50.0,
										"/lfe/equalizer/filter/1/order" : 2,
										"/lfe/equalizer/filter/2/active" : 1,
										"/lfe/equalizer/filter/2/freq" : 100.0,
										"/lfe/equalizer/filter/2/gain" : 0.0,
										"/lfe/equalizer/filter/2/q" : 1.0,
										"/lfe/equalizer/filter/3/active" : 1,
										"/lfe/equalizer/filter/3/freq" : 500.0,
										"/lfe/equalizer/filter/3/gain" : 0.0,
										"/lfe/equalizer/filter/3/q" : 1.0,
										"/lfe/equalizer/filter/4/active" : 1,
										"/lfe/equalizer/filter/4/freq" : 1000.0,
										"/lfe/equalizer/filter/4/gain" : 0.0,
										"/lfe/equalizer/filter/4/q" : 1.0,
										"/lfe/equalizer/filter/5/active" : 1,
										"/lfe/equalizer/filter/5/freq" : 2000.0,
										"/lfe/equalizer/filter/5/gain" : 0.0,
										"/lfe/equalizer/filter/5/q" : 1.0,
										"/lfe/equalizer/filter/6/active" : 1,
										"/lfe/equalizer/filter/6/freq" : 5000.0,
										"/lfe/equalizer/filter/6/gain" : 0.0,
										"/lfe/equalizer/filter/6/q" : 1.0,
										"/lfe/equalizer/filter/7/active" : 1,
										"/lfe/equalizer/filter/7/freq" : 10000.0,
										"/lfe/equalizer/filter/7/gain" : 0.0,
										"/lfe/equalizer/filter/7/q" : 1.0,
										"/lfe/equalizer/filter/8/active" : 0,
										"/lfe/equalizer/filter/8/freq" : 16000.0,
										"/lfe/equalizer/filter/8/order" : 2,
										"/lfe/gain" : 0.0,
										"/lfe/gain/ramptime" : 20.0,
										"/lfe/mute" : 0,
										"/lfe/annotation" : "",
										"/lfe/levels/input/visible" : 1,
										"/lfe/levels/input/post" : 0,
										"/lfe/levels/input/mode" : "peak",
										"/lfe/levels/output/visible" : 1,
										"/lfe/levels/output/post" : 1,
										"/lfe/levels/output/mode" : "peak",
										"/lfe/routing/output/1/master" : 17,
										"/lfe/lock" : 0,
										"/lfe/delay" : 0.0,
										"/lfe/delay/bypass" : 0,
										"/lfe/cutoff" : 50.0,
										"/lfe/cutoff/bypass" : 0,
										"/track/indices" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ],
										"/stereo/indices" : [  ],
										"/multi/indices" : [  ],
										"/tree/indices" : [  ],
										"/hoastream/indices" : 1,
										"/eigenmike/indices" : [  ],
										"/zylia/indices" : [  ],
										"/bformat/indices" : [  ],
										"/aformat/indices" : [  ],
										"/d2m/indices" : 1,
										"/d2b/indices" : [  ],
										"/track/1/name" : "Mono 1",
										"/track/1/color" : [ 1.0, 0.541176497936249, 0.0, 1.0 ],
										"/track/1/numinputs" : 1,
										"/track/1/visible" : 1,
										"/track/1/trim" : 0.0,
										"/track/1/dynamics/attack" : 10.010000228881836,
										"/track/1/dynamics/release" : 30.0,
										"/track/1/dynamics/lookahead" : 0.0,
										"/track/1/dynamics/compressor/threshold" : -30.0,
										"/track/1/dynamics/compressor/ratio" : 1.0,
										"/track/1/dynamics/expander/threshold" : -60.0,
										"/track/1/dynamics/expander/ratio" : 1.0,
										"/track/1/dynamics/makeup" : 0.0,
										"/track/1/dynamics/link" : "multi mono",
										"/track/1/dynamics/bypass" : 1,
										"/track/1/dynamics/grid/visible" : 1,
										"/track/1/dynamics/curve/fill" : 1,
										"/track/1/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/1/dynamics/foreground/color" : [ 1.0, 0.541176497936249, 0.0, 0.600000023841858 ],
										"/track/1/dynamics/curve/color" : [ 0.800000011920929, 0.431372553110123, 0.0, 1.0 ],
										"/track/1/dynamics/curve/thickness" : 1.0,
										"/track/1/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/1/dynamics/title" : "Mono 1",
										"/track/1/equalizer/bypass" : 1,
										"/track/1/equalizer/gain" : 0.0,
										"/track/1/equalizer/filter/1/active" : 0,
										"/track/1/equalizer/filter/1/freq" : 50.0,
										"/track/1/equalizer/filter/1/order" : 2,
										"/track/1/equalizer/filter/2/active" : 1,
										"/track/1/equalizer/filter/2/freq" : 100.0,
										"/track/1/equalizer/filter/2/gain" : 0.0,
										"/track/1/equalizer/filter/2/q" : 1.0,
										"/track/1/equalizer/filter/3/active" : 1,
										"/track/1/equalizer/filter/3/freq" : 500.0,
										"/track/1/equalizer/filter/3/gain" : 0.0,
										"/track/1/equalizer/filter/3/q" : 1.0,
										"/track/1/equalizer/filter/4/active" : 1,
										"/track/1/equalizer/filter/4/freq" : 1000.0,
										"/track/1/equalizer/filter/4/gain" : 0.0,
										"/track/1/equalizer/filter/4/q" : 1.0,
										"/track/1/equalizer/filter/5/active" : 1,
										"/track/1/equalizer/filter/5/freq" : 2000.0,
										"/track/1/equalizer/filter/5/gain" : 0.0,
										"/track/1/equalizer/filter/5/q" : 1.0,
										"/track/1/equalizer/filter/6/active" : 1,
										"/track/1/equalizer/filter/6/freq" : 5000.0,
										"/track/1/equalizer/filter/6/gain" : 0.0,
										"/track/1/equalizer/filter/6/q" : 1.0,
										"/track/1/equalizer/filter/7/active" : 1,
										"/track/1/equalizer/filter/7/freq" : 10000.0,
										"/track/1/equalizer/filter/7/gain" : 0.0,
										"/track/1/equalizer/filter/7/q" : 1.0,
										"/track/1/equalizer/filter/8/active" : 0,
										"/track/1/equalizer/filter/8/freq" : 16000.0,
										"/track/1/equalizer/filter/8/order" : 2,
										"/track/1/gain" : 0.0,
										"/track/1/gain/ramptime" : 20.0,
										"/track/1/mute" : 0,
										"/track/1/annotation" : "",
										"/track/1/levels/input/visible" : 1,
										"/track/1/levels/input/post" : 0,
										"/track/1/levels/input/mode" : "peak",
										"/track/1/levels/output/visible" : 1,
										"/track/1/levels/output/post" : 1,
										"/track/1/levels/output/mode" : "peak",
										"/track/1/solo" : 0,
										"/track/1/bus/A/destination" : "/bus/1",
										"/track/1/bus/B/destination" : "/",
										"/track/1/bus/C/destination" : "/",
										"/track/1/bus/D/destination" : "/",
										"/track/1/bus/E/destination" : "/",
										"/track/1/bus/F/destination" : "/",
										"/track/1/bus/A/mute" : 0,
										"/track/1/bus/B/mute" : 0,
										"/track/1/bus/C/mute" : 0,
										"/track/1/bus/D/mute" : 0,
										"/track/1/bus/E/mute" : 0,
										"/track/1/bus/F/mute" : 0,
										"/track/1/bus/A/gain" : 0.0,
										"/track/1/bus/B/gain" : 0.0,
										"/track/1/bus/C/gain" : 0.0,
										"/track/1/bus/D/gain" : 0.0,
										"/track/1/bus/E/gain" : 0.0,
										"/track/1/bus/F/gain" : 0.0,
										"/track/1/bus/display" : "A",
										"/track/1/routing/input/1/adc" : 1,
										"/track/1/lfe/send" : 0.0,
										"/track/1/lfe/mute" : 0,
										"/track/1/doppler" : 0,
										"/track/1/air" : 0,
										"/track/1/phaseinvert" : 0,
										"/track/1/azim" : 0.0,
										"/track/1/elev" : 0.0,
										"/track/1/dist" : 10.0,
										"/track/1/delay/linkedtodistance" : 0,
										"/track/1/direct/linkedtodistance" : 1,
										"/track/1/early/linkedtodistance" : 1,
										"/track/1/reverb/linkedtodistance" : 1,
										"/track/1/delay" : 0.0,
										"/track/1/direct/gain/offset" : 0.0,
										"/track/1/direct/gain" : -20.5,
										"/track/1/direct/gain/low" : 0.0,
										"/track/1/direct/gain/med" : 0.0,
										"/track/1/direct/gain/high" : 0.0,
										"/track/1/direct/freq/low" : 250.0,
										"/track/1/direct/freq/high" : 4000.0,
										"/track/1/direct/filter/bypass" : 1,
										"/track/1/early/gain/offset" : 0.0,
										"/track/1/early/gain" : -28.100000381469727,
										"/track/1/early/gain/low" : 0.0,
										"/track/1/early/gain/med" : 0.0,
										"/track/1/early/gain/high" : 0.0,
										"/track/1/early/freq/low" : 250.0,
										"/track/1/early/freq/high" : 4000.0,
										"/track/1/early/filter/bypass" : 1,
										"/track/1/reverb/send" : -19.399999618530273,
										"/track/1/reverb/send/offset" : 0.0,
										"/track/1/reverb/send/ramptime" : 20.0,
										"/track/1/early/width" : 30.0,
										"/track/1/direct/mute" : 0,
										"/track/1/early/mute" : 0,
										"/track/1/reverb/mute" : 0,
										"/track/1/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/1/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/1/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/1/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/1/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/1/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/1/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/1/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/1/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/1/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/1/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/1/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/1/lock" : 0,
										"/track/2/name" : "Mono 2",
										"/track/2/color" : [ 1.0, 0.541176497936249, 0.0, 1.0 ],
										"/track/2/numinputs" : 1,
										"/track/2/visible" : 1,
										"/track/2/trim" : 0.0,
										"/track/2/dynamics/attack" : 10.010000228881836,
										"/track/2/dynamics/release" : 30.0,
										"/track/2/dynamics/lookahead" : 0.0,
										"/track/2/dynamics/compressor/threshold" : -30.0,
										"/track/2/dynamics/compressor/ratio" : 1.0,
										"/track/2/dynamics/expander/threshold" : -60.0,
										"/track/2/dynamics/expander/ratio" : 1.0,
										"/track/2/dynamics/makeup" : 0.0,
										"/track/2/dynamics/link" : "multi mono",
										"/track/2/dynamics/bypass" : 1,
										"/track/2/dynamics/grid/visible" : 1,
										"/track/2/dynamics/curve/fill" : 1,
										"/track/2/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/2/dynamics/foreground/color" : [ 1.0, 0.541176497936249, 0.0, 0.600000023841858 ],
										"/track/2/dynamics/curve/color" : [ 0.800000011920929, 0.431372553110123, 0.0, 1.0 ],
										"/track/2/dynamics/curve/thickness" : 1.0,
										"/track/2/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/2/dynamics/title" : "Mono 2",
										"/track/2/equalizer/bypass" : 1,
										"/track/2/equalizer/gain" : 0.0,
										"/track/2/equalizer/filter/1/active" : 0,
										"/track/2/equalizer/filter/1/freq" : 50.0,
										"/track/2/equalizer/filter/1/order" : 2,
										"/track/2/equalizer/filter/2/active" : 1,
										"/track/2/equalizer/filter/2/freq" : 100.0,
										"/track/2/equalizer/filter/2/gain" : 0.0,
										"/track/2/equalizer/filter/2/q" : 1.0,
										"/track/2/equalizer/filter/3/active" : 1,
										"/track/2/equalizer/filter/3/freq" : 500.0,
										"/track/2/equalizer/filter/3/gain" : 0.0,
										"/track/2/equalizer/filter/3/q" : 1.0,
										"/track/2/equalizer/filter/4/active" : 1,
										"/track/2/equalizer/filter/4/freq" : 1000.0,
										"/track/2/equalizer/filter/4/gain" : 0.0,
										"/track/2/equalizer/filter/4/q" : 1.0,
										"/track/2/equalizer/filter/5/active" : 1,
										"/track/2/equalizer/filter/5/freq" : 2000.0,
										"/track/2/equalizer/filter/5/gain" : 0.0,
										"/track/2/equalizer/filter/5/q" : 1.0,
										"/track/2/equalizer/filter/6/active" : 1,
										"/track/2/equalizer/filter/6/freq" : 5000.0,
										"/track/2/equalizer/filter/6/gain" : 0.0,
										"/track/2/equalizer/filter/6/q" : 1.0,
										"/track/2/equalizer/filter/7/active" : 1,
										"/track/2/equalizer/filter/7/freq" : 10000.0,
										"/track/2/equalizer/filter/7/gain" : 0.0,
										"/track/2/equalizer/filter/7/q" : 1.0,
										"/track/2/equalizer/filter/8/active" : 0,
										"/track/2/equalizer/filter/8/freq" : 16000.0,
										"/track/2/equalizer/filter/8/order" : 2,
										"/track/2/gain" : 0.0,
										"/track/2/gain/ramptime" : 20.0,
										"/track/2/mute" : 0,
										"/track/2/annotation" : "",
										"/track/2/levels/input/visible" : 1,
										"/track/2/levels/input/post" : 0,
										"/track/2/levels/input/mode" : "peak",
										"/track/2/levels/output/visible" : 1,
										"/track/2/levels/output/post" : 1,
										"/track/2/levels/output/mode" : "peak",
										"/track/2/solo" : 0,
										"/track/2/bus/A/destination" : "/bus/1",
										"/track/2/bus/B/destination" : "/",
										"/track/2/bus/C/destination" : "/",
										"/track/2/bus/D/destination" : "/",
										"/track/2/bus/E/destination" : "/",
										"/track/2/bus/F/destination" : "/",
										"/track/2/bus/A/mute" : 0,
										"/track/2/bus/B/mute" : 0,
										"/track/2/bus/C/mute" : 0,
										"/track/2/bus/D/mute" : 0,
										"/track/2/bus/E/mute" : 0,
										"/track/2/bus/F/mute" : 0,
										"/track/2/bus/A/gain" : 0.0,
										"/track/2/bus/B/gain" : 0.0,
										"/track/2/bus/C/gain" : 0.0,
										"/track/2/bus/D/gain" : 0.0,
										"/track/2/bus/E/gain" : 0.0,
										"/track/2/bus/F/gain" : 0.0,
										"/track/2/bus/display" : "A",
										"/track/2/routing/input/1/adc" : 2,
										"/track/2/lfe/send" : 0.0,
										"/track/2/lfe/mute" : 0,
										"/track/2/doppler" : 0,
										"/track/2/air" : 0,
										"/track/2/phaseinvert" : 0,
										"/track/2/azim" : 45.0,
										"/track/2/elev" : 0.0,
										"/track/2/dist" : 10.0,
										"/track/2/delay/linkedtodistance" : 0,
										"/track/2/direct/linkedtodistance" : 1,
										"/track/2/early/linkedtodistance" : 1,
										"/track/2/reverb/linkedtodistance" : 1,
										"/track/2/delay" : 0.0,
										"/track/2/direct/gain/offset" : 0.0,
										"/track/2/direct/gain" : -20.5,
										"/track/2/direct/gain/low" : 0.0,
										"/track/2/direct/gain/med" : 0.0,
										"/track/2/direct/gain/high" : 0.0,
										"/track/2/direct/freq/low" : 250.0,
										"/track/2/direct/freq/high" : 4000.0,
										"/track/2/direct/filter/bypass" : 1,
										"/track/2/early/gain/offset" : 0.0,
										"/track/2/early/gain" : -28.100000381469727,
										"/track/2/early/gain/low" : 0.0,
										"/track/2/early/gain/med" : 0.0,
										"/track/2/early/gain/high" : 0.0,
										"/track/2/early/freq/low" : 250.0,
										"/track/2/early/freq/high" : 4000.0,
										"/track/2/early/filter/bypass" : 1,
										"/track/2/reverb/send" : -19.399999618530273,
										"/track/2/reverb/send/offset" : 0.0,
										"/track/2/reverb/send/ramptime" : 20.0,
										"/track/2/early/width" : 30.0,
										"/track/2/direct/mute" : 0,
										"/track/2/early/mute" : 0,
										"/track/2/reverb/mute" : 0,
										"/track/2/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/2/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/2/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/2/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/2/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/2/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/2/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/2/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/2/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/2/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/2/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/2/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/2/lock" : 0,
										"/track/3/name" : "Mono 3",
										"/track/3/color" : [ 1.0, 0.541176497936249, 0.0, 1.0 ],
										"/track/3/numinputs" : 1,
										"/track/3/visible" : 1,
										"/track/3/trim" : 0.0,
										"/track/3/dynamics/attack" : 10.010000228881836,
										"/track/3/dynamics/release" : 30.0,
										"/track/3/dynamics/lookahead" : 0.0,
										"/track/3/dynamics/compressor/threshold" : -30.0,
										"/track/3/dynamics/compressor/ratio" : 1.0,
										"/track/3/dynamics/expander/threshold" : -60.0,
										"/track/3/dynamics/expander/ratio" : 1.0,
										"/track/3/dynamics/makeup" : 0.0,
										"/track/3/dynamics/link" : "multi mono",
										"/track/3/dynamics/bypass" : 1,
										"/track/3/dynamics/grid/visible" : 1,
										"/track/3/dynamics/curve/fill" : 1,
										"/track/3/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/3/dynamics/foreground/color" : [ 1.0, 0.541176497936249, 0.0, 0.600000023841858 ],
										"/track/3/dynamics/curve/color" : [ 0.800000011920929, 0.431372553110123, 0.0, 1.0 ],
										"/track/3/dynamics/curve/thickness" : 1.0,
										"/track/3/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/3/dynamics/title" : "Mono 3",
										"/track/3/equalizer/bypass" : 1,
										"/track/3/equalizer/gain" : 0.0,
										"/track/3/equalizer/filter/1/active" : 0,
										"/track/3/equalizer/filter/1/freq" : 50.0,
										"/track/3/equalizer/filter/1/order" : 2,
										"/track/3/equalizer/filter/2/active" : 1,
										"/track/3/equalizer/filter/2/freq" : 100.0,
										"/track/3/equalizer/filter/2/gain" : 0.0,
										"/track/3/equalizer/filter/2/q" : 1.0,
										"/track/3/equalizer/filter/3/active" : 1,
										"/track/3/equalizer/filter/3/freq" : 500.0,
										"/track/3/equalizer/filter/3/gain" : 0.0,
										"/track/3/equalizer/filter/3/q" : 1.0,
										"/track/3/equalizer/filter/4/active" : 1,
										"/track/3/equalizer/filter/4/freq" : 1000.0,
										"/track/3/equalizer/filter/4/gain" : 0.0,
										"/track/3/equalizer/filter/4/q" : 1.0,
										"/track/3/equalizer/filter/5/active" : 1,
										"/track/3/equalizer/filter/5/freq" : 2000.0,
										"/track/3/equalizer/filter/5/gain" : 0.0,
										"/track/3/equalizer/filter/5/q" : 1.0,
										"/track/3/equalizer/filter/6/active" : 1,
										"/track/3/equalizer/filter/6/freq" : 5000.0,
										"/track/3/equalizer/filter/6/gain" : 0.0,
										"/track/3/equalizer/filter/6/q" : 1.0,
										"/track/3/equalizer/filter/7/active" : 1,
										"/track/3/equalizer/filter/7/freq" : 10000.0,
										"/track/3/equalizer/filter/7/gain" : 0.0,
										"/track/3/equalizer/filter/7/q" : 1.0,
										"/track/3/equalizer/filter/8/active" : 0,
										"/track/3/equalizer/filter/8/freq" : 16000.0,
										"/track/3/equalizer/filter/8/order" : 2,
										"/track/3/gain" : 0.0,
										"/track/3/gain/ramptime" : 20.0,
										"/track/3/mute" : 0,
										"/track/3/annotation" : "",
										"/track/3/levels/input/visible" : 1,
										"/track/3/levels/input/post" : 0,
										"/track/3/levels/input/mode" : "peak",
										"/track/3/levels/output/visible" : 1,
										"/track/3/levels/output/post" : 1,
										"/track/3/levels/output/mode" : "peak",
										"/track/3/solo" : 0,
										"/track/3/bus/A/destination" : "/bus/1",
										"/track/3/bus/B/destination" : "/",
										"/track/3/bus/C/destination" : "/",
										"/track/3/bus/D/destination" : "/",
										"/track/3/bus/E/destination" : "/",
										"/track/3/bus/F/destination" : "/",
										"/track/3/bus/A/mute" : 0,
										"/track/3/bus/B/mute" : 0,
										"/track/3/bus/C/mute" : 0,
										"/track/3/bus/D/mute" : 0,
										"/track/3/bus/E/mute" : 0,
										"/track/3/bus/F/mute" : 0,
										"/track/3/bus/A/gain" : 0.0,
										"/track/3/bus/B/gain" : 0.0,
										"/track/3/bus/C/gain" : 0.0,
										"/track/3/bus/D/gain" : 0.0,
										"/track/3/bus/E/gain" : 0.0,
										"/track/3/bus/F/gain" : 0.0,
										"/track/3/bus/display" : "A",
										"/track/3/routing/input/1/adc" : 3,
										"/track/3/lfe/send" : 0.0,
										"/track/3/lfe/mute" : 0,
										"/track/3/doppler" : 0,
										"/track/3/air" : 0,
										"/track/3/phaseinvert" : 0,
										"/track/3/azim" : 90.0,
										"/track/3/elev" : 0.0,
										"/track/3/dist" : 10.0,
										"/track/3/delay/linkedtodistance" : 0,
										"/track/3/direct/linkedtodistance" : 1,
										"/track/3/early/linkedtodistance" : 1,
										"/track/3/reverb/linkedtodistance" : 1,
										"/track/3/delay" : 0.0,
										"/track/3/direct/gain/offset" : 0.0,
										"/track/3/direct/gain" : -20.5,
										"/track/3/direct/gain/low" : 0.0,
										"/track/3/direct/gain/med" : 0.0,
										"/track/3/direct/gain/high" : 0.0,
										"/track/3/direct/freq/low" : 250.0,
										"/track/3/direct/freq/high" : 4000.0,
										"/track/3/direct/filter/bypass" : 1,
										"/track/3/early/gain/offset" : 0.0,
										"/track/3/early/gain" : -28.100000381469727,
										"/track/3/early/gain/low" : 0.0,
										"/track/3/early/gain/med" : 0.0,
										"/track/3/early/gain/high" : 0.0,
										"/track/3/early/freq/low" : 250.0,
										"/track/3/early/freq/high" : 4000.0,
										"/track/3/early/filter/bypass" : 1,
										"/track/3/reverb/send" : -19.399999618530273,
										"/track/3/reverb/send/offset" : 0.0,
										"/track/3/reverb/send/ramptime" : 20.0,
										"/track/3/early/width" : 30.0,
										"/track/3/direct/mute" : 0,
										"/track/3/early/mute" : 0,
										"/track/3/reverb/mute" : 0,
										"/track/3/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/3/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/3/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/3/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/3/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/3/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/3/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/3/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/3/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/3/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/3/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/3/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/3/lock" : 0,
										"/track/4/name" : "Mono 4",
										"/track/4/color" : [ 1.0, 0.541176497936249, 0.0, 1.0 ],
										"/track/4/numinputs" : 1,
										"/track/4/visible" : 1,
										"/track/4/trim" : 0.0,
										"/track/4/dynamics/attack" : 10.010000228881836,
										"/track/4/dynamics/release" : 30.0,
										"/track/4/dynamics/lookahead" : 0.0,
										"/track/4/dynamics/compressor/threshold" : -30.0,
										"/track/4/dynamics/compressor/ratio" : 1.0,
										"/track/4/dynamics/expander/threshold" : -60.0,
										"/track/4/dynamics/expander/ratio" : 1.0,
										"/track/4/dynamics/makeup" : 0.0,
										"/track/4/dynamics/link" : "multi mono",
										"/track/4/dynamics/bypass" : 1,
										"/track/4/dynamics/grid/visible" : 1,
										"/track/4/dynamics/curve/fill" : 1,
										"/track/4/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/4/dynamics/foreground/color" : [ 1.0, 0.541176497936249, 0.0, 0.600000023841858 ],
										"/track/4/dynamics/curve/color" : [ 0.800000011920929, 0.431372553110123, 0.0, 1.0 ],
										"/track/4/dynamics/curve/thickness" : 1.0,
										"/track/4/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/4/dynamics/title" : "Mono 4",
										"/track/4/equalizer/bypass" : 1,
										"/track/4/equalizer/gain" : 0.0,
										"/track/4/equalizer/filter/1/active" : 0,
										"/track/4/equalizer/filter/1/freq" : 50.0,
										"/track/4/equalizer/filter/1/order" : 2,
										"/track/4/equalizer/filter/2/active" : 1,
										"/track/4/equalizer/filter/2/freq" : 100.0,
										"/track/4/equalizer/filter/2/gain" : 0.0,
										"/track/4/equalizer/filter/2/q" : 1.0,
										"/track/4/equalizer/filter/3/active" : 1,
										"/track/4/equalizer/filter/3/freq" : 500.0,
										"/track/4/equalizer/filter/3/gain" : 0.0,
										"/track/4/equalizer/filter/3/q" : 1.0,
										"/track/4/equalizer/filter/4/active" : 1,
										"/track/4/equalizer/filter/4/freq" : 1000.0,
										"/track/4/equalizer/filter/4/gain" : 0.0,
										"/track/4/equalizer/filter/4/q" : 1.0,
										"/track/4/equalizer/filter/5/active" : 1,
										"/track/4/equalizer/filter/5/freq" : 2000.0,
										"/track/4/equalizer/filter/5/gain" : 0.0,
										"/track/4/equalizer/filter/5/q" : 1.0,
										"/track/4/equalizer/filter/6/active" : 1,
										"/track/4/equalizer/filter/6/freq" : 5000.0,
										"/track/4/equalizer/filter/6/gain" : 0.0,
										"/track/4/equalizer/filter/6/q" : 1.0,
										"/track/4/equalizer/filter/7/active" : 1,
										"/track/4/equalizer/filter/7/freq" : 10000.0,
										"/track/4/equalizer/filter/7/gain" : 0.0,
										"/track/4/equalizer/filter/7/q" : 1.0,
										"/track/4/equalizer/filter/8/active" : 0,
										"/track/4/equalizer/filter/8/freq" : 16000.0,
										"/track/4/equalizer/filter/8/order" : 2,
										"/track/4/gain" : 0.0,
										"/track/4/gain/ramptime" : 20.0,
										"/track/4/mute" : 0,
										"/track/4/annotation" : "",
										"/track/4/levels/input/visible" : 1,
										"/track/4/levels/input/post" : 0,
										"/track/4/levels/input/mode" : "peak",
										"/track/4/levels/output/visible" : 1,
										"/track/4/levels/output/post" : 1,
										"/track/4/levels/output/mode" : "peak",
										"/track/4/solo" : 0,
										"/track/4/bus/A/destination" : "/bus/1",
										"/track/4/bus/B/destination" : "/",
										"/track/4/bus/C/destination" : "/",
										"/track/4/bus/D/destination" : "/",
										"/track/4/bus/E/destination" : "/",
										"/track/4/bus/F/destination" : "/",
										"/track/4/bus/A/mute" : 0,
										"/track/4/bus/B/mute" : 0,
										"/track/4/bus/C/mute" : 0,
										"/track/4/bus/D/mute" : 0,
										"/track/4/bus/E/mute" : 0,
										"/track/4/bus/F/mute" : 0,
										"/track/4/bus/A/gain" : 0.0,
										"/track/4/bus/B/gain" : 0.0,
										"/track/4/bus/C/gain" : 0.0,
										"/track/4/bus/D/gain" : 0.0,
										"/track/4/bus/E/gain" : 0.0,
										"/track/4/bus/F/gain" : 0.0,
										"/track/4/bus/display" : "A",
										"/track/4/routing/input/1/adc" : 4,
										"/track/4/lfe/send" : 0.0,
										"/track/4/lfe/mute" : 0,
										"/track/4/doppler" : 0,
										"/track/4/air" : 0,
										"/track/4/phaseinvert" : 0,
										"/track/4/azim" : 135.0,
										"/track/4/elev" : 0.0,
										"/track/4/dist" : 10.0,
										"/track/4/delay/linkedtodistance" : 0,
										"/track/4/direct/linkedtodistance" : 1,
										"/track/4/early/linkedtodistance" : 1,
										"/track/4/reverb/linkedtodistance" : 1,
										"/track/4/delay" : 0.0,
										"/track/4/direct/gain/offset" : 0.0,
										"/track/4/direct/gain" : -20.5,
										"/track/4/direct/gain/low" : 0.0,
										"/track/4/direct/gain/med" : 0.0,
										"/track/4/direct/gain/high" : 0.0,
										"/track/4/direct/freq/low" : 250.0,
										"/track/4/direct/freq/high" : 4000.0,
										"/track/4/direct/filter/bypass" : 1,
										"/track/4/early/gain/offset" : 0.0,
										"/track/4/early/gain" : -28.100000381469727,
										"/track/4/early/gain/low" : 0.0,
										"/track/4/early/gain/med" : 0.0,
										"/track/4/early/gain/high" : 0.0,
										"/track/4/early/freq/low" : 250.0,
										"/track/4/early/freq/high" : 4000.0,
										"/track/4/early/filter/bypass" : 1,
										"/track/4/reverb/send" : -19.399999618530273,
										"/track/4/reverb/send/offset" : 0.0,
										"/track/4/reverb/send/ramptime" : 20.0,
										"/track/4/early/width" : 30.0,
										"/track/4/direct/mute" : 0,
										"/track/4/early/mute" : 0,
										"/track/4/reverb/mute" : 0,
										"/track/4/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/4/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/4/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/4/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/4/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/4/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/4/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/4/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/4/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/4/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/4/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/4/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/4/lock" : 0,
										"/track/5/name" : "Mono 5",
										"/track/5/color" : [ 1.0, 0.541176497936249, 0.0, 1.0 ],
										"/track/5/numinputs" : 1,
										"/track/5/visible" : 1,
										"/track/5/trim" : 0.0,
										"/track/5/dynamics/attack" : 10.010000228881836,
										"/track/5/dynamics/release" : 30.0,
										"/track/5/dynamics/lookahead" : 0.0,
										"/track/5/dynamics/compressor/threshold" : -30.0,
										"/track/5/dynamics/compressor/ratio" : 1.0,
										"/track/5/dynamics/expander/threshold" : -60.0,
										"/track/5/dynamics/expander/ratio" : 1.0,
										"/track/5/dynamics/makeup" : 0.0,
										"/track/5/dynamics/link" : "multi mono",
										"/track/5/dynamics/bypass" : 1,
										"/track/5/dynamics/grid/visible" : 1,
										"/track/5/dynamics/curve/fill" : 1,
										"/track/5/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/5/dynamics/foreground/color" : [ 1.0, 0.541176497936249, 0.0, 0.600000023841858 ],
										"/track/5/dynamics/curve/color" : [ 0.800000011920929, 0.431372553110123, 0.0, 1.0 ],
										"/track/5/dynamics/curve/thickness" : 1.0,
										"/track/5/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/5/dynamics/title" : "Mono 5",
										"/track/5/equalizer/bypass" : 1,
										"/track/5/equalizer/gain" : 0.0,
										"/track/5/equalizer/filter/1/active" : 0,
										"/track/5/equalizer/filter/1/freq" : 50.0,
										"/track/5/equalizer/filter/1/order" : 2,
										"/track/5/equalizer/filter/2/active" : 1,
										"/track/5/equalizer/filter/2/freq" : 100.0,
										"/track/5/equalizer/filter/2/gain" : 0.0,
										"/track/5/equalizer/filter/2/q" : 1.0,
										"/track/5/equalizer/filter/3/active" : 1,
										"/track/5/equalizer/filter/3/freq" : 500.0,
										"/track/5/equalizer/filter/3/gain" : 0.0,
										"/track/5/equalizer/filter/3/q" : 1.0,
										"/track/5/equalizer/filter/4/active" : 1,
										"/track/5/equalizer/filter/4/freq" : 1000.0,
										"/track/5/equalizer/filter/4/gain" : 0.0,
										"/track/5/equalizer/filter/4/q" : 1.0,
										"/track/5/equalizer/filter/5/active" : 1,
										"/track/5/equalizer/filter/5/freq" : 2000.0,
										"/track/5/equalizer/filter/5/gain" : 0.0,
										"/track/5/equalizer/filter/5/q" : 1.0,
										"/track/5/equalizer/filter/6/active" : 1,
										"/track/5/equalizer/filter/6/freq" : 5000.0,
										"/track/5/equalizer/filter/6/gain" : 0.0,
										"/track/5/equalizer/filter/6/q" : 1.0,
										"/track/5/equalizer/filter/7/active" : 1,
										"/track/5/equalizer/filter/7/freq" : 10000.0,
										"/track/5/equalizer/filter/7/gain" : 0.0,
										"/track/5/equalizer/filter/7/q" : 1.0,
										"/track/5/equalizer/filter/8/active" : 0,
										"/track/5/equalizer/filter/8/freq" : 16000.0,
										"/track/5/equalizer/filter/8/order" : 2,
										"/track/5/gain" : 0.0,
										"/track/5/gain/ramptime" : 20.0,
										"/track/5/mute" : 0,
										"/track/5/annotation" : "",
										"/track/5/levels/input/visible" : 1,
										"/track/5/levels/input/post" : 0,
										"/track/5/levels/input/mode" : "peak",
										"/track/5/levels/output/visible" : 1,
										"/track/5/levels/output/post" : 1,
										"/track/5/levels/output/mode" : "peak",
										"/track/5/solo" : 0,
										"/track/5/bus/A/destination" : "/bus/1",
										"/track/5/bus/B/destination" : "/",
										"/track/5/bus/C/destination" : "/",
										"/track/5/bus/D/destination" : "/",
										"/track/5/bus/E/destination" : "/",
										"/track/5/bus/F/destination" : "/",
										"/track/5/bus/A/mute" : 0,
										"/track/5/bus/B/mute" : 0,
										"/track/5/bus/C/mute" : 0,
										"/track/5/bus/D/mute" : 0,
										"/track/5/bus/E/mute" : 0,
										"/track/5/bus/F/mute" : 0,
										"/track/5/bus/A/gain" : 0.0,
										"/track/5/bus/B/gain" : 0.0,
										"/track/5/bus/C/gain" : 0.0,
										"/track/5/bus/D/gain" : 0.0,
										"/track/5/bus/E/gain" : 0.0,
										"/track/5/bus/F/gain" : 0.0,
										"/track/5/bus/display" : "A",
										"/track/5/routing/input/1/adc" : 5,
										"/track/5/lfe/send" : 0.0,
										"/track/5/lfe/mute" : 0,
										"/track/5/doppler" : 0,
										"/track/5/air" : 0,
										"/track/5/phaseinvert" : 0,
										"/track/5/azim" : -180.0,
										"/track/5/elev" : 0.0,
										"/track/5/dist" : 10.0,
										"/track/5/delay/linkedtodistance" : 0,
										"/track/5/direct/linkedtodistance" : 1,
										"/track/5/early/linkedtodistance" : 1,
										"/track/5/reverb/linkedtodistance" : 1,
										"/track/5/delay" : 0.0,
										"/track/5/direct/gain/offset" : 0.0,
										"/track/5/direct/gain" : -20.5,
										"/track/5/direct/gain/low" : 0.0,
										"/track/5/direct/gain/med" : 0.0,
										"/track/5/direct/gain/high" : 0.0,
										"/track/5/direct/freq/low" : 250.0,
										"/track/5/direct/freq/high" : 4000.0,
										"/track/5/direct/filter/bypass" : 1,
										"/track/5/early/gain/offset" : 0.0,
										"/track/5/early/gain" : -28.100000381469727,
										"/track/5/early/gain/low" : 0.0,
										"/track/5/early/gain/med" : 0.0,
										"/track/5/early/gain/high" : 0.0,
										"/track/5/early/freq/low" : 250.0,
										"/track/5/early/freq/high" : 4000.0,
										"/track/5/early/filter/bypass" : 1,
										"/track/5/reverb/send" : -19.399999618530273,
										"/track/5/reverb/send/offset" : 0.0,
										"/track/5/reverb/send/ramptime" : 20.0,
										"/track/5/early/width" : 30.0,
										"/track/5/direct/mute" : 0,
										"/track/5/early/mute" : 0,
										"/track/5/reverb/mute" : 0,
										"/track/5/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/5/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/5/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/5/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/5/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/5/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/5/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/5/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/5/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/5/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/5/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/5/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/5/lock" : 0,
										"/track/6/name" : "Mono 6",
										"/track/6/color" : [ 1.0, 0.541176497936249, 0.0, 1.0 ],
										"/track/6/numinputs" : 1,
										"/track/6/visible" : 1,
										"/track/6/trim" : 0.0,
										"/track/6/dynamics/attack" : 10.010000228881836,
										"/track/6/dynamics/release" : 30.0,
										"/track/6/dynamics/lookahead" : 0.0,
										"/track/6/dynamics/compressor/threshold" : -30.0,
										"/track/6/dynamics/compressor/ratio" : 1.0,
										"/track/6/dynamics/expander/threshold" : -60.0,
										"/track/6/dynamics/expander/ratio" : 1.0,
										"/track/6/dynamics/makeup" : 0.0,
										"/track/6/dynamics/link" : "multi mono",
										"/track/6/dynamics/bypass" : 1,
										"/track/6/dynamics/grid/visible" : 1,
										"/track/6/dynamics/curve/fill" : 1,
										"/track/6/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/6/dynamics/foreground/color" : [ 1.0, 0.541176497936249, 0.0, 0.600000023841858 ],
										"/track/6/dynamics/curve/color" : [ 0.800000011920929, 0.431372553110123, 0.0, 1.0 ],
										"/track/6/dynamics/curve/thickness" : 1.0,
										"/track/6/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/6/dynamics/title" : "Mono 6",
										"/track/6/equalizer/bypass" : 1,
										"/track/6/equalizer/gain" : 0.0,
										"/track/6/equalizer/filter/1/active" : 0,
										"/track/6/equalizer/filter/1/freq" : 50.0,
										"/track/6/equalizer/filter/1/order" : 2,
										"/track/6/equalizer/filter/2/active" : 1,
										"/track/6/equalizer/filter/2/freq" : 100.0,
										"/track/6/equalizer/filter/2/gain" : 0.0,
										"/track/6/equalizer/filter/2/q" : 1.0,
										"/track/6/equalizer/filter/3/active" : 1,
										"/track/6/equalizer/filter/3/freq" : 500.0,
										"/track/6/equalizer/filter/3/gain" : 0.0,
										"/track/6/equalizer/filter/3/q" : 1.0,
										"/track/6/equalizer/filter/4/active" : 1,
										"/track/6/equalizer/filter/4/freq" : 1000.0,
										"/track/6/equalizer/filter/4/gain" : 0.0,
										"/track/6/equalizer/filter/4/q" : 1.0,
										"/track/6/equalizer/filter/5/active" : 1,
										"/track/6/equalizer/filter/5/freq" : 2000.0,
										"/track/6/equalizer/filter/5/gain" : 0.0,
										"/track/6/equalizer/filter/5/q" : 1.0,
										"/track/6/equalizer/filter/6/active" : 1,
										"/track/6/equalizer/filter/6/freq" : 5000.0,
										"/track/6/equalizer/filter/6/gain" : 0.0,
										"/track/6/equalizer/filter/6/q" : 1.0,
										"/track/6/equalizer/filter/7/active" : 1,
										"/track/6/equalizer/filter/7/freq" : 10000.0,
										"/track/6/equalizer/filter/7/gain" : 0.0,
										"/track/6/equalizer/filter/7/q" : 1.0,
										"/track/6/equalizer/filter/8/active" : 0,
										"/track/6/equalizer/filter/8/freq" : 16000.0,
										"/track/6/equalizer/filter/8/order" : 2,
										"/track/6/gain" : 0.0,
										"/track/6/gain/ramptime" : 20.0,
										"/track/6/mute" : 0,
										"/track/6/annotation" : "",
										"/track/6/levels/input/visible" : 1,
										"/track/6/levels/input/post" : 0,
										"/track/6/levels/input/mode" : "peak",
										"/track/6/levels/output/visible" : 1,
										"/track/6/levels/output/post" : 1,
										"/track/6/levels/output/mode" : "peak",
										"/track/6/solo" : 0,
										"/track/6/bus/A/destination" : "/bus/1",
										"/track/6/bus/B/destination" : "/",
										"/track/6/bus/C/destination" : "/",
										"/track/6/bus/D/destination" : "/",
										"/track/6/bus/E/destination" : "/",
										"/track/6/bus/F/destination" : "/",
										"/track/6/bus/A/mute" : 0,
										"/track/6/bus/B/mute" : 0,
										"/track/6/bus/C/mute" : 0,
										"/track/6/bus/D/mute" : 0,
										"/track/6/bus/E/mute" : 0,
										"/track/6/bus/F/mute" : 0,
										"/track/6/bus/A/gain" : 0.0,
										"/track/6/bus/B/gain" : 0.0,
										"/track/6/bus/C/gain" : 0.0,
										"/track/6/bus/D/gain" : 0.0,
										"/track/6/bus/E/gain" : 0.0,
										"/track/6/bus/F/gain" : 0.0,
										"/track/6/bus/display" : "A",
										"/track/6/routing/input/1/adc" : 6,
										"/track/6/lfe/send" : 0.0,
										"/track/6/lfe/mute" : 0,
										"/track/6/doppler" : 0,
										"/track/6/air" : 0,
										"/track/6/phaseinvert" : 0,
										"/track/6/azim" : -135.0,
										"/track/6/elev" : 0.0,
										"/track/6/dist" : 10.0,
										"/track/6/delay/linkedtodistance" : 0,
										"/track/6/direct/linkedtodistance" : 1,
										"/track/6/early/linkedtodistance" : 1,
										"/track/6/reverb/linkedtodistance" : 1,
										"/track/6/delay" : 0.0,
										"/track/6/direct/gain/offset" : 0.0,
										"/track/6/direct/gain" : -20.5,
										"/track/6/direct/gain/low" : 0.0,
										"/track/6/direct/gain/med" : 0.0,
										"/track/6/direct/gain/high" : 0.0,
										"/track/6/direct/freq/low" : 250.0,
										"/track/6/direct/freq/high" : 4000.0,
										"/track/6/direct/filter/bypass" : 1,
										"/track/6/early/gain/offset" : 0.0,
										"/track/6/early/gain" : -28.100000381469727,
										"/track/6/early/gain/low" : 0.0,
										"/track/6/early/gain/med" : 0.0,
										"/track/6/early/gain/high" : 0.0,
										"/track/6/early/freq/low" : 250.0,
										"/track/6/early/freq/high" : 4000.0,
										"/track/6/early/filter/bypass" : 1,
										"/track/6/reverb/send" : -19.399999618530273,
										"/track/6/reverb/send/offset" : 0.0,
										"/track/6/reverb/send/ramptime" : 20.0,
										"/track/6/early/width" : 30.0,
										"/track/6/direct/mute" : 0,
										"/track/6/early/mute" : 0,
										"/track/6/reverb/mute" : 0,
										"/track/6/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/6/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/6/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/6/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/6/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/6/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/6/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/6/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/6/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/6/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/6/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/6/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/6/lock" : 0,
										"/track/7/name" : "Mono 7",
										"/track/7/color" : [ 1.0, 0.541176497936249, 0.0, 1.0 ],
										"/track/7/numinputs" : 1,
										"/track/7/visible" : 1,
										"/track/7/trim" : 0.0,
										"/track/7/dynamics/attack" : 10.010000228881836,
										"/track/7/dynamics/release" : 30.0,
										"/track/7/dynamics/lookahead" : 0.0,
										"/track/7/dynamics/compressor/threshold" : -30.0,
										"/track/7/dynamics/compressor/ratio" : 1.0,
										"/track/7/dynamics/expander/threshold" : -60.0,
										"/track/7/dynamics/expander/ratio" : 1.0,
										"/track/7/dynamics/makeup" : 0.0,
										"/track/7/dynamics/link" : "multi mono",
										"/track/7/dynamics/bypass" : 1,
										"/track/7/dynamics/grid/visible" : 1,
										"/track/7/dynamics/curve/fill" : 1,
										"/track/7/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/7/dynamics/foreground/color" : [ 1.0, 0.541176497936249, 0.0, 0.600000023841858 ],
										"/track/7/dynamics/curve/color" : [ 0.800000011920929, 0.431372553110123, 0.0, 1.0 ],
										"/track/7/dynamics/curve/thickness" : 1.0,
										"/track/7/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/7/dynamics/title" : "Mono 7",
										"/track/7/equalizer/bypass" : 1,
										"/track/7/equalizer/gain" : 0.0,
										"/track/7/equalizer/filter/1/active" : 0,
										"/track/7/equalizer/filter/1/freq" : 50.0,
										"/track/7/equalizer/filter/1/order" : 2,
										"/track/7/equalizer/filter/2/active" : 1,
										"/track/7/equalizer/filter/2/freq" : 100.0,
										"/track/7/equalizer/filter/2/gain" : 0.0,
										"/track/7/equalizer/filter/2/q" : 1.0,
										"/track/7/equalizer/filter/3/active" : 1,
										"/track/7/equalizer/filter/3/freq" : 500.0,
										"/track/7/equalizer/filter/3/gain" : 0.0,
										"/track/7/equalizer/filter/3/q" : 1.0,
										"/track/7/equalizer/filter/4/active" : 1,
										"/track/7/equalizer/filter/4/freq" : 1000.0,
										"/track/7/equalizer/filter/4/gain" : 0.0,
										"/track/7/equalizer/filter/4/q" : 1.0,
										"/track/7/equalizer/filter/5/active" : 1,
										"/track/7/equalizer/filter/5/freq" : 2000.0,
										"/track/7/equalizer/filter/5/gain" : 0.0,
										"/track/7/equalizer/filter/5/q" : 1.0,
										"/track/7/equalizer/filter/6/active" : 1,
										"/track/7/equalizer/filter/6/freq" : 5000.0,
										"/track/7/equalizer/filter/6/gain" : 0.0,
										"/track/7/equalizer/filter/6/q" : 1.0,
										"/track/7/equalizer/filter/7/active" : 1,
										"/track/7/equalizer/filter/7/freq" : 10000.0,
										"/track/7/equalizer/filter/7/gain" : 0.0,
										"/track/7/equalizer/filter/7/q" : 1.0,
										"/track/7/equalizer/filter/8/active" : 0,
										"/track/7/equalizer/filter/8/freq" : 16000.0,
										"/track/7/equalizer/filter/8/order" : 2,
										"/track/7/gain" : 0.0,
										"/track/7/gain/ramptime" : 20.0,
										"/track/7/mute" : 0,
										"/track/7/annotation" : "",
										"/track/7/levels/input/visible" : 1,
										"/track/7/levels/input/post" : 0,
										"/track/7/levels/input/mode" : "peak",
										"/track/7/levels/output/visible" : 1,
										"/track/7/levels/output/post" : 1,
										"/track/7/levels/output/mode" : "peak",
										"/track/7/solo" : 0,
										"/track/7/bus/A/destination" : "/bus/1",
										"/track/7/bus/B/destination" : "/",
										"/track/7/bus/C/destination" : "/",
										"/track/7/bus/D/destination" : "/",
										"/track/7/bus/E/destination" : "/",
										"/track/7/bus/F/destination" : "/",
										"/track/7/bus/A/mute" : 0,
										"/track/7/bus/B/mute" : 0,
										"/track/7/bus/C/mute" : 0,
										"/track/7/bus/D/mute" : 0,
										"/track/7/bus/E/mute" : 0,
										"/track/7/bus/F/mute" : 0,
										"/track/7/bus/A/gain" : 0.0,
										"/track/7/bus/B/gain" : 0.0,
										"/track/7/bus/C/gain" : 0.0,
										"/track/7/bus/D/gain" : 0.0,
										"/track/7/bus/E/gain" : 0.0,
										"/track/7/bus/F/gain" : 0.0,
										"/track/7/bus/display" : "A",
										"/track/7/routing/input/1/adc" : 7,
										"/track/7/lfe/send" : 0.0,
										"/track/7/lfe/mute" : 0,
										"/track/7/doppler" : 0,
										"/track/7/air" : 0,
										"/track/7/phaseinvert" : 0,
										"/track/7/azim" : -90.0,
										"/track/7/elev" : 0.0,
										"/track/7/dist" : 10.0,
										"/track/7/delay/linkedtodistance" : 0,
										"/track/7/direct/linkedtodistance" : 1,
										"/track/7/early/linkedtodistance" : 1,
										"/track/7/reverb/linkedtodistance" : 1,
										"/track/7/delay" : 0.0,
										"/track/7/direct/gain/offset" : 0.0,
										"/track/7/direct/gain" : -20.5,
										"/track/7/direct/gain/low" : 0.0,
										"/track/7/direct/gain/med" : 0.0,
										"/track/7/direct/gain/high" : 0.0,
										"/track/7/direct/freq/low" : 250.0,
										"/track/7/direct/freq/high" : 4000.0,
										"/track/7/direct/filter/bypass" : 1,
										"/track/7/early/gain/offset" : 0.0,
										"/track/7/early/gain" : -28.100000381469727,
										"/track/7/early/gain/low" : 0.0,
										"/track/7/early/gain/med" : 0.0,
										"/track/7/early/gain/high" : 0.0,
										"/track/7/early/freq/low" : 250.0,
										"/track/7/early/freq/high" : 4000.0,
										"/track/7/early/filter/bypass" : 1,
										"/track/7/reverb/send" : -19.399999618530273,
										"/track/7/reverb/send/offset" : 0.0,
										"/track/7/reverb/send/ramptime" : 20.0,
										"/track/7/early/width" : 30.0,
										"/track/7/direct/mute" : 0,
										"/track/7/early/mute" : 0,
										"/track/7/reverb/mute" : 0,
										"/track/7/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/7/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/7/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/7/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/7/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/7/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/7/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/7/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/7/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/7/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/7/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/7/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/7/lock" : 0,
										"/track/8/name" : "Mono 8",
										"/track/8/color" : [ 1.0, 0.541176497936249, 0.0, 1.0 ],
										"/track/8/numinputs" : 1,
										"/track/8/visible" : 1,
										"/track/8/trim" : 0.0,
										"/track/8/dynamics/attack" : 10.010000228881836,
										"/track/8/dynamics/release" : 30.0,
										"/track/8/dynamics/lookahead" : 0.0,
										"/track/8/dynamics/compressor/threshold" : -30.0,
										"/track/8/dynamics/compressor/ratio" : 1.0,
										"/track/8/dynamics/expander/threshold" : -60.0,
										"/track/8/dynamics/expander/ratio" : 1.0,
										"/track/8/dynamics/makeup" : 0.0,
										"/track/8/dynamics/link" : "multi mono",
										"/track/8/dynamics/bypass" : 1,
										"/track/8/dynamics/grid/visible" : 1,
										"/track/8/dynamics/curve/fill" : 1,
										"/track/8/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/8/dynamics/foreground/color" : [ 1.0, 0.541176497936249, 0.0, 0.600000023841858 ],
										"/track/8/dynamics/curve/color" : [ 0.800000011920929, 0.431372553110123, 0.0, 1.0 ],
										"/track/8/dynamics/curve/thickness" : 1.0,
										"/track/8/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/8/dynamics/title" : "Mono 8",
										"/track/8/equalizer/bypass" : 1,
										"/track/8/equalizer/gain" : 0.0,
										"/track/8/equalizer/filter/1/active" : 0,
										"/track/8/equalizer/filter/1/freq" : 50.0,
										"/track/8/equalizer/filter/1/order" : 2,
										"/track/8/equalizer/filter/2/active" : 1,
										"/track/8/equalizer/filter/2/freq" : 100.0,
										"/track/8/equalizer/filter/2/gain" : 0.0,
										"/track/8/equalizer/filter/2/q" : 1.0,
										"/track/8/equalizer/filter/3/active" : 1,
										"/track/8/equalizer/filter/3/freq" : 500.0,
										"/track/8/equalizer/filter/3/gain" : 0.0,
										"/track/8/equalizer/filter/3/q" : 1.0,
										"/track/8/equalizer/filter/4/active" : 1,
										"/track/8/equalizer/filter/4/freq" : 1000.0,
										"/track/8/equalizer/filter/4/gain" : 0.0,
										"/track/8/equalizer/filter/4/q" : 1.0,
										"/track/8/equalizer/filter/5/active" : 1,
										"/track/8/equalizer/filter/5/freq" : 2000.0,
										"/track/8/equalizer/filter/5/gain" : 0.0,
										"/track/8/equalizer/filter/5/q" : 1.0,
										"/track/8/equalizer/filter/6/active" : 1,
										"/track/8/equalizer/filter/6/freq" : 5000.0,
										"/track/8/equalizer/filter/6/gain" : 0.0,
										"/track/8/equalizer/filter/6/q" : 1.0,
										"/track/8/equalizer/filter/7/active" : 1,
										"/track/8/equalizer/filter/7/freq" : 10000.0,
										"/track/8/equalizer/filter/7/gain" : 0.0,
										"/track/8/equalizer/filter/7/q" : 1.0,
										"/track/8/equalizer/filter/8/active" : 0,
										"/track/8/equalizer/filter/8/freq" : 16000.0,
										"/track/8/equalizer/filter/8/order" : 2,
										"/track/8/gain" : 0.0,
										"/track/8/gain/ramptime" : 20.0,
										"/track/8/mute" : 0,
										"/track/8/annotation" : "",
										"/track/8/levels/input/visible" : 1,
										"/track/8/levels/input/post" : 0,
										"/track/8/levels/input/mode" : "peak",
										"/track/8/levels/output/visible" : 1,
										"/track/8/levels/output/post" : 1,
										"/track/8/levels/output/mode" : "peak",
										"/track/8/solo" : 0,
										"/track/8/bus/A/destination" : "/bus/1",
										"/track/8/bus/B/destination" : "/",
										"/track/8/bus/C/destination" : "/",
										"/track/8/bus/D/destination" : "/",
										"/track/8/bus/E/destination" : "/",
										"/track/8/bus/F/destination" : "/",
										"/track/8/bus/A/mute" : 0,
										"/track/8/bus/B/mute" : 0,
										"/track/8/bus/C/mute" : 0,
										"/track/8/bus/D/mute" : 0,
										"/track/8/bus/E/mute" : 0,
										"/track/8/bus/F/mute" : 0,
										"/track/8/bus/A/gain" : 0.0,
										"/track/8/bus/B/gain" : 0.0,
										"/track/8/bus/C/gain" : 0.0,
										"/track/8/bus/D/gain" : 0.0,
										"/track/8/bus/E/gain" : 0.0,
										"/track/8/bus/F/gain" : 0.0,
										"/track/8/bus/display" : "A",
										"/track/8/routing/input/1/adc" : 8,
										"/track/8/lfe/send" : 0.0,
										"/track/8/lfe/mute" : 0,
										"/track/8/doppler" : 0,
										"/track/8/air" : 0,
										"/track/8/phaseinvert" : 0,
										"/track/8/azim" : -45.0,
										"/track/8/elev" : 0.0,
										"/track/8/dist" : 10.0,
										"/track/8/delay/linkedtodistance" : 0,
										"/track/8/direct/linkedtodistance" : 1,
										"/track/8/early/linkedtodistance" : 1,
										"/track/8/reverb/linkedtodistance" : 1,
										"/track/8/delay" : 0.0,
										"/track/8/direct/gain/offset" : 0.0,
										"/track/8/direct/gain" : -20.5,
										"/track/8/direct/gain/low" : 0.0,
										"/track/8/direct/gain/med" : 0.0,
										"/track/8/direct/gain/high" : 0.0,
										"/track/8/direct/freq/low" : 250.0,
										"/track/8/direct/freq/high" : 4000.0,
										"/track/8/direct/filter/bypass" : 1,
										"/track/8/early/gain/offset" : 0.0,
										"/track/8/early/gain" : -28.100000381469727,
										"/track/8/early/gain/low" : 0.0,
										"/track/8/early/gain/med" : 0.0,
										"/track/8/early/gain/high" : 0.0,
										"/track/8/early/freq/low" : 250.0,
										"/track/8/early/freq/high" : 4000.0,
										"/track/8/early/filter/bypass" : 1,
										"/track/8/reverb/send" : -19.399999618530273,
										"/track/8/reverb/send/offset" : 0.0,
										"/track/8/reverb/send/ramptime" : 20.0,
										"/track/8/early/width" : 30.0,
										"/track/8/direct/mute" : 0,
										"/track/8/early/mute" : 0,
										"/track/8/reverb/mute" : 0,
										"/track/8/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/8/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/8/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/8/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/8/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/8/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/8/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/8/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/8/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/8/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/8/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/8/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/8/lock" : 0,
										"/track/9/name" : "Mono 9",
										"/track/9/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 1.0 ],
										"/track/9/numinputs" : 1,
										"/track/9/visible" : 1,
										"/track/9/trim" : 0.0,
										"/track/9/dynamics/attack" : 10.010000228881836,
										"/track/9/dynamics/release" : 30.0,
										"/track/9/dynamics/lookahead" : 0.0,
										"/track/9/dynamics/compressor/threshold" : -30.0,
										"/track/9/dynamics/compressor/ratio" : 1.0,
										"/track/9/dynamics/expander/threshold" : -60.0,
										"/track/9/dynamics/expander/ratio" : 1.0,
										"/track/9/dynamics/makeup" : 0.0,
										"/track/9/dynamics/link" : "multi mono",
										"/track/9/dynamics/bypass" : 1,
										"/track/9/dynamics/grid/visible" : 1,
										"/track/9/dynamics/curve/fill" : 1,
										"/track/9/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/9/dynamics/foreground/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 0.600000023841858 ],
										"/track/9/dynamics/curve/color" : [ 0.800000011920929, 0.776470601558685, 0.094117648899555, 1.0 ],
										"/track/9/dynamics/curve/thickness" : 1.0,
										"/track/9/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/9/dynamics/title" : "Mono 9",
										"/track/9/equalizer/bypass" : 1,
										"/track/9/equalizer/gain" : 0.0,
										"/track/9/equalizer/filter/1/active" : 0,
										"/track/9/equalizer/filter/1/freq" : 50.0,
										"/track/9/equalizer/filter/1/order" : 2,
										"/track/9/equalizer/filter/2/active" : 1,
										"/track/9/equalizer/filter/2/freq" : 100.0,
										"/track/9/equalizer/filter/2/gain" : 0.0,
										"/track/9/equalizer/filter/2/q" : 1.0,
										"/track/9/equalizer/filter/3/active" : 1,
										"/track/9/equalizer/filter/3/freq" : 500.0,
										"/track/9/equalizer/filter/3/gain" : 0.0,
										"/track/9/equalizer/filter/3/q" : 1.0,
										"/track/9/equalizer/filter/4/active" : 1,
										"/track/9/equalizer/filter/4/freq" : 1000.0,
										"/track/9/equalizer/filter/4/gain" : 0.0,
										"/track/9/equalizer/filter/4/q" : 1.0,
										"/track/9/equalizer/filter/5/active" : 1,
										"/track/9/equalizer/filter/5/freq" : 2000.0,
										"/track/9/equalizer/filter/5/gain" : 0.0,
										"/track/9/equalizer/filter/5/q" : 1.0,
										"/track/9/equalizer/filter/6/active" : 1,
										"/track/9/equalizer/filter/6/freq" : 5000.0,
										"/track/9/equalizer/filter/6/gain" : 0.0,
										"/track/9/equalizer/filter/6/q" : 1.0,
										"/track/9/equalizer/filter/7/active" : 1,
										"/track/9/equalizer/filter/7/freq" : 10000.0,
										"/track/9/equalizer/filter/7/gain" : 0.0,
										"/track/9/equalizer/filter/7/q" : 1.0,
										"/track/9/equalizer/filter/8/active" : 0,
										"/track/9/equalizer/filter/8/freq" : 16000.0,
										"/track/9/equalizer/filter/8/order" : 2,
										"/track/9/gain" : 0.0,
										"/track/9/gain/ramptime" : 20.0,
										"/track/9/mute" : 0,
										"/track/9/annotation" : "",
										"/track/9/levels/input/visible" : 1,
										"/track/9/levels/input/post" : 0,
										"/track/9/levels/input/mode" : "peak",
										"/track/9/levels/output/visible" : 1,
										"/track/9/levels/output/post" : 1,
										"/track/9/levels/output/mode" : "peak",
										"/track/9/solo" : 0,
										"/track/9/bus/A/destination" : "/bus/1",
										"/track/9/bus/B/destination" : "/",
										"/track/9/bus/C/destination" : "/",
										"/track/9/bus/D/destination" : "/",
										"/track/9/bus/E/destination" : "/",
										"/track/9/bus/F/destination" : "/",
										"/track/9/bus/A/mute" : 0,
										"/track/9/bus/B/mute" : 0,
										"/track/9/bus/C/mute" : 0,
										"/track/9/bus/D/mute" : 0,
										"/track/9/bus/E/mute" : 0,
										"/track/9/bus/F/mute" : 0,
										"/track/9/bus/A/gain" : 0.0,
										"/track/9/bus/B/gain" : 0.0,
										"/track/9/bus/C/gain" : 0.0,
										"/track/9/bus/D/gain" : 0.0,
										"/track/9/bus/E/gain" : 0.0,
										"/track/9/bus/F/gain" : 0.0,
										"/track/9/bus/display" : "A",
										"/track/9/routing/input/1/adc" : 9,
										"/track/9/lfe/send" : 0.0,
										"/track/9/lfe/mute" : 0,
										"/track/9/doppler" : 0,
										"/track/9/air" : 0,
										"/track/9/phaseinvert" : 0,
										"/track/9/azim" : 0.0,
										"/track/9/elev" : 0.0,
										"/track/9/dist" : 10.0,
										"/track/9/delay/linkedtodistance" : 0,
										"/track/9/direct/linkedtodistance" : 1,
										"/track/9/early/linkedtodistance" : 1,
										"/track/9/reverb/linkedtodistance" : 1,
										"/track/9/delay" : 0.0,
										"/track/9/direct/gain/offset" : 0.0,
										"/track/9/direct/gain" : -20.5,
										"/track/9/direct/gain/low" : 0.0,
										"/track/9/direct/gain/med" : 0.0,
										"/track/9/direct/gain/high" : 0.0,
										"/track/9/direct/freq/low" : 250.0,
										"/track/9/direct/freq/high" : 4000.0,
										"/track/9/direct/filter/bypass" : 1,
										"/track/9/early/gain/offset" : 0.0,
										"/track/9/early/gain" : -28.100000381469727,
										"/track/9/early/gain/low" : 0.0,
										"/track/9/early/gain/med" : 0.0,
										"/track/9/early/gain/high" : 0.0,
										"/track/9/early/freq/low" : 250.0,
										"/track/9/early/freq/high" : 4000.0,
										"/track/9/early/filter/bypass" : 1,
										"/track/9/reverb/send" : -19.399999618530273,
										"/track/9/reverb/send/offset" : 0.0,
										"/track/9/reverb/send/ramptime" : 20.0,
										"/track/9/early/width" : 30.0,
										"/track/9/direct/mute" : 0,
										"/track/9/early/mute" : 0,
										"/track/9/reverb/mute" : 0,
										"/track/9/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/9/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/9/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/9/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/9/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/9/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/9/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/9/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/9/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/9/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/9/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/9/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/9/lock" : 0,
										"/track/10/name" : "Mono 10",
										"/track/10/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 1.0 ],
										"/track/10/numinputs" : 1,
										"/track/10/visible" : 1,
										"/track/10/trim" : 0.0,
										"/track/10/dynamics/attack" : 10.010000228881836,
										"/track/10/dynamics/release" : 30.0,
										"/track/10/dynamics/lookahead" : 0.0,
										"/track/10/dynamics/compressor/threshold" : -30.0,
										"/track/10/dynamics/compressor/ratio" : 1.0,
										"/track/10/dynamics/expander/threshold" : -60.0,
										"/track/10/dynamics/expander/ratio" : 1.0,
										"/track/10/dynamics/makeup" : 0.0,
										"/track/10/dynamics/link" : "multi mono",
										"/track/10/dynamics/bypass" : 1,
										"/track/10/dynamics/grid/visible" : 1,
										"/track/10/dynamics/curve/fill" : 1,
										"/track/10/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/10/dynamics/foreground/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 0.600000023841858 ],
										"/track/10/dynamics/curve/color" : [ 0.800000011920929, 0.776470601558685, 0.094117648899555, 1.0 ],
										"/track/10/dynamics/curve/thickness" : 1.0,
										"/track/10/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/10/dynamics/title" : "Mono 10",
										"/track/10/equalizer/bypass" : 1,
										"/track/10/equalizer/gain" : 0.0,
										"/track/10/equalizer/filter/1/active" : 0,
										"/track/10/equalizer/filter/1/freq" : 50.0,
										"/track/10/equalizer/filter/1/order" : 2,
										"/track/10/equalizer/filter/2/active" : 1,
										"/track/10/equalizer/filter/2/freq" : 100.0,
										"/track/10/equalizer/filter/2/gain" : 0.0,
										"/track/10/equalizer/filter/2/q" : 1.0,
										"/track/10/equalizer/filter/3/active" : 1,
										"/track/10/equalizer/filter/3/freq" : 500.0,
										"/track/10/equalizer/filter/3/gain" : 0.0,
										"/track/10/equalizer/filter/3/q" : 1.0,
										"/track/10/equalizer/filter/4/active" : 1,
										"/track/10/equalizer/filter/4/freq" : 1000.0,
										"/track/10/equalizer/filter/4/gain" : 0.0,
										"/track/10/equalizer/filter/4/q" : 1.0,
										"/track/10/equalizer/filter/5/active" : 1,
										"/track/10/equalizer/filter/5/freq" : 2000.0,
										"/track/10/equalizer/filter/5/gain" : 0.0,
										"/track/10/equalizer/filter/5/q" : 1.0,
										"/track/10/equalizer/filter/6/active" : 1,
										"/track/10/equalizer/filter/6/freq" : 5000.0,
										"/track/10/equalizer/filter/6/gain" : 0.0,
										"/track/10/equalizer/filter/6/q" : 1.0,
										"/track/10/equalizer/filter/7/active" : 1,
										"/track/10/equalizer/filter/7/freq" : 10000.0,
										"/track/10/equalizer/filter/7/gain" : 0.0,
										"/track/10/equalizer/filter/7/q" : 1.0,
										"/track/10/equalizer/filter/8/active" : 0,
										"/track/10/equalizer/filter/8/freq" : 16000.0,
										"/track/10/equalizer/filter/8/order" : 2,
										"/track/10/gain" : 0.0,
										"/track/10/gain/ramptime" : 20.0,
										"/track/10/mute" : 0,
										"/track/10/annotation" : "",
										"/track/10/levels/input/visible" : 1,
										"/track/10/levels/input/post" : 0,
										"/track/10/levels/input/mode" : "peak",
										"/track/10/levels/output/visible" : 1,
										"/track/10/levels/output/post" : 1,
										"/track/10/levels/output/mode" : "peak",
										"/track/10/solo" : 0,
										"/track/10/bus/A/destination" : "/bus/1",
										"/track/10/bus/B/destination" : "/",
										"/track/10/bus/C/destination" : "/",
										"/track/10/bus/D/destination" : "/",
										"/track/10/bus/E/destination" : "/",
										"/track/10/bus/F/destination" : "/",
										"/track/10/bus/A/mute" : 0,
										"/track/10/bus/B/mute" : 0,
										"/track/10/bus/C/mute" : 0,
										"/track/10/bus/D/mute" : 0,
										"/track/10/bus/E/mute" : 0,
										"/track/10/bus/F/mute" : 0,
										"/track/10/bus/A/gain" : 0.0,
										"/track/10/bus/B/gain" : 0.0,
										"/track/10/bus/C/gain" : 0.0,
										"/track/10/bus/D/gain" : 0.0,
										"/track/10/bus/E/gain" : 0.0,
										"/track/10/bus/F/gain" : 0.0,
										"/track/10/bus/display" : "A",
										"/track/10/routing/input/1/adc" : 10,
										"/track/10/lfe/send" : 0.0,
										"/track/10/lfe/mute" : 0,
										"/track/10/doppler" : 0,
										"/track/10/air" : 0,
										"/track/10/phaseinvert" : 0,
										"/track/10/azim" : 45.0,
										"/track/10/elev" : 0.0,
										"/track/10/dist" : 10.0,
										"/track/10/delay/linkedtodistance" : 0,
										"/track/10/direct/linkedtodistance" : 1,
										"/track/10/early/linkedtodistance" : 1,
										"/track/10/reverb/linkedtodistance" : 1,
										"/track/10/delay" : 0.0,
										"/track/10/direct/gain/offset" : 0.0,
										"/track/10/direct/gain" : -20.5,
										"/track/10/direct/gain/low" : 0.0,
										"/track/10/direct/gain/med" : 0.0,
										"/track/10/direct/gain/high" : 0.0,
										"/track/10/direct/freq/low" : 250.0,
										"/track/10/direct/freq/high" : 4000.0,
										"/track/10/direct/filter/bypass" : 1,
										"/track/10/early/gain/offset" : 0.0,
										"/track/10/early/gain" : -28.100000381469727,
										"/track/10/early/gain/low" : 0.0,
										"/track/10/early/gain/med" : 0.0,
										"/track/10/early/gain/high" : 0.0,
										"/track/10/early/freq/low" : 250.0,
										"/track/10/early/freq/high" : 4000.0,
										"/track/10/early/filter/bypass" : 1,
										"/track/10/reverb/send" : -19.399999618530273,
										"/track/10/reverb/send/offset" : 0.0,
										"/track/10/reverb/send/ramptime" : 20.0,
										"/track/10/early/width" : 30.0,
										"/track/10/direct/mute" : 0,
										"/track/10/early/mute" : 0,
										"/track/10/reverb/mute" : 0,
										"/track/10/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/10/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/10/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/10/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/10/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/10/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/10/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/10/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/10/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/10/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/10/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/10/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/10/lock" : 0,
										"/track/11/name" : "Mono 11",
										"/track/11/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 1.0 ],
										"/track/11/numinputs" : 1,
										"/track/11/visible" : 1,
										"/track/11/trim" : 0.0,
										"/track/11/dynamics/attack" : 10.010000228881836,
										"/track/11/dynamics/release" : 30.0,
										"/track/11/dynamics/lookahead" : 0.0,
										"/track/11/dynamics/compressor/threshold" : -30.0,
										"/track/11/dynamics/compressor/ratio" : 1.0,
										"/track/11/dynamics/expander/threshold" : -60.0,
										"/track/11/dynamics/expander/ratio" : 1.0,
										"/track/11/dynamics/makeup" : 0.0,
										"/track/11/dynamics/link" : "multi mono",
										"/track/11/dynamics/bypass" : 1,
										"/track/11/dynamics/grid/visible" : 1,
										"/track/11/dynamics/curve/fill" : 1,
										"/track/11/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/11/dynamics/foreground/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 0.600000023841858 ],
										"/track/11/dynamics/curve/color" : [ 0.800000011920929, 0.776470601558685, 0.094117648899555, 1.0 ],
										"/track/11/dynamics/curve/thickness" : 1.0,
										"/track/11/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/11/dynamics/title" : "Mono 11",
										"/track/11/equalizer/bypass" : 1,
										"/track/11/equalizer/gain" : 0.0,
										"/track/11/equalizer/filter/1/active" : 0,
										"/track/11/equalizer/filter/1/freq" : 50.0,
										"/track/11/equalizer/filter/1/order" : 2,
										"/track/11/equalizer/filter/2/active" : 1,
										"/track/11/equalizer/filter/2/freq" : 100.0,
										"/track/11/equalizer/filter/2/gain" : 0.0,
										"/track/11/equalizer/filter/2/q" : 1.0,
										"/track/11/equalizer/filter/3/active" : 1,
										"/track/11/equalizer/filter/3/freq" : 500.0,
										"/track/11/equalizer/filter/3/gain" : 0.0,
										"/track/11/equalizer/filter/3/q" : 1.0,
										"/track/11/equalizer/filter/4/active" : 1,
										"/track/11/equalizer/filter/4/freq" : 1000.0,
										"/track/11/equalizer/filter/4/gain" : 0.0,
										"/track/11/equalizer/filter/4/q" : 1.0,
										"/track/11/equalizer/filter/5/active" : 1,
										"/track/11/equalizer/filter/5/freq" : 2000.0,
										"/track/11/equalizer/filter/5/gain" : 0.0,
										"/track/11/equalizer/filter/5/q" : 1.0,
										"/track/11/equalizer/filter/6/active" : 1,
										"/track/11/equalizer/filter/6/freq" : 5000.0,
										"/track/11/equalizer/filter/6/gain" : 0.0,
										"/track/11/equalizer/filter/6/q" : 1.0,
										"/track/11/equalizer/filter/7/active" : 1,
										"/track/11/equalizer/filter/7/freq" : 10000.0,
										"/track/11/equalizer/filter/7/gain" : 0.0,
										"/track/11/equalizer/filter/7/q" : 1.0,
										"/track/11/equalizer/filter/8/active" : 0,
										"/track/11/equalizer/filter/8/freq" : 16000.0,
										"/track/11/equalizer/filter/8/order" : 2,
										"/track/11/gain" : 0.0,
										"/track/11/gain/ramptime" : 20.0,
										"/track/11/mute" : 0,
										"/track/11/annotation" : "",
										"/track/11/levels/input/visible" : 1,
										"/track/11/levels/input/post" : 0,
										"/track/11/levels/input/mode" : "peak",
										"/track/11/levels/output/visible" : 1,
										"/track/11/levels/output/post" : 1,
										"/track/11/levels/output/mode" : "peak",
										"/track/11/solo" : 0,
										"/track/11/bus/A/destination" : "/bus/1",
										"/track/11/bus/B/destination" : "/",
										"/track/11/bus/C/destination" : "/",
										"/track/11/bus/D/destination" : "/",
										"/track/11/bus/E/destination" : "/",
										"/track/11/bus/F/destination" : "/",
										"/track/11/bus/A/mute" : 0,
										"/track/11/bus/B/mute" : 0,
										"/track/11/bus/C/mute" : 0,
										"/track/11/bus/D/mute" : 0,
										"/track/11/bus/E/mute" : 0,
										"/track/11/bus/F/mute" : 0,
										"/track/11/bus/A/gain" : 0.0,
										"/track/11/bus/B/gain" : 0.0,
										"/track/11/bus/C/gain" : 0.0,
										"/track/11/bus/D/gain" : 0.0,
										"/track/11/bus/E/gain" : 0.0,
										"/track/11/bus/F/gain" : 0.0,
										"/track/11/bus/display" : "A",
										"/track/11/routing/input/1/adc" : 11,
										"/track/11/lfe/send" : 0.0,
										"/track/11/lfe/mute" : 0,
										"/track/11/doppler" : 0,
										"/track/11/air" : 0,
										"/track/11/phaseinvert" : 0,
										"/track/11/azim" : 90.0,
										"/track/11/elev" : 0.0,
										"/track/11/dist" : 10.0,
										"/track/11/delay/linkedtodistance" : 0,
										"/track/11/direct/linkedtodistance" : 1,
										"/track/11/early/linkedtodistance" : 1,
										"/track/11/reverb/linkedtodistance" : 1,
										"/track/11/delay" : 0.0,
										"/track/11/direct/gain/offset" : 0.0,
										"/track/11/direct/gain" : -20.5,
										"/track/11/direct/gain/low" : 0.0,
										"/track/11/direct/gain/med" : 0.0,
										"/track/11/direct/gain/high" : 0.0,
										"/track/11/direct/freq/low" : 250.0,
										"/track/11/direct/freq/high" : 4000.0,
										"/track/11/direct/filter/bypass" : 1,
										"/track/11/early/gain/offset" : 0.0,
										"/track/11/early/gain" : -28.100000381469727,
										"/track/11/early/gain/low" : 0.0,
										"/track/11/early/gain/med" : 0.0,
										"/track/11/early/gain/high" : 0.0,
										"/track/11/early/freq/low" : 250.0,
										"/track/11/early/freq/high" : 4000.0,
										"/track/11/early/filter/bypass" : 1,
										"/track/11/reverb/send" : -19.399999618530273,
										"/track/11/reverb/send/offset" : 0.0,
										"/track/11/reverb/send/ramptime" : 20.0,
										"/track/11/early/width" : 30.0,
										"/track/11/direct/mute" : 0,
										"/track/11/early/mute" : 0,
										"/track/11/reverb/mute" : 0,
										"/track/11/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/11/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/11/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/11/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/11/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/11/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/11/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/11/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/11/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/11/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/11/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/11/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/11/lock" : 0,
										"/track/12/name" : "Mono 12",
										"/track/12/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 1.0 ],
										"/track/12/numinputs" : 1,
										"/track/12/visible" : 1,
										"/track/12/trim" : 0.0,
										"/track/12/dynamics/attack" : 10.010000228881836,
										"/track/12/dynamics/release" : 30.0,
										"/track/12/dynamics/lookahead" : 0.0,
										"/track/12/dynamics/compressor/threshold" : -30.0,
										"/track/12/dynamics/compressor/ratio" : 1.0,
										"/track/12/dynamics/expander/threshold" : -60.0,
										"/track/12/dynamics/expander/ratio" : 1.0,
										"/track/12/dynamics/makeup" : 0.0,
										"/track/12/dynamics/link" : "multi mono",
										"/track/12/dynamics/bypass" : 1,
										"/track/12/dynamics/grid/visible" : 1,
										"/track/12/dynamics/curve/fill" : 1,
										"/track/12/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/12/dynamics/foreground/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 0.600000023841858 ],
										"/track/12/dynamics/curve/color" : [ 0.800000011920929, 0.776470601558685, 0.094117648899555, 1.0 ],
										"/track/12/dynamics/curve/thickness" : 1.0,
										"/track/12/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/12/dynamics/title" : "Mono 12",
										"/track/12/equalizer/bypass" : 1,
										"/track/12/equalizer/gain" : 0.0,
										"/track/12/equalizer/filter/1/active" : 0,
										"/track/12/equalizer/filter/1/freq" : 50.0,
										"/track/12/equalizer/filter/1/order" : 2,
										"/track/12/equalizer/filter/2/active" : 1,
										"/track/12/equalizer/filter/2/freq" : 100.0,
										"/track/12/equalizer/filter/2/gain" : 0.0,
										"/track/12/equalizer/filter/2/q" : 1.0,
										"/track/12/equalizer/filter/3/active" : 1,
										"/track/12/equalizer/filter/3/freq" : 500.0,
										"/track/12/equalizer/filter/3/gain" : 0.0,
										"/track/12/equalizer/filter/3/q" : 1.0,
										"/track/12/equalizer/filter/4/active" : 1,
										"/track/12/equalizer/filter/4/freq" : 1000.0,
										"/track/12/equalizer/filter/4/gain" : 0.0,
										"/track/12/equalizer/filter/4/q" : 1.0,
										"/track/12/equalizer/filter/5/active" : 1,
										"/track/12/equalizer/filter/5/freq" : 2000.0,
										"/track/12/equalizer/filter/5/gain" : 0.0,
										"/track/12/equalizer/filter/5/q" : 1.0,
										"/track/12/equalizer/filter/6/active" : 1,
										"/track/12/equalizer/filter/6/freq" : 5000.0,
										"/track/12/equalizer/filter/6/gain" : 0.0,
										"/track/12/equalizer/filter/6/q" : 1.0,
										"/track/12/equalizer/filter/7/active" : 1,
										"/track/12/equalizer/filter/7/freq" : 10000.0,
										"/track/12/equalizer/filter/7/gain" : 0.0,
										"/track/12/equalizer/filter/7/q" : 1.0,
										"/track/12/equalizer/filter/8/active" : 0,
										"/track/12/equalizer/filter/8/freq" : 16000.0,
										"/track/12/equalizer/filter/8/order" : 2,
										"/track/12/gain" : 0.0,
										"/track/12/gain/ramptime" : 20.0,
										"/track/12/mute" : 0,
										"/track/12/annotation" : "",
										"/track/12/levels/input/visible" : 1,
										"/track/12/levels/input/post" : 0,
										"/track/12/levels/input/mode" : "peak",
										"/track/12/levels/output/visible" : 1,
										"/track/12/levels/output/post" : 1,
										"/track/12/levels/output/mode" : "peak",
										"/track/12/solo" : 0,
										"/track/12/bus/A/destination" : "/bus/1",
										"/track/12/bus/B/destination" : "/",
										"/track/12/bus/C/destination" : "/",
										"/track/12/bus/D/destination" : "/",
										"/track/12/bus/E/destination" : "/",
										"/track/12/bus/F/destination" : "/",
										"/track/12/bus/A/mute" : 0,
										"/track/12/bus/B/mute" : 0,
										"/track/12/bus/C/mute" : 0,
										"/track/12/bus/D/mute" : 0,
										"/track/12/bus/E/mute" : 0,
										"/track/12/bus/F/mute" : 0,
										"/track/12/bus/A/gain" : 0.0,
										"/track/12/bus/B/gain" : 0.0,
										"/track/12/bus/C/gain" : 0.0,
										"/track/12/bus/D/gain" : 0.0,
										"/track/12/bus/E/gain" : 0.0,
										"/track/12/bus/F/gain" : 0.0,
										"/track/12/bus/display" : "A",
										"/track/12/routing/input/1/adc" : 12,
										"/track/12/lfe/send" : 0.0,
										"/track/12/lfe/mute" : 0,
										"/track/12/doppler" : 0,
										"/track/12/air" : 0,
										"/track/12/phaseinvert" : 0,
										"/track/12/azim" : 135.0,
										"/track/12/elev" : 0.0,
										"/track/12/dist" : 10.0,
										"/track/12/delay/linkedtodistance" : 0,
										"/track/12/direct/linkedtodistance" : 1,
										"/track/12/early/linkedtodistance" : 1,
										"/track/12/reverb/linkedtodistance" : 1,
										"/track/12/delay" : 0.0,
										"/track/12/direct/gain/offset" : 0.0,
										"/track/12/direct/gain" : -20.5,
										"/track/12/direct/gain/low" : 0.0,
										"/track/12/direct/gain/med" : 0.0,
										"/track/12/direct/gain/high" : 0.0,
										"/track/12/direct/freq/low" : 250.0,
										"/track/12/direct/freq/high" : 4000.0,
										"/track/12/direct/filter/bypass" : 1,
										"/track/12/early/gain/offset" : 0.0,
										"/track/12/early/gain" : -28.100000381469727,
										"/track/12/early/gain/low" : 0.0,
										"/track/12/early/gain/med" : 0.0,
										"/track/12/early/gain/high" : 0.0,
										"/track/12/early/freq/low" : 250.0,
										"/track/12/early/freq/high" : 4000.0,
										"/track/12/early/filter/bypass" : 1,
										"/track/12/reverb/send" : -19.399999618530273,
										"/track/12/reverb/send/offset" : 0.0,
										"/track/12/reverb/send/ramptime" : 20.0,
										"/track/12/early/width" : 30.0,
										"/track/12/direct/mute" : 0,
										"/track/12/early/mute" : 0,
										"/track/12/reverb/mute" : 0,
										"/track/12/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/12/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/12/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/12/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/12/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/12/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/12/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/12/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/12/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/12/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/12/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/12/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/12/lock" : 0,
										"/track/13/name" : "Mono 13",
										"/track/13/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 1.0 ],
										"/track/13/numinputs" : 1,
										"/track/13/visible" : 1,
										"/track/13/trim" : 0.0,
										"/track/13/dynamics/attack" : 10.010000228881836,
										"/track/13/dynamics/release" : 30.0,
										"/track/13/dynamics/lookahead" : 0.0,
										"/track/13/dynamics/compressor/threshold" : -30.0,
										"/track/13/dynamics/compressor/ratio" : 1.0,
										"/track/13/dynamics/expander/threshold" : -60.0,
										"/track/13/dynamics/expander/ratio" : 1.0,
										"/track/13/dynamics/makeup" : 0.0,
										"/track/13/dynamics/link" : "multi mono",
										"/track/13/dynamics/bypass" : 1,
										"/track/13/dynamics/grid/visible" : 1,
										"/track/13/dynamics/curve/fill" : 1,
										"/track/13/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/13/dynamics/foreground/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 0.600000023841858 ],
										"/track/13/dynamics/curve/color" : [ 0.800000011920929, 0.776470601558685, 0.094117648899555, 1.0 ],
										"/track/13/dynamics/curve/thickness" : 1.0,
										"/track/13/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/13/dynamics/title" : "Mono 13",
										"/track/13/equalizer/bypass" : 1,
										"/track/13/equalizer/gain" : 0.0,
										"/track/13/equalizer/filter/1/active" : 0,
										"/track/13/equalizer/filter/1/freq" : 50.0,
										"/track/13/equalizer/filter/1/order" : 2,
										"/track/13/equalizer/filter/2/active" : 1,
										"/track/13/equalizer/filter/2/freq" : 100.0,
										"/track/13/equalizer/filter/2/gain" : 0.0,
										"/track/13/equalizer/filter/2/q" : 1.0,
										"/track/13/equalizer/filter/3/active" : 1,
										"/track/13/equalizer/filter/3/freq" : 500.0,
										"/track/13/equalizer/filter/3/gain" : 0.0,
										"/track/13/equalizer/filter/3/q" : 1.0,
										"/track/13/equalizer/filter/4/active" : 1,
										"/track/13/equalizer/filter/4/freq" : 1000.0,
										"/track/13/equalizer/filter/4/gain" : 0.0,
										"/track/13/equalizer/filter/4/q" : 1.0,
										"/track/13/equalizer/filter/5/active" : 1,
										"/track/13/equalizer/filter/5/freq" : 2000.0,
										"/track/13/equalizer/filter/5/gain" : 0.0,
										"/track/13/equalizer/filter/5/q" : 1.0,
										"/track/13/equalizer/filter/6/active" : 1,
										"/track/13/equalizer/filter/6/freq" : 5000.0,
										"/track/13/equalizer/filter/6/gain" : 0.0,
										"/track/13/equalizer/filter/6/q" : 1.0,
										"/track/13/equalizer/filter/7/active" : 1,
										"/track/13/equalizer/filter/7/freq" : 10000.0,
										"/track/13/equalizer/filter/7/gain" : 0.0,
										"/track/13/equalizer/filter/7/q" : 1.0,
										"/track/13/equalizer/filter/8/active" : 0,
										"/track/13/equalizer/filter/8/freq" : 16000.0,
										"/track/13/equalizer/filter/8/order" : 2,
										"/track/13/gain" : 0.0,
										"/track/13/gain/ramptime" : 20.0,
										"/track/13/mute" : 0,
										"/track/13/annotation" : "",
										"/track/13/levels/input/visible" : 1,
										"/track/13/levels/input/post" : 0,
										"/track/13/levels/input/mode" : "peak",
										"/track/13/levels/output/visible" : 1,
										"/track/13/levels/output/post" : 1,
										"/track/13/levels/output/mode" : "peak",
										"/track/13/solo" : 0,
										"/track/13/bus/A/destination" : "/bus/1",
										"/track/13/bus/B/destination" : "/",
										"/track/13/bus/C/destination" : "/",
										"/track/13/bus/D/destination" : "/",
										"/track/13/bus/E/destination" : "/",
										"/track/13/bus/F/destination" : "/",
										"/track/13/bus/A/mute" : 0,
										"/track/13/bus/B/mute" : 0,
										"/track/13/bus/C/mute" : 0,
										"/track/13/bus/D/mute" : 0,
										"/track/13/bus/E/mute" : 0,
										"/track/13/bus/F/mute" : 0,
										"/track/13/bus/A/gain" : 0.0,
										"/track/13/bus/B/gain" : 0.0,
										"/track/13/bus/C/gain" : 0.0,
										"/track/13/bus/D/gain" : 0.0,
										"/track/13/bus/E/gain" : 0.0,
										"/track/13/bus/F/gain" : 0.0,
										"/track/13/bus/display" : "A",
										"/track/13/routing/input/1/adc" : 13,
										"/track/13/lfe/send" : 0.0,
										"/track/13/lfe/mute" : 0,
										"/track/13/doppler" : 0,
										"/track/13/air" : 0,
										"/track/13/phaseinvert" : 0,
										"/track/13/azim" : -180.0,
										"/track/13/elev" : 0.0,
										"/track/13/dist" : 10.0,
										"/track/13/delay/linkedtodistance" : 0,
										"/track/13/direct/linkedtodistance" : 1,
										"/track/13/early/linkedtodistance" : 1,
										"/track/13/reverb/linkedtodistance" : 1,
										"/track/13/delay" : 0.0,
										"/track/13/direct/gain/offset" : 0.0,
										"/track/13/direct/gain" : -20.5,
										"/track/13/direct/gain/low" : 0.0,
										"/track/13/direct/gain/med" : 0.0,
										"/track/13/direct/gain/high" : 0.0,
										"/track/13/direct/freq/low" : 250.0,
										"/track/13/direct/freq/high" : 4000.0,
										"/track/13/direct/filter/bypass" : 1,
										"/track/13/early/gain/offset" : 0.0,
										"/track/13/early/gain" : -28.100000381469727,
										"/track/13/early/gain/low" : 0.0,
										"/track/13/early/gain/med" : 0.0,
										"/track/13/early/gain/high" : 0.0,
										"/track/13/early/freq/low" : 250.0,
										"/track/13/early/freq/high" : 4000.0,
										"/track/13/early/filter/bypass" : 1,
										"/track/13/reverb/send" : -19.399999618530273,
										"/track/13/reverb/send/offset" : 0.0,
										"/track/13/reverb/send/ramptime" : 20.0,
										"/track/13/early/width" : 30.0,
										"/track/13/direct/mute" : 0,
										"/track/13/early/mute" : 0,
										"/track/13/reverb/mute" : 0,
										"/track/13/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/13/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/13/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/13/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/13/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/13/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/13/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/13/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/13/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/13/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/13/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/13/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/13/lock" : 0,
										"/track/14/name" : "Mono 14",
										"/track/14/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 1.0 ],
										"/track/14/numinputs" : 1,
										"/track/14/visible" : 1,
										"/track/14/trim" : 0.0,
										"/track/14/dynamics/attack" : 10.010000228881836,
										"/track/14/dynamics/release" : 30.0,
										"/track/14/dynamics/lookahead" : 0.0,
										"/track/14/dynamics/compressor/threshold" : -30.0,
										"/track/14/dynamics/compressor/ratio" : 1.0,
										"/track/14/dynamics/expander/threshold" : -60.0,
										"/track/14/dynamics/expander/ratio" : 1.0,
										"/track/14/dynamics/makeup" : 0.0,
										"/track/14/dynamics/link" : "multi mono",
										"/track/14/dynamics/bypass" : 1,
										"/track/14/dynamics/grid/visible" : 1,
										"/track/14/dynamics/curve/fill" : 1,
										"/track/14/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/14/dynamics/foreground/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 0.600000023841858 ],
										"/track/14/dynamics/curve/color" : [ 0.800000011920929, 0.776470601558685, 0.094117648899555, 1.0 ],
										"/track/14/dynamics/curve/thickness" : 1.0,
										"/track/14/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/14/dynamics/title" : "Mono 14",
										"/track/14/equalizer/bypass" : 1,
										"/track/14/equalizer/gain" : 0.0,
										"/track/14/equalizer/filter/1/active" : 0,
										"/track/14/equalizer/filter/1/freq" : 50.0,
										"/track/14/equalizer/filter/1/order" : 2,
										"/track/14/equalizer/filter/2/active" : 1,
										"/track/14/equalizer/filter/2/freq" : 100.0,
										"/track/14/equalizer/filter/2/gain" : 0.0,
										"/track/14/equalizer/filter/2/q" : 1.0,
										"/track/14/equalizer/filter/3/active" : 1,
										"/track/14/equalizer/filter/3/freq" : 500.0,
										"/track/14/equalizer/filter/3/gain" : 0.0,
										"/track/14/equalizer/filter/3/q" : 1.0,
										"/track/14/equalizer/filter/4/active" : 1,
										"/track/14/equalizer/filter/4/freq" : 1000.0,
										"/track/14/equalizer/filter/4/gain" : 0.0,
										"/track/14/equalizer/filter/4/q" : 1.0,
										"/track/14/equalizer/filter/5/active" : 1,
										"/track/14/equalizer/filter/5/freq" : 2000.0,
										"/track/14/equalizer/filter/5/gain" : 0.0,
										"/track/14/equalizer/filter/5/q" : 1.0,
										"/track/14/equalizer/filter/6/active" : 1,
										"/track/14/equalizer/filter/6/freq" : 5000.0,
										"/track/14/equalizer/filter/6/gain" : 0.0,
										"/track/14/equalizer/filter/6/q" : 1.0,
										"/track/14/equalizer/filter/7/active" : 1,
										"/track/14/equalizer/filter/7/freq" : 10000.0,
										"/track/14/equalizer/filter/7/gain" : 0.0,
										"/track/14/equalizer/filter/7/q" : 1.0,
										"/track/14/equalizer/filter/8/active" : 0,
										"/track/14/equalizer/filter/8/freq" : 16000.0,
										"/track/14/equalizer/filter/8/order" : 2,
										"/track/14/gain" : 0.0,
										"/track/14/gain/ramptime" : 20.0,
										"/track/14/mute" : 0,
										"/track/14/annotation" : "",
										"/track/14/levels/input/visible" : 1,
										"/track/14/levels/input/post" : 0,
										"/track/14/levels/input/mode" : "peak",
										"/track/14/levels/output/visible" : 1,
										"/track/14/levels/output/post" : 1,
										"/track/14/levels/output/mode" : "peak",
										"/track/14/solo" : 0,
										"/track/14/bus/A/destination" : "/bus/1",
										"/track/14/bus/B/destination" : "/",
										"/track/14/bus/C/destination" : "/",
										"/track/14/bus/D/destination" : "/",
										"/track/14/bus/E/destination" : "/",
										"/track/14/bus/F/destination" : "/",
										"/track/14/bus/A/mute" : 0,
										"/track/14/bus/B/mute" : 0,
										"/track/14/bus/C/mute" : 0,
										"/track/14/bus/D/mute" : 0,
										"/track/14/bus/E/mute" : 0,
										"/track/14/bus/F/mute" : 0,
										"/track/14/bus/A/gain" : 0.0,
										"/track/14/bus/B/gain" : 0.0,
										"/track/14/bus/C/gain" : 0.0,
										"/track/14/bus/D/gain" : 0.0,
										"/track/14/bus/E/gain" : 0.0,
										"/track/14/bus/F/gain" : 0.0,
										"/track/14/bus/display" : "A",
										"/track/14/routing/input/1/adc" : 14,
										"/track/14/lfe/send" : 0.0,
										"/track/14/lfe/mute" : 0,
										"/track/14/doppler" : 0,
										"/track/14/air" : 0,
										"/track/14/phaseinvert" : 0,
										"/track/14/azim" : -135.0,
										"/track/14/elev" : 0.0,
										"/track/14/dist" : 10.0,
										"/track/14/delay/linkedtodistance" : 0,
										"/track/14/direct/linkedtodistance" : 1,
										"/track/14/early/linkedtodistance" : 1,
										"/track/14/reverb/linkedtodistance" : 1,
										"/track/14/delay" : 0.0,
										"/track/14/direct/gain/offset" : 0.0,
										"/track/14/direct/gain" : -20.5,
										"/track/14/direct/gain/low" : 0.0,
										"/track/14/direct/gain/med" : 0.0,
										"/track/14/direct/gain/high" : 0.0,
										"/track/14/direct/freq/low" : 250.0,
										"/track/14/direct/freq/high" : 4000.0,
										"/track/14/direct/filter/bypass" : 1,
										"/track/14/early/gain/offset" : 0.0,
										"/track/14/early/gain" : -28.100000381469727,
										"/track/14/early/gain/low" : 0.0,
										"/track/14/early/gain/med" : 0.0,
										"/track/14/early/gain/high" : 0.0,
										"/track/14/early/freq/low" : 250.0,
										"/track/14/early/freq/high" : 4000.0,
										"/track/14/early/filter/bypass" : 1,
										"/track/14/reverb/send" : -19.399999618530273,
										"/track/14/reverb/send/offset" : 0.0,
										"/track/14/reverb/send/ramptime" : 20.0,
										"/track/14/early/width" : 30.0,
										"/track/14/direct/mute" : 0,
										"/track/14/early/mute" : 0,
										"/track/14/reverb/mute" : 0,
										"/track/14/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/14/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/14/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/14/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/14/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/14/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/14/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/14/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/14/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/14/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/14/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/14/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/14/lock" : 0,
										"/track/15/name" : "Mono 15",
										"/track/15/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 1.0 ],
										"/track/15/numinputs" : 1,
										"/track/15/visible" : 1,
										"/track/15/trim" : 0.0,
										"/track/15/dynamics/attack" : 10.010000228881836,
										"/track/15/dynamics/release" : 30.0,
										"/track/15/dynamics/lookahead" : 0.0,
										"/track/15/dynamics/compressor/threshold" : -30.0,
										"/track/15/dynamics/compressor/ratio" : 1.0,
										"/track/15/dynamics/expander/threshold" : -60.0,
										"/track/15/dynamics/expander/ratio" : 1.0,
										"/track/15/dynamics/makeup" : 0.0,
										"/track/15/dynamics/link" : "multi mono",
										"/track/15/dynamics/bypass" : 1,
										"/track/15/dynamics/grid/visible" : 1,
										"/track/15/dynamics/curve/fill" : 1,
										"/track/15/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/15/dynamics/foreground/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 0.600000023841858 ],
										"/track/15/dynamics/curve/color" : [ 0.800000011920929, 0.776470601558685, 0.094117648899555, 1.0 ],
										"/track/15/dynamics/curve/thickness" : 1.0,
										"/track/15/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/15/dynamics/title" : "Mono 15",
										"/track/15/equalizer/bypass" : 1,
										"/track/15/equalizer/gain" : 0.0,
										"/track/15/equalizer/filter/1/active" : 0,
										"/track/15/equalizer/filter/1/freq" : 50.0,
										"/track/15/equalizer/filter/1/order" : 2,
										"/track/15/equalizer/filter/2/active" : 1,
										"/track/15/equalizer/filter/2/freq" : 100.0,
										"/track/15/equalizer/filter/2/gain" : 0.0,
										"/track/15/equalizer/filter/2/q" : 1.0,
										"/track/15/equalizer/filter/3/active" : 1,
										"/track/15/equalizer/filter/3/freq" : 500.0,
										"/track/15/equalizer/filter/3/gain" : 0.0,
										"/track/15/equalizer/filter/3/q" : 1.0,
										"/track/15/equalizer/filter/4/active" : 1,
										"/track/15/equalizer/filter/4/freq" : 1000.0,
										"/track/15/equalizer/filter/4/gain" : 0.0,
										"/track/15/equalizer/filter/4/q" : 1.0,
										"/track/15/equalizer/filter/5/active" : 1,
										"/track/15/equalizer/filter/5/freq" : 2000.0,
										"/track/15/equalizer/filter/5/gain" : 0.0,
										"/track/15/equalizer/filter/5/q" : 1.0,
										"/track/15/equalizer/filter/6/active" : 1,
										"/track/15/equalizer/filter/6/freq" : 5000.0,
										"/track/15/equalizer/filter/6/gain" : 0.0,
										"/track/15/equalizer/filter/6/q" : 1.0,
										"/track/15/equalizer/filter/7/active" : 1,
										"/track/15/equalizer/filter/7/freq" : 10000.0,
										"/track/15/equalizer/filter/7/gain" : 0.0,
										"/track/15/equalizer/filter/7/q" : 1.0,
										"/track/15/equalizer/filter/8/active" : 0,
										"/track/15/equalizer/filter/8/freq" : 16000.0,
										"/track/15/equalizer/filter/8/order" : 2,
										"/track/15/gain" : 0.0,
										"/track/15/gain/ramptime" : 20.0,
										"/track/15/mute" : 0,
										"/track/15/annotation" : "",
										"/track/15/levels/input/visible" : 1,
										"/track/15/levels/input/post" : 0,
										"/track/15/levels/input/mode" : "peak",
										"/track/15/levels/output/visible" : 1,
										"/track/15/levels/output/post" : 1,
										"/track/15/levels/output/mode" : "peak",
										"/track/15/solo" : 0,
										"/track/15/bus/A/destination" : "/bus/1",
										"/track/15/bus/B/destination" : "/",
										"/track/15/bus/C/destination" : "/",
										"/track/15/bus/D/destination" : "/",
										"/track/15/bus/E/destination" : "/",
										"/track/15/bus/F/destination" : "/",
										"/track/15/bus/A/mute" : 0,
										"/track/15/bus/B/mute" : 0,
										"/track/15/bus/C/mute" : 0,
										"/track/15/bus/D/mute" : 0,
										"/track/15/bus/E/mute" : 0,
										"/track/15/bus/F/mute" : 0,
										"/track/15/bus/A/gain" : 0.0,
										"/track/15/bus/B/gain" : 0.0,
										"/track/15/bus/C/gain" : 0.0,
										"/track/15/bus/D/gain" : 0.0,
										"/track/15/bus/E/gain" : 0.0,
										"/track/15/bus/F/gain" : 0.0,
										"/track/15/bus/display" : "A",
										"/track/15/routing/input/1/adc" : 15,
										"/track/15/lfe/send" : 0.0,
										"/track/15/lfe/mute" : 0,
										"/track/15/doppler" : 0,
										"/track/15/air" : 0,
										"/track/15/phaseinvert" : 0,
										"/track/15/azim" : -90.0,
										"/track/15/elev" : 0.0,
										"/track/15/dist" : 10.0,
										"/track/15/delay/linkedtodistance" : 0,
										"/track/15/direct/linkedtodistance" : 1,
										"/track/15/early/linkedtodistance" : 1,
										"/track/15/reverb/linkedtodistance" : 1,
										"/track/15/delay" : 0.0,
										"/track/15/direct/gain/offset" : 0.0,
										"/track/15/direct/gain" : -20.5,
										"/track/15/direct/gain/low" : 0.0,
										"/track/15/direct/gain/med" : 0.0,
										"/track/15/direct/gain/high" : 0.0,
										"/track/15/direct/freq/low" : 250.0,
										"/track/15/direct/freq/high" : 4000.0,
										"/track/15/direct/filter/bypass" : 1,
										"/track/15/early/gain/offset" : 0.0,
										"/track/15/early/gain" : -28.100000381469727,
										"/track/15/early/gain/low" : 0.0,
										"/track/15/early/gain/med" : 0.0,
										"/track/15/early/gain/high" : 0.0,
										"/track/15/early/freq/low" : 250.0,
										"/track/15/early/freq/high" : 4000.0,
										"/track/15/early/filter/bypass" : 1,
										"/track/15/reverb/send" : -19.399999618530273,
										"/track/15/reverb/send/offset" : 0.0,
										"/track/15/reverb/send/ramptime" : 20.0,
										"/track/15/early/width" : 30.0,
										"/track/15/direct/mute" : 0,
										"/track/15/early/mute" : 0,
										"/track/15/reverb/mute" : 0,
										"/track/15/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/15/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/15/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/15/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/15/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/15/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/15/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/15/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/15/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/15/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/15/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/15/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/15/lock" : 0,
										"/track/16/name" : "Mono 16",
										"/track/16/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 1.0 ],
										"/track/16/numinputs" : 1,
										"/track/16/visible" : 1,
										"/track/16/trim" : 0.0,
										"/track/16/dynamics/attack" : 10.010000228881836,
										"/track/16/dynamics/release" : 30.0,
										"/track/16/dynamics/lookahead" : 0.0,
										"/track/16/dynamics/compressor/threshold" : -30.0,
										"/track/16/dynamics/compressor/ratio" : 1.0,
										"/track/16/dynamics/expander/threshold" : -60.0,
										"/track/16/dynamics/expander/ratio" : 1.0,
										"/track/16/dynamics/makeup" : 0.0,
										"/track/16/dynamics/link" : "multi mono",
										"/track/16/dynamics/bypass" : 1,
										"/track/16/dynamics/grid/visible" : 1,
										"/track/16/dynamics/curve/fill" : 1,
										"/track/16/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/track/16/dynamics/foreground/color" : [ 1.0, 0.972549021244049, 0.117647059261799, 0.600000023841858 ],
										"/track/16/dynamics/curve/color" : [ 0.800000011920929, 0.776470601558685, 0.094117648899555, 1.0 ],
										"/track/16/dynamics/curve/thickness" : 1.0,
										"/track/16/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/track/16/dynamics/title" : "Mono 16",
										"/track/16/equalizer/bypass" : 1,
										"/track/16/equalizer/gain" : 0.0,
										"/track/16/equalizer/filter/1/active" : 0,
										"/track/16/equalizer/filter/1/freq" : 50.0,
										"/track/16/equalizer/filter/1/order" : 2,
										"/track/16/equalizer/filter/2/active" : 1,
										"/track/16/equalizer/filter/2/freq" : 100.0,
										"/track/16/equalizer/filter/2/gain" : 0.0,
										"/track/16/equalizer/filter/2/q" : 1.0,
										"/track/16/equalizer/filter/3/active" : 1,
										"/track/16/equalizer/filter/3/freq" : 500.0,
										"/track/16/equalizer/filter/3/gain" : 0.0,
										"/track/16/equalizer/filter/3/q" : 1.0,
										"/track/16/equalizer/filter/4/active" : 1,
										"/track/16/equalizer/filter/4/freq" : 1000.0,
										"/track/16/equalizer/filter/4/gain" : 0.0,
										"/track/16/equalizer/filter/4/q" : 1.0,
										"/track/16/equalizer/filter/5/active" : 1,
										"/track/16/equalizer/filter/5/freq" : 2000.0,
										"/track/16/equalizer/filter/5/gain" : 0.0,
										"/track/16/equalizer/filter/5/q" : 1.0,
										"/track/16/equalizer/filter/6/active" : 1,
										"/track/16/equalizer/filter/6/freq" : 5000.0,
										"/track/16/equalizer/filter/6/gain" : 0.0,
										"/track/16/equalizer/filter/6/q" : 1.0,
										"/track/16/equalizer/filter/7/active" : 1,
										"/track/16/equalizer/filter/7/freq" : 10000.0,
										"/track/16/equalizer/filter/7/gain" : 0.0,
										"/track/16/equalizer/filter/7/q" : 1.0,
										"/track/16/equalizer/filter/8/active" : 0,
										"/track/16/equalizer/filter/8/freq" : 16000.0,
										"/track/16/equalizer/filter/8/order" : 2,
										"/track/16/gain" : 0.0,
										"/track/16/gain/ramptime" : 20.0,
										"/track/16/mute" : 0,
										"/track/16/annotation" : "",
										"/track/16/levels/input/visible" : 1,
										"/track/16/levels/input/post" : 0,
										"/track/16/levels/input/mode" : "peak",
										"/track/16/levels/output/visible" : 1,
										"/track/16/levels/output/post" : 1,
										"/track/16/levels/output/mode" : "peak",
										"/track/16/solo" : 0,
										"/track/16/bus/A/destination" : "/bus/1",
										"/track/16/bus/B/destination" : "/",
										"/track/16/bus/C/destination" : "/",
										"/track/16/bus/D/destination" : "/",
										"/track/16/bus/E/destination" : "/",
										"/track/16/bus/F/destination" : "/",
										"/track/16/bus/A/mute" : 0,
										"/track/16/bus/B/mute" : 0,
										"/track/16/bus/C/mute" : 0,
										"/track/16/bus/D/mute" : 0,
										"/track/16/bus/E/mute" : 0,
										"/track/16/bus/F/mute" : 0,
										"/track/16/bus/A/gain" : 0.0,
										"/track/16/bus/B/gain" : 0.0,
										"/track/16/bus/C/gain" : 0.0,
										"/track/16/bus/D/gain" : 0.0,
										"/track/16/bus/E/gain" : 0.0,
										"/track/16/bus/F/gain" : 0.0,
										"/track/16/bus/display" : "A",
										"/track/16/routing/input/1/adc" : 16,
										"/track/16/lfe/send" : 0.0,
										"/track/16/lfe/mute" : 0,
										"/track/16/doppler" : 0,
										"/track/16/air" : 0,
										"/track/16/phaseinvert" : 0,
										"/track/16/azim" : -45.0,
										"/track/16/elev" : 0.0,
										"/track/16/dist" : 10.0,
										"/track/16/delay/linkedtodistance" : 0,
										"/track/16/direct/linkedtodistance" : 1,
										"/track/16/early/linkedtodistance" : 1,
										"/track/16/reverb/linkedtodistance" : 1,
										"/track/16/delay" : 0.0,
										"/track/16/direct/gain/offset" : 0.0,
										"/track/16/direct/gain" : -20.5,
										"/track/16/direct/gain/low" : 0.0,
										"/track/16/direct/gain/med" : 0.0,
										"/track/16/direct/gain/high" : 0.0,
										"/track/16/direct/freq/low" : 250.0,
										"/track/16/direct/freq/high" : 4000.0,
										"/track/16/direct/filter/bypass" : 1,
										"/track/16/early/gain/offset" : 0.0,
										"/track/16/early/gain" : -28.100000381469727,
										"/track/16/early/gain/low" : 0.0,
										"/track/16/early/gain/med" : 0.0,
										"/track/16/early/gain/high" : 0.0,
										"/track/16/early/freq/low" : 250.0,
										"/track/16/early/freq/high" : 4000.0,
										"/track/16/early/filter/bypass" : 1,
										"/track/16/reverb/send" : -19.399999618530273,
										"/track/16/reverb/send/offset" : 0.0,
										"/track/16/reverb/send/ramptime" : 20.0,
										"/track/16/early/width" : 30.0,
										"/track/16/direct/mute" : 0,
										"/track/16/early/mute" : 0,
										"/track/16/reverb/mute" : 0,
										"/track/16/bus/A/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/16/bus/A/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/16/bus/B/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/16/bus/B/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/16/bus/C/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/16/bus/C/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/16/bus/D/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/16/bus/D/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/16/bus/E/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/16/bus/E/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/16/bus/F/early/delays" : [ 22.219999313354492, 25.296899795532227, 26.636100769042969, 28.912599563598633, 32.273998260498047, 34.406299591064453, 36.777000427246094, 39.709999084472656 ],
										"/track/16/bus/F/early/gains" : [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 ],
										"/track/16/lock" : 0,
										"/hoastream/1/name" : "HoaStream 1",
										"/hoastream/1/color" : [ 0.960784316062927, 1.0, 0.980392158031464, 1.0 ],
										"/hoastream/1/numinputs" : 16,
										"/hoastream/1/visible" : 1,
										"/hoastream/1/trim" : 0.0,
										"/hoastream/1/dynamics/attack" : 10.010000228881836,
										"/hoastream/1/dynamics/release" : 30.0,
										"/hoastream/1/dynamics/lookahead" : 0.0,
										"/hoastream/1/dynamics/compressor/threshold" : -30.0,
										"/hoastream/1/dynamics/compressor/ratio" : 1.0,
										"/hoastream/1/dynamics/expander/threshold" : -60.0,
										"/hoastream/1/dynamics/expander/ratio" : 1.0,
										"/hoastream/1/dynamics/makeup" : 0.0,
										"/hoastream/1/dynamics/link" : "multi mono",
										"/hoastream/1/dynamics/bypass" : 1,
										"/hoastream/1/dynamics/grid/visible" : 1,
										"/hoastream/1/dynamics/curve/fill" : 1,
										"/hoastream/1/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/hoastream/1/dynamics/foreground/color" : [ 0.960784316062927, 1.0, 0.980392158031464, 0.600000023841858 ],
										"/hoastream/1/dynamics/curve/color" : [ 0.768627464771271, 0.800000011920929, 0.7843137383461, 1.0 ],
										"/hoastream/1/dynamics/curve/thickness" : 1.0,
										"/hoastream/1/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/hoastream/1/dynamics/title" : "HoaStream 1",
										"/hoastream/1/equalizer/bypass" : 1,
										"/hoastream/1/equalizer/gain" : 0.0,
										"/hoastream/1/equalizer/filter/1/active" : 0,
										"/hoastream/1/equalizer/filter/1/freq" : 50.0,
										"/hoastream/1/equalizer/filter/1/order" : 2,
										"/hoastream/1/equalizer/filter/2/active" : 1,
										"/hoastream/1/equalizer/filter/2/freq" : 100.0,
										"/hoastream/1/equalizer/filter/2/gain" : 0.0,
										"/hoastream/1/equalizer/filter/2/q" : 1.0,
										"/hoastream/1/equalizer/filter/3/active" : 1,
										"/hoastream/1/equalizer/filter/3/freq" : 500.0,
										"/hoastream/1/equalizer/filter/3/gain" : 0.0,
										"/hoastream/1/equalizer/filter/3/q" : 1.0,
										"/hoastream/1/equalizer/filter/4/active" : 1,
										"/hoastream/1/equalizer/filter/4/freq" : 1000.0,
										"/hoastream/1/equalizer/filter/4/gain" : 0.0,
										"/hoastream/1/equalizer/filter/4/q" : 1.0,
										"/hoastream/1/equalizer/filter/5/active" : 1,
										"/hoastream/1/equalizer/filter/5/freq" : 2000.0,
										"/hoastream/1/equalizer/filter/5/gain" : 0.0,
										"/hoastream/1/equalizer/filter/5/q" : 1.0,
										"/hoastream/1/equalizer/filter/6/active" : 1,
										"/hoastream/1/equalizer/filter/6/freq" : 5000.0,
										"/hoastream/1/equalizer/filter/6/gain" : 0.0,
										"/hoastream/1/equalizer/filter/6/q" : 1.0,
										"/hoastream/1/equalizer/filter/7/active" : 1,
										"/hoastream/1/equalizer/filter/7/freq" : 10000.0,
										"/hoastream/1/equalizer/filter/7/gain" : 0.0,
										"/hoastream/1/equalizer/filter/7/q" : 1.0,
										"/hoastream/1/equalizer/filter/8/active" : 0,
										"/hoastream/1/equalizer/filter/8/freq" : 16000.0,
										"/hoastream/1/equalizer/filter/8/order" : 2,
										"/hoastream/1/gain" : 0.0,
										"/hoastream/1/gain/ramptime" : 20.0,
										"/hoastream/1/mute" : 0,
										"/hoastream/1/annotation" : "",
										"/hoastream/1/levels/input/visible" : 1,
										"/hoastream/1/levels/input/post" : 0,
										"/hoastream/1/levels/input/mode" : "peak",
										"/hoastream/1/levels/output/visible" : 1,
										"/hoastream/1/levels/output/post" : 1,
										"/hoastream/1/levels/output/mode" : "peak",
										"/hoastream/1/solo" : 0,
										"/hoastream/1/bus/A/destination" : "/bus/1",
										"/hoastream/1/bus/B/destination" : "/",
										"/hoastream/1/bus/C/destination" : "/",
										"/hoastream/1/bus/D/destination" : "/",
										"/hoastream/1/bus/E/destination" : "/",
										"/hoastream/1/bus/F/destination" : "/",
										"/hoastream/1/bus/A/mute" : 0,
										"/hoastream/1/bus/B/mute" : 0,
										"/hoastream/1/bus/C/mute" : 0,
										"/hoastream/1/bus/D/mute" : 0,
										"/hoastream/1/bus/E/mute" : 0,
										"/hoastream/1/bus/F/mute" : 0,
										"/hoastream/1/bus/A/gain" : 0.0,
										"/hoastream/1/bus/B/gain" : 0.0,
										"/hoastream/1/bus/C/gain" : 0.0,
										"/hoastream/1/bus/D/gain" : 0.0,
										"/hoastream/1/bus/E/gain" : 0.0,
										"/hoastream/1/bus/F/gain" : 0.0,
										"/hoastream/1/bus/display" : "A",
										"/hoastream/1/routing/input/1/adc" : 17,
										"/hoastream/1/routing/input/2/adc" : 18,
										"/hoastream/1/routing/input/3/adc" : 19,
										"/hoastream/1/routing/input/4/adc" : 20,
										"/hoastream/1/routing/input/5/adc" : 21,
										"/hoastream/1/routing/input/6/adc" : 22,
										"/hoastream/1/routing/input/7/adc" : 23,
										"/hoastream/1/routing/input/8/adc" : 24,
										"/hoastream/1/routing/input/9/adc" : 25,
										"/hoastream/1/routing/input/10/adc" : 26,
										"/hoastream/1/routing/input/11/adc" : 27,
										"/hoastream/1/routing/input/12/adc" : 28,
										"/hoastream/1/routing/input/13/adc" : 29,
										"/hoastream/1/routing/input/14/adc" : 30,
										"/hoastream/1/routing/input/15/adc" : 31,
										"/hoastream/1/routing/input/16/adc" : 32,
										"/hoastream/1/lfe/send" : 0.0,
										"/hoastream/1/lfe/mute" : 0,
										"/hoastream/1/delay" : 0.0,
										"/hoastream/1/delay/bypass" : 0,
										"/hoastream/1/reverb/send" : 0.0,
										"/hoastream/1/reverb/send/ramptime" : 20.0,
										"/hoastream/1/reverb/mute" : 0,
										"/hoastream/1/sorting" : "ACN",
										"/hoastream/1/norm" : "N3D",
										"/hoastream/1/convention" : "spat",
										"/hoastream/1/rotation/z" : 0.0,
										"/hoastream/1/rotation/y" : 0.0,
										"/hoastream/1/rotation/x" : 0.0,
										"/hoastream/1/focus/dsp/bypass" : 1,
										"/hoastream/1/focus/beam/1/selectivity" : 0.0,
										"/hoastream/1/focus/beam/1/azim" : 0.0,
										"/hoastream/1/focus/beam/1/elev" : 0.0,
										"/hoastream/1/lock" : 0,
										"/d2m/1/name" : "ToMaster 1",
										"/d2m/1/color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
										"/d2m/1/numinputs" : 17,
										"/d2m/1/visible" : 1,
										"/d2m/1/trim" : 0.0,
										"/d2m/1/dynamics/attack" : 10.010000228881836,
										"/d2m/1/dynamics/release" : 30.0,
										"/d2m/1/dynamics/lookahead" : 0.0,
										"/d2m/1/dynamics/compressor/threshold" : -30.0,
										"/d2m/1/dynamics/compressor/ratio" : 1.0,
										"/d2m/1/dynamics/expander/threshold" : -60.0,
										"/d2m/1/dynamics/expander/ratio" : 1.0,
										"/d2m/1/dynamics/makeup" : 0.0,
										"/d2m/1/dynamics/link" : "multi mono",
										"/d2m/1/dynamics/bypass" : 1,
										"/d2m/1/dynamics/grid/visible" : 1,
										"/d2m/1/dynamics/curve/fill" : 1,
										"/d2m/1/dynamics/background/color" : [ 1.0, 1.0, 1.0, 0.0 ],
										"/d2m/1/dynamics/foreground/color" : [ 0.501960813999176, 0.0, 0.501960813999176, 0.600000023841858 ],
										"/d2m/1/dynamics/curve/color" : [ 0.400000005960464, 0.0, 0.400000005960464, 1.0 ],
										"/d2m/1/dynamics/curve/thickness" : 1.0,
										"/d2m/1/dynamics/grid/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 1.0 ],
										"/d2m/1/dynamics/title" : "ToMaster 1",
										"/d2m/1/equalizer/bypass" : 1,
										"/d2m/1/equalizer/gain" : 0.0,
										"/d2m/1/equalizer/filter/1/active" : 0,
										"/d2m/1/equalizer/filter/1/freq" : 50.0,
										"/d2m/1/equalizer/filter/1/order" : 2,
										"/d2m/1/equalizer/filter/2/active" : 1,
										"/d2m/1/equalizer/filter/2/freq" : 100.0,
										"/d2m/1/equalizer/filter/2/gain" : 0.0,
										"/d2m/1/equalizer/filter/2/q" : 1.0,
										"/d2m/1/equalizer/filter/3/active" : 1,
										"/d2m/1/equalizer/filter/3/freq" : 500.0,
										"/d2m/1/equalizer/filter/3/gain" : 0.0,
										"/d2m/1/equalizer/filter/3/q" : 1.0,
										"/d2m/1/equalizer/filter/4/active" : 1,
										"/d2m/1/equalizer/filter/4/freq" : 1000.0,
										"/d2m/1/equalizer/filter/4/gain" : 0.0,
										"/d2m/1/equalizer/filter/4/q" : 1.0,
										"/d2m/1/equalizer/filter/5/active" : 1,
										"/d2m/1/equalizer/filter/5/freq" : 2000.0,
										"/d2m/1/equalizer/filter/5/gain" : 0.0,
										"/d2m/1/equalizer/filter/5/q" : 1.0,
										"/d2m/1/equalizer/filter/6/active" : 1,
										"/d2m/1/equalizer/filter/6/freq" : 5000.0,
										"/d2m/1/equalizer/filter/6/gain" : 0.0,
										"/d2m/1/equalizer/filter/6/q" : 1.0,
										"/d2m/1/equalizer/filter/7/active" : 1,
										"/d2m/1/equalizer/filter/7/freq" : 10000.0,
										"/d2m/1/equalizer/filter/7/gain" : 0.0,
										"/d2m/1/equalizer/filter/7/q" : 1.0,
										"/d2m/1/equalizer/filter/8/active" : 0,
										"/d2m/1/equalizer/filter/8/freq" : 16000.0,
										"/d2m/1/equalizer/filter/8/order" : 2,
										"/d2m/1/gain" : 0.0,
										"/d2m/1/gain/ramptime" : 20.0,
										"/d2m/1/mute" : 0,
										"/d2m/1/annotation" : "",
										"/d2m/1/levels/input/visible" : 1,
										"/d2m/1/levels/input/post" : 0,
										"/d2m/1/levels/input/mode" : "peak",
										"/d2m/1/levels/output/visible" : 1,
										"/d2m/1/levels/output/post" : 1,
										"/d2m/1/levels/output/mode" : "peak",
										"/d2m/1/solo" : 0,
										"/d2m/1/routing/input/1/adc" : 33,
										"/d2m/1/routing/input/2/adc" : 34,
										"/d2m/1/routing/input/3/adc" : 35,
										"/d2m/1/routing/input/4/adc" : 36,
										"/d2m/1/routing/input/5/adc" : 37,
										"/d2m/1/routing/input/6/adc" : 38,
										"/d2m/1/routing/input/7/adc" : 39,
										"/d2m/1/routing/input/8/adc" : 40,
										"/d2m/1/routing/input/9/adc" : 41,
										"/d2m/1/routing/input/10/adc" : 42,
										"/d2m/1/routing/input/11/adc" : 43,
										"/d2m/1/routing/input/12/adc" : 44,
										"/d2m/1/routing/input/13/adc" : 45,
										"/d2m/1/routing/input/14/adc" : 46,
										"/d2m/1/routing/input/15/adc" : 47,
										"/d2m/1/routing/input/16/adc" : 48,
										"/d2m/1/routing/input/17/adc" : 49,
										"/d2m/1/lfe/send" : 0.0,
										"/d2m/1/lfe/mute" : 1,
										"/d2m/1/delay" : 0.0,
										"/d2m/1/delay/bypass" : 0,
										"/d2m/1/lock" : 0,
										"/d2m/1/routing/output/1/master" : 1,
										"/d2m/1/routing/output/2/master" : 2,
										"/d2m/1/routing/output/3/master" : 3,
										"/d2m/1/routing/output/4/master" : 4,
										"/d2m/1/routing/output/5/master" : 5,
										"/d2m/1/routing/output/6/master" : 6,
										"/d2m/1/routing/output/7/master" : 7,
										"/d2m/1/routing/output/8/master" : 8,
										"/d2m/1/routing/output/9/master" : 9,
										"/d2m/1/routing/output/10/master" : 10,
										"/d2m/1/routing/output/11/master" : 11,
										"/d2m/1/routing/output/12/master" : 12,
										"/d2m/1/routing/output/13/master" : 13,
										"/d2m/1/routing/output/14/master" : 14,
										"/d2m/1/routing/output/15/master" : 15,
										"/d2m/1/routing/output/16/master" : 16,
										"/d2m/1/routing/output/17/master" : 17,
										"/group/indices" : [ 1, 2, 3, 4, 5 ],
										"/group/1/name" : "all tracks",
										"/group/1/affects/mute" : 1,
										"/group/1/affects/solo" : 1,
										"/group/1/affects/send" : 1,
										"/group/1/affects/bussend" : 1,
										"/group/1/affects/position" : 1,
										"/group/1/track" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ],
										"/group/1/hoastream" : 1,
										"/group/1/d2m" : 1,
										"/group/1/active" : 0,
										"/group/2/name" : "all mono",
										"/group/2/affects/mute" : 1,
										"/group/2/affects/solo" : 1,
										"/group/2/affects/send" : 1,
										"/group/2/affects/bussend" : 1,
										"/group/2/affects/position" : 1,
										"/group/2/track" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ],
										"/group/2/active" : 0,
										"/group/3/name" : ">> HOA Bus 1",
										"/group/3/affects/mute" : 1,
										"/group/3/affects/solo" : 1,
										"/group/3/affects/send" : 1,
										"/group/3/affects/bussend" : 1,
										"/group/3/affects/position" : 1,
										"/group/3/track" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ],
										"/group/3/hoastream" : 1,
										"/group/3/active" : 0,
										"/group/4/name" : "Mono 1-8",
										"/group/4/affects/mute" : 1,
										"/group/4/affects/solo" : 1,
										"/group/4/affects/send" : 1,
										"/group/4/affects/bussend" : 1,
										"/group/4/affects/position" : 1,
										"/group/4/track" : [ 1, 2, 3, 4, 5, 6, 7, 8 ],
										"/group/4/active" : 0,
										"/group/5/name" : "Mono 9-16",
										"/group/5/affects/mute" : 1,
										"/group/5/affects/solo" : 1,
										"/group/5/affects/send" : 1,
										"/group/5/affects/bussend" : 1,
										"/group/5/affects/position" : 1,
										"/group/5/track" : [ 9, 10, 11, 12, 13, 14, 15, 16 ],
										"/group/5/active" : 0,
										"/internals" : 8,
										"/viewer/layout" : "single",
										"/viewer/background/color" : [ 0.709803938865662, 0.709803938865662, 0.709803938865662, 1.0 ],
										"/viewer/backgroundimage/file" : "",
										"/viewer/backgroundimage/visible" : 1,
										"/viewer/backgroundimage/opacity" : 1.0,
										"/viewer/backgroundimage/scale" : 100.0,
										"/viewer/backgroundimage/angle" : 0.0,
										"/viewer/backgroundimage/offset/xy" : [ 0.0, 0.0 ],
										"/viewer/backgroundimage/quality" : "medium",
										"/viewer/display/zoom" : 26.353599548339844,
										"/viewer/display/offset/xyz" : [ 0.0, 0.0, 0.0 ],
										"/viewer/display/zoom/lock" : 0,
										"/viewer/axis/visible" : 1,
										"/viewer/axis/label/visible" : 1,
										"/viewer/axis/origin/visible" : 1,
										"/viewer/axis/color" : [ 1.0, 1.0, 1.0, 1.0 ],
										"/viewer/axis/thickness" : 2.0,
										"/viewer/grid/visible" : 1,
										"/viewer/grid/mode" : "circular",
										"/viewer/grid/spacing" : 1.0,
										"/viewer/grid/line/number" : 30,
										"/viewer/grid/angulardivisions/number" : 8,
										"/viewer/grid/angulardivisions/visible" : 1,
										"/viewer/grid/dashed" : 0,
										"/viewer/grid/color" : [ 1.0, 1.0, 1.0, 0.501960813999176 ],
										"/viewer/grid/thickness" : 1.0,
										"/viewer/grid/unitcircle/visible" : 1,
										"/viewer/grid/unitcircle/color" : [ 0.501960813999176, 0.501960813999176, 0.501960813999176, 0.239215686917305 ],
										"/viewer/grid/unitcircle/radius" : 1.0,
										"/viewer/legend/visible" : 1,
										"/viewer/legend/color" : [ 1.0, 1.0, 1.0, 1.0 ],
										"/viewer/legend/unit" : "meters",
										"/viewer/emphasis/source" : 1,
										"/viewer/emphasis/speaker" : 0,
										"/viewer/emphasis/microphone" : 0,
										"/viewer/settings/visible" : 0,
										"/viewer/settings/editable" : 1,
										"/viewer/area/number" : 0,
										"/viewer/anchor/number" : 0,
										"/usurp" : 0,
										"/window/title" : "Panoramix - default *",
										"/window/visible" : 0,
										"/window/moveable" : 1,
										"/window/resizable" : 1,
										"/window/enable" : 1,
										"/window/bounds" : [ 416, 144, 1445, 958 ],
										"/window/background/color" : [ 0.82745099067688, 0.82745099067688, 0.82745099067688, 1.0 ],
										"/window/opaque" : 1,
										"/window/titlebar" : 1,
										"/window/fullscreen" : 0,
										"/window/minimise" : 0,
										"/window/scale" : 80.0,
										"/window/rendering/engine" : "",
										"/window/rendering/fps/visible" : 0,
										"/window/floating" : 0,
										"/window/hidesondeactivate" : 0,
										"/window/buttons/close" : 1,
										"/window/buttons/minimise" : 1,
										"/window/buttons/maximise" : 1,
										"embed" : 1
									}
,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 317.0, 364.0, 641.0, 22.0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"text" : "spat5.panoramix @inlets 49 @outlets 35 @embed 1 @initwith \"/preset/load Project:sessions/default.txt, /window/close\""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 326.5, 126.0, 261.5, 126.0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"midpoints" : [ 326.5, 426.0, 43.0, 426.0, 43.0, 284.0, 71.5, 284.0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"midpoints" : [ 405.5, 612.0, 71.5, 612.0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"midpoints" : [ 337.5, 297.0, 348.0, 297.0, 348.0, 351.0, 326.5, 351.0 ],
									"source" : [ "obj-17", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 1 ],
									"order" : 0,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"order" : 1,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 1 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-2", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"midpoints" : [ 71.5, 466.0, 326.5, 466.0 ],
									"order" : 0,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"order" : 1,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 37.0, 701.0, 436.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p panoramix"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1099.5, 493.0, 129.0, 22.0 ],
					"text" : "folder Project:sessions"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Ableton Sans Light",
					"fontsize" : 24.0,
					"id" : "obj-79",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 231.0, 235.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.5, 226.0, 534.0, 35.0 ],
					"text" : "[ audio process v0.5.2 ]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 168.0, 201.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.5, 133.0, 533.5, 21.0 ],
					"style" : "rnbodefault",
					"text" : "Contact technique : 06.25.89.21.92"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"fontsize" : 14.0,
					"id" : "obj-77",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 145.0, 235.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.5, 108.0, 533.5, 23.0 ],
					"style" : "rnbodefault",
					"text" : "Artiste Julie Rousse, RIM Martin Saëz"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 268.0, 150.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 327.0, 440.5, 102.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "HOA Player",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 268.0, 150.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 56.5, 440.5, 102.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "WATER player",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 68.0, 303.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 58.0, 478.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[15]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "newbuffer",
					"varname" : "live.text[12]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 68.0, 352.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 58.0, 544.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[10]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "open",
					"varname" : "live.text[7]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 68.0, 336.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 58.0, 522.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[11]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "stop",
					"varname" : "live.text[8]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 68.0, 319.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 58.0, 500.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[13]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "start",
					"varname" : "live.text[10]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 378.0, 375.5, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 327.0, 544.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[7]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "open",
					"varname" : "live.text[4]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 378.0, 328.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 327.0, 478.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[6]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "newbuffer",
					"varname" : "live.text[3]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 378.0, 360.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 327.0, 522.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[2]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "stop",
					"varname" : "live.text[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 378.0, 344.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 327.0, 500.5, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[1]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "start",
					"varname" : "live.text[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 980.0, 454.0, 32.0, 22.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 648.0, 568.5, 70.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 150.0, 386.5, 73.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "Audio_thru",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "live.toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 621.0, 566.0, 25.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 125.0, 384.5, 25.0, 25.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_longname" : "live.toggle[1]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.toggle",
							"parameter_type" : 2
						}

					}
,
					"varname" : "live.toggle[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 815.0, 357.0, 640.0, 225.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 41.0, 32.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 41.0, 164.5, 279.0, 21.0 ],
									"style" : "rnbodefault",
									"text" : "dac~ : Focusrite red 4 pre >> Dante >> Console"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 41.0, 83.5, 68.0, 22.0 ],
									"text" : "mc.gate~ 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 5,
									"outlettype" : [ "multichannelsignal", "multichannelsignal", "", "", "" ],
									"patching_rect" : [ 41.0, 107.5, 166.0, 22.0 ],
									"text" : "mc.omx.peaklim~ @chans 17"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 41.0, 140.5, 277.0, 22.0 ],
									"text" : "mc.dac~ 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 90.0, 36.5, 277.0, 22.0 ],
									"text" : "mc.adc~ 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "rnbodefault",
								"default" : 								{
									"accentcolor" : [ 0.343034118413925, 0.506230533123016, 0.86220508813858, 1.0 ],
									"bgcolor" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"autogradient" : 0.0,
										"color" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
										"color1" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
										"color2" : [ 0.263682, 0.004541, 0.038797, 1.0 ],
										"proportion" : 0.39,
										"type" : "color"
									}
,
									"color" : [ 0.929412, 0.929412, 0.352941, 1.0 ],
									"elementcolor" : [ 0.357540726661682, 0.515565991401672, 0.861786782741547, 1.0 ],
									"fontname" : [ "Lato" ],
									"fontsize" : [ 12.0 ],
									"stripecolor" : [ 0.258338063955307, 0.352425158023834, 0.511919498443604, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 621.0, 825.0, 76.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Audio_thru"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1117.0, 408.0, 54.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 488.0, 294.0, 54.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "Update",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "live.button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1100.0, 411.0, 15.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 544.0, 297.0, 15.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_longname" : "live.button",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.button",
							"parameter_type" : 2
						}

					}
,
					"varname" : "live.button"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 547.5, 703.0, 38.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 81.5, 386.5, 38.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "REC",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"activebgoncolor" : [ 1.0, 0.345098039215686, 0.298039215686275, 1.0 ],
					"id" : "obj-32",
					"maxclass" : "live.toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 524.0, 701.0, 25.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 57.0, 384.5, 25.0, 25.0 ],
					"saved_attribute_attributes" : 					{
						"activebgoncolor" : 						{
							"expression" : "themecolor.live_active_automation"
						}
,
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_longname" : "live.toggle",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.toggle",
							"parameter_type" : 2
						}

					}
,
					"varname" : "live.toggle"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 872.0, 548.0, 105.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 58.0, 294.0, 105.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "Panoramix session",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 859.5, 677.0, 61.5, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 498.0, 389.0, 65.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "Open",
					"varname" : "live.text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 137.0, 558.0, 217.0, 50.0 ],
					"style" : "rnbodefault",
					"text" : "/ • 8 + 8 mono tracks\n|  • 16 HOA 3rd order encoded tracks\n\\ • 17 direct to master tracks"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 37.0, 849.0, 279.0, 21.0 ],
					"style" : "rnbodefault",
					"text" : "dac~ : Focusrite red 4 pre >> Dante >> Console"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 10.0,
					"id" : "obj-38",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 212.0, 65.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.5, 176.0, 65.0, 18.0 ],
					"style" : "rnbodefault",
					"text" : "spat5 v5.2.9"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 10.0,
					"id" : "obj-37",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 192.0, 113.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.5, 156.0, 113.0, 18.0 ],
					"style" : "rnbodefault",
					"text" : "Build with Max 8.5.2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Bold",
					"fontsize" : 48.0,
					"id" : "obj-33",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 814.0, 82.0, 359.0, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.5, 42.0, 533.5, 64.0 ],
					"text" : "Métamorphoses"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 37.0, 130.0, 229.0, 22.0 ],
					"text" : "spat5.osc.route /eau /pack /hoa /directout"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 37.0, 106.0, 246.0, 22.0 ],
					"text" : "spat5.osc.route /metamorph/player"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 37.0, 34.0, 97.0, 22.0 ],
					"text" : "udpreceive 9999"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.52156862745098, 0.250980392156863, 1.0 ],
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 378.0, 427.0, 128.0, 22.0 ],
					"text" : "metamorph.player.hoa"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 980.0, 408.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.71419882774353, 0.714335978031158, 0.714175820350647, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.71419882774353, 0.714335978031158, 0.714175820350647, 1.0 ],
					"bgfillcolor_color1" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
					"bgfillcolor_color2" : [ 0.263682, 0.004541, 0.038797, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"id" : "obj-10",
					"items" : [ "default.txt", ",", "template.txt", ",", "test avec espaces.txt" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 980.0, 547.0, 192.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 58.0, 316.0, 505.0, 23.0 ],
					"style" : "rnbolight"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-14",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "spat5.osc.receive.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 814.0, 613.0, 227.0, 32.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 58.0, 345.0, 505.0, 35.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1066.5, 629.0, 155.0, 22.0 ],
					"text" : "conformpath slash absolute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1066.5, 701.0, 88.0, 22.0 ],
					"text" : "/preset/load $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 144.0, 232.0, 1368.0, 597.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "multichannelsignal" ],
									"patching_rect" : [ 132.0, 425.0, 98.0, 22.0 ],
									"text" : "mc.separate~ 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "multichannelsignal" ],
									"patching_rect" : [ 53.0, 333.5, 98.0, 22.0 ],
									"text" : "mc.separate~ 17"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 836.0, 485.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 902.0, 485.0, 55.0, 22.0 ],
									"text" : "/open $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 836.0, 539.0, 196.0, 21.0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"text" : "spat5.sfrecord~ @channels 16 @mc 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 866.0, 485.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 866.0, 434.0, 55.0, 22.0 ],
									"text" : "t b s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 836.0, 121.0, 44.0, 22.0 ],
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 866.0, 407.0, 155.0, 22.0 ],
									"text" : "conformpath slash absolute"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 888.0, 296.5, 75.0, 22.0 ],
									"text" : "set $1 $2 $3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 866.0, 333.5, 49.0, 22.0 ],
									"text" : "append"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "bang" ],
									"patching_rect" : [ 866.0, 170.0, 32.0, 22.0 ],
									"text" : "t b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 866.0, 216.0, 32.0, 22.0 ],
									"text" : "date"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 900.0, 216.0, 31.0, 22.0 ],
									"text" : "time"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "list", "list", "int" ],
									"patching_rect" : [ 866.0, 264.5, 40.0, 22.0 ],
									"text" : "date"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 866.0, 383.0, 441.0, 22.0 ],
									"text" : "sprintf Project:audio_files/exports/hoa-%02d-%02d-%04d-%02d-%02d-%02d.wav"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 273.0, 43.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 53.0, 43.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 273.0, 485.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 339.0, 485.0, 55.0, 22.0 ],
									"text" : "/open $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-73",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 273.0, 539.0, 196.0, 21.0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"text" : "spat5.sfrecord~ @channels 17 @mc 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 303.0, 485.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 303.0, 434.0, 55.0, 22.0 ],
									"text" : "t b s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-69",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 273.0, 121.0, 44.0, 22.0 ],
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 303.0, 407.0, 155.0, 22.0 ],
									"text" : "conformpath slash absolute"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 325.0, 296.5, 75.0, 22.0 ],
									"text" : "set $1 $2 $3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 303.0, 333.5, 49.0, 22.0 ],
									"text" : "append"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "bang" ],
									"patching_rect" : [ 303.0, 170.0, 32.0, 22.0 ],
									"text" : "t b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 303.0, 216.0, 32.0, 22.0 ],
									"text" : "date"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 337.0, 216.0, 31.0, 22.0 ],
									"text" : "time"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "list", "list", "int" ],
									"patching_rect" : [ 303.0, 264.5, 40.0, 22.0 ],
									"text" : "date"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 303.0, 383.0, 458.0, 22.0 ],
									"text" : "sprintf Project:audio_files/exports/master-%02d-%02d-%04d-%02d-%02d-%02d.wav"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"midpoints" : [ 282.5, 106.0, 845.5, 106.0 ],
									"order" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-69", 0 ],
									"order" : 1,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"midpoints" : [ 141.5, 525.0, 845.5, 525.0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"midpoints" : [ 911.5, 525.0, 845.5, 525.0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"midpoints" : [ 875.5, 525.0, 845.5, 525.0 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-34", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"source" : [ "obj-35", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"source" : [ "obj-40", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"source" : [ "obj-42", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-44", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-47", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"midpoints" : [ 62.5, 525.0, 282.5, 525.0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"source" : [ "obj-51", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-51", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-69", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"source" : [ "obj-69", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"source" : [ "obj-70", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"midpoints" : [ 312.5, 525.0, 282.5, 525.0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"midpoints" : [ 348.5, 525.0, 282.5, 525.0 ],
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"source" : [ "obj-81", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 454.0, 825.0, 89.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p recorder"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1171.5, 670.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 106.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 46.0, 400.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 46.0, 44.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 102.5, 265.0, 43.0, 22.0 ],
									"text" : "% 360"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 46.0, 318.5, 46.0, 22.0 ],
									"text" : "pack i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 46.0, 167.0, 29.5, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 102.5, 241.0, 30.0, 22.0 ],
									"text" : "* 45"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 102.5, 217.0, 29.5, 22.0 ],
									"text" : "- 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 46.0, 129.0, 75.0, 22.0 ],
									"text" : "counter 1 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "int" ],
									"patching_rect" : [ 46.0, 105.0, 40.0, 22.0 ],
									"text" : "uzi 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 46.0, 342.5, 165.0, 22.0 ],
									"text" : "sprintf /track/%d/aed %d 0 10"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"source" : [ "obj-29", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 1 ],
									"source" : [ "obj-33", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1171.5, 701.0, 95.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p sourceInCircle"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 37.0, 825.0, 277.0, 22.0 ],
					"text" : "mc.dac~ 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 943.5, 701.0, 121.0, 22.0 ],
					"text" : "/window/openorclose"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 859.5, 701.0, 82.0, 22.0 ],
					"style" : "default",
					"text" : "/window/open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 37.0, 572.0, 90.0, 22.0 ],
					"text" : "mc.combine~ 3"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.52156862745098, 0.250980392156863, 1.0 ],
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 37.0, 427.0, 128.0, 22.0 ],
					"text" : "metamorph.player.eau"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 980.0, 493.0, 118.0, 22.0 ],
					"text" : "setsymbol default.txt"
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.419607843137255, 0.537254901960784, 0.611764705882353, 1.0 ],
					"bordercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"id" : "obj-70",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1206.0, 97.0, 38.0, 38.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.5, 278.0, 533.5, 142.0 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 1.0, 0.47843137254902, 0.188235294117647, 1.0 ],
					"id" : "obj-75",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1206.0, 97.0, 38.0, 38.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.5, 426.5, 128.0, 259.0 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 1.0, 0.47843137254902, 0.188235294117647, 1.0 ],
					"id" : "obj-76",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1206.0, 97.0, 38.0, 38.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 314.0, 426.5, 128.0, 259.0 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 1.0, 0.47843137254902, 0.188235294117647, 1.0 ],
					"id" : "obj-29",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1206.0, 97.0, 38.0, 38.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 449.0, 426.5, 128.0, 259.0 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.419607843137255, 0.537254901960784, 0.611764705882353, 1.0 ],
					"bordercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"id" : "obj-80",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1206.0, 97.0, 38.0, 38.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.5, 693.0, 533.5, 34.0 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 1.0, 0.47843137254902, 0.188235294117647, 1.0 ],
					"id" : "obj-28",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1206.0, 97.0, 38.0, 38.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 178.5, 426.5, 128.0, 259.0 ],
					"proportion" : 0.5
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"source" : [ "obj-10", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 1 ],
					"midpoints" : [ 387.5, 474.0, 82.0, 474.0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"midpoints" : [ 496.5, 451.0, 518.0, 451.0, 518.0, 424.0, 532.5, 424.0 ],
					"source" : [ "obj-11", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"midpoints" : [ 716.0, 394.0, 726.0, 394.0, 726.0, 418.0, 673.5, 418.0 ],
					"source" : [ "obj-12", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"midpoints" : [ 716.0, 346.0, 735.0, 346.0, 735.0, 421.0, 673.5, 421.0 ],
					"source" : [ "obj-13", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"midpoints" : [ 823.5, 646.0, 823.5, 646.0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"midpoints" : [ 716.0, 379.0, 726.0, 379.0, 726.0, 418.0, 673.5, 418.0 ],
					"source" : [ "obj-15", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"midpoints" : [ 716.0, 364.0, 735.0, 364.0, 735.0, 421.0, 673.5, 421.0 ],
					"source" : [ "obj-18", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 2 ],
					"midpoints" : [ 673.5, 502.0, 117.5, 502.0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 808.5, 451.0, 830.0, 451.0, 830.0, 424.0, 844.5, 424.0 ],
					"source" : [ "obj-19", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"order" : 1,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"order" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 151.5, 168.0, 363.0, 168.0, 363.0, 414.0, 387.5, 414.0 ],
					"source" : [ "obj-24", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"midpoints" : [ 204.0, 163.0, 651.0, 163.0, 651.0, 414.0, 673.5, 414.0 ],
					"source" : [ "obj-24", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 99.0, 173.0, 202.75, 173.0 ],
					"source" : [ "obj-24", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 430.0, 364.0, 449.0, 364.0, 449.0, 421.0, 387.5, 421.0 ],
					"source" : [ "obj-25", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"midpoints" : [ 869.0, 760.0, 823.5, 760.0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"midpoints" : [ 1002.5, 484.0, 1109.0, 484.0 ],
					"source" : [ "obj-30", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 430.0, 379.0, 440.0, 379.0, 440.0, 418.0, 387.5, 418.0 ],
					"source" : [ "obj-31", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 1 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"midpoints" : [ 1181.0, 760.0, 823.5, 760.0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"midpoints" : [ 1293.0, 760.0, 823.5, 760.0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"midpoints" : [ 533.5, 683.0, 46.5, 683.0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"midpoints" : [ 953.0, 760.0, 823.5, 760.0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 430.0, 346.0, 449.0, 346.0, 449.0, 421.0, 387.5, 421.0 ],
					"source" : [ "obj-41", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 430.0, 394.0, 440.0, 394.0, 440.0, 418.0, 387.5, 418.0 ],
					"source" : [ "obj-45", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"midpoints" : [ 630.5, 615.0, 533.5, 615.0 ],
					"order" : 1,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"order" : 0,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 120.0, 370.0, 46.5, 370.0 ],
					"source" : [ "obj-57", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 120.0, 355.0, 129.0, 355.0, 129.0, 370.0, 46.5, 370.0 ],
					"source" : [ "obj-58", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 120.0, 322.0, 141.0, 322.0, 141.0, 412.0, 46.5, 412.0 ],
					"source" : [ "obj-61", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 120.0, 307.0, 141.0, 307.0, 141.0, 412.0, 46.5, 412.0 ],
					"source" : [ "obj-66", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 276.25, 318.0, 285.0, 318.0, 285.0, 285.0, 202.75, 285.0 ],
					"source" : [ "obj-67", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"midpoints" : [ 1109.0, 525.0, 990.0, 525.0, 990.0, 543.0, 989.5, 543.0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 276.25, 370.0, 202.75, 370.0 ],
					"source" : [ "obj-84", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-85", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"midpoints" : [ 1076.0, 760.0, 823.5, 760.0 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 276.25, 355.0, 285.25, 355.0, 285.25, 370.0, 202.75, 370.0 ],
					"source" : [ "obj-88", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 276.25, 322.0, 297.25, 322.0, 297.25, 412.0, 202.75, 412.0 ],
					"source" : [ "obj-92", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 233.75, 414.0, 202.75, 414.0 ],
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 202.75, 461.0, 46.5, 461.0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-12" : [ "live.text[19]", "live.text", 0 ],
			"obj-13" : [ "live.text[20]", "live.text", 0 ],
			"obj-14::obj-14" : [ "live.text[4]", "live.text", 0 ],
			"obj-14::obj-2" : [ "live.text[5]", "live.text", 0 ],
			"obj-14::obj-20" : [ "live.button[2]", "live.button", 0 ],
			"obj-14::obj-53" : [ "OSCOutPort[2]", "OutPort", 0 ],
			"obj-14::obj-8" : [ "live.numbox[2]", "live.numbox", 0 ],
			"obj-15" : [ "live.text[21]", "live.text", 0 ],
			"obj-18" : [ "live.text[22]", "live.text", 0 ],
			"obj-25" : [ "live.text[1]", "live.text", 0 ],
			"obj-31" : [ "live.text[2]", "live.text", 0 ],
			"obj-32" : [ "live.toggle", "live.toggle", 0 ],
			"obj-41" : [ "live.text[6]", "live.text", 0 ],
			"obj-45" : [ "live.text[7]", "live.text", 0 ],
			"obj-48" : [ "live.toggle[2]", "live.toggle", 0 ],
			"obj-54" : [ "live.toggle[1]", "live.toggle", 0 ],
			"obj-56" : [ "live.text", "live.text", 0 ],
			"obj-57" : [ "live.text[10]", "live.text", 0 ],
			"obj-58" : [ "live.text[11]", "live.text", 0 ],
			"obj-60" : [ "live.button", "live.button", 0 ],
			"obj-61" : [ "live.text[13]", "live.text", 0 ],
			"obj-66" : [ "live.text[15]", "live.text", 0 ],
			"obj-67" : [ "live.text[27]", "live.text", 0 ],
			"obj-74" : [ "live.text[23]", "live.text", 0 ],
			"obj-84" : [ "live.text[18]", "live.text", 0 ],
			"obj-88" : [ "live.text[29]", "live.text", 0 ],
			"obj-9" : [ "live.text[24]", "live.text", 0 ],
			"obj-92" : [ "live.text[31]", "live.text", 0 ],
			"parameterbanks" : 			{

			}
,
			"parameter_overrides" : 			{
				"obj-14::obj-14" : 				{
					"parameter_longname" : "live.text[4]"
				}
,
				"obj-14::obj-2" : 				{
					"parameter_longname" : "live.text[5]"
				}
,
				"obj-14::obj-20" : 				{
					"parameter_longname" : "live.button[2]"
				}
,
				"obj-14::obj-53" : 				{
					"parameter_longname" : "OSCOutPort[2]"
				}
,
				"obj-14::obj-8" : 				{
					"parameter_longname" : "live.numbox[2]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "metamorph.fades.maxpat",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "metamorph.player.directout.maxpat",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "metamorph.player.eau.maxpat",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "metamorph.player.hoa.maxpat",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "metamorph.player.pack.maxpat",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "randomFile.maxpat",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "spat5.folder.infos.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.osc.print.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.osc.receive.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/spat5_5.2.7/patchers",
				"patcherrelativepath" : "../../../../../../Users/martinsaez/Documents/Max 8/Packages/spat5_5.2.7/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "spat5.osc.route.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.osc.speedlim.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.panoramix.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.panoramix~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.sfrecord~.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "rnbodefault",
				"default" : 				{
					"accentcolor" : [ 0.343034118413925, 0.506230533123016, 0.86220508813858, 1.0 ],
					"bgcolor" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
					"bgfillcolor" : 					{
						"angle" : 270.0,
						"autogradient" : 0.0,
						"color" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
						"color1" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
						"color2" : [ 0.263682, 0.004541, 0.038797, 1.0 ],
						"proportion" : 0.39,
						"type" : "color"
					}
,
					"color" : [ 0.929412, 0.929412, 0.352941, 1.0 ],
					"elementcolor" : [ 0.357540726661682, 0.515565991401672, 0.861786782741547, 1.0 ],
					"fontname" : [ "Lato" ],
					"fontsize" : [ 12.0 ],
					"stripecolor" : [ 0.258338063955307, 0.352425158023834, 0.511919498443604, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rnbohighcontrast",
				"default" : 				{
					"accentcolor" : [ 0.666666666666667, 0.666666666666667, 0.666666666666667, 1.0 ],
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor" : 					{
						"angle" : 270.0,
						"autogradient" : 0.0,
						"color" : [ 0.0, 0.0, 0.0, 1.0 ],
						"color1" : [ 0.090196078431373, 0.090196078431373, 0.090196078431373, 1.0 ],
						"color2" : [ 0.156862745098039, 0.168627450980392, 0.164705882352941, 1.0 ],
						"proportion" : 0.5,
						"type" : "color"
					}
,
					"clearcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"color" : [ 1.0, 0.874509803921569, 0.141176470588235, 1.0 ],
					"editing_bgcolor" : [ 0.258823529411765, 0.258823529411765, 0.258823529411765, 1.0 ],
					"elementcolor" : [ 0.223386004567146, 0.254748553037643, 0.998085916042328, 1.0 ],
					"fontsize" : [ 13.0 ],
					"locked_bgcolor" : [ 0.258823529411765, 0.258823529411765, 0.258823529411765, 1.0 ],
					"selectioncolor" : [ 0.301960784313725, 0.694117647058824, 0.949019607843137, 1.0 ],
					"stripecolor" : [ 0.258823529411765, 0.258823529411765, 0.258823529411765, 1.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"textcolor_inverse" : [ 1.0, 1.0, 1.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rnbolight",
				"default" : 				{
					"accentcolor" : [ 0.443137254901961, 0.505882352941176, 0.556862745098039, 1.0 ],
					"bgcolor" : [ 0.796078431372549, 0.862745098039216, 0.925490196078431, 1.0 ],
					"bgfillcolor" : 					{
						"angle" : 270.0,
						"autogradient" : 0.0,
						"color" : [ 0.835294117647059, 0.901960784313726, 0.964705882352941, 1.0 ],
						"color1" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
						"color2" : [ 0.263682, 0.004541, 0.038797, 1.0 ],
						"proportion" : 0.39,
						"type" : "color"
					}
,
					"clearcolor" : [ 0.898039, 0.898039, 0.898039, 1.0 ],
					"color" : [ 0.815686274509804, 0.509803921568627, 0.262745098039216, 1.0 ],
					"editing_bgcolor" : [ 0.898039, 0.898039, 0.898039, 1.0 ],
					"elementcolor" : [ 0.337254901960784, 0.384313725490196, 0.462745098039216, 1.0 ],
					"fontname" : [ "Lato" ],
					"locked_bgcolor" : [ 0.898039, 0.898039, 0.898039, 1.0 ],
					"stripecolor" : [ 0.309803921568627, 0.698039215686274, 0.764705882352941, 1.0 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rnbomonokai",
				"default" : 				{
					"accentcolor" : [ 0.501960784313725, 0.501960784313725, 0.501960784313725, 1.0 ],
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor" : 					{
						"angle" : 270.0,
						"autogradient" : 0.0,
						"color" : [ 0.0, 0.0, 0.0, 1.0 ],
						"color1" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
						"color2" : [ 0.263682, 0.004541, 0.038797, 1.0 ],
						"proportion" : 0.39,
						"type" : "color"
					}
,
					"clearcolor" : [ 0.976470588235294, 0.96078431372549, 0.917647058823529, 1.0 ],
					"color" : [ 0.611764705882353, 0.125490196078431, 0.776470588235294, 1.0 ],
					"editing_bgcolor" : [ 0.976470588235294, 0.96078431372549, 0.917647058823529, 1.0 ],
					"elementcolor" : [ 0.749019607843137, 0.83921568627451, 1.0, 1.0 ],
					"fontname" : [ "Lato" ],
					"locked_bgcolor" : [ 0.976470588235294, 0.96078431372549, 0.917647058823529, 1.0 ],
					"stripecolor" : [ 0.796078431372549, 0.207843137254902, 1.0, 1.0 ],
					"textcolor" : [ 0.129412, 0.129412, 0.129412, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
