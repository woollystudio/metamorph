{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 2,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 932.0, 193.0, 714.0, 760.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Ableton Sans Medium",
					"id" : "obj-47",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 535.0, 73.0, 153.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 535.0, 73.0, 153.0, 21.0 ],
					"text" : "v0.1.2 - Trajectories",
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Medium",
					"fontsize" : 36.0,
					"id" : "obj-45",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 391.0, 21.0, 297.0, 50.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 391.0, 21.0, 297.0, 50.0 ],
					"text" : "Metamorphoses",
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.905882352941176, 0.905882352941176, 0.905882352941176, 1.0 ],
					"bgmode" : 2,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-43",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "metamorph.trajectories.session.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 22.0, 21.0, 332.0, 77.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.0, 21.0, 332.0, 77.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 8 ],
					"bgcolor" : [ 0.905882352941176, 0.905882352941176, 0.905882352941176, 1.0 ],
					"bgmode" : 2,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "metamorph.trajectory.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 523.0, 419.0, 165.0, 317.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 523.0, 419.0, 165.0, 317.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 7 ],
					"bgcolor" : [ 0.905882352941176, 0.905882352941176, 0.905882352941176, 1.0 ],
					"bgmode" : 2,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "metamorph.trajectory.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 356.0, 419.0, 165.0, 317.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 356.0, 419.0, 165.0, 317.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 6 ],
					"bgcolor" : [ 0.905882352941176, 0.905882352941176, 0.905882352941176, 1.0 ],
					"bgmode" : 2,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-8",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "metamorph.trajectory.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 189.0, 419.0, 165.0, 317.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 189.0, 419.0, 165.0, 317.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 5 ],
					"bgcolor" : [ 0.905882352941176, 0.905882352941176, 0.905882352941176, 1.0 ],
					"bgmode" : 2,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-9",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "metamorph.trajectory.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 22.0, 419.0, 165.0, 317.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.0, 419.0, 165.0, 317.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgcolor" : [ 0.905882352941176, 0.905882352941176, 0.905882352941176, 1.0 ],
					"bgmode" : 2,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "metamorph.trajectory.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 523.0, 100.0, 165.0, 317.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 523.0, 100.0, 165.0, 317.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 3 ],
					"bgcolor" : [ 0.905882352941176, 0.905882352941176, 0.905882352941176, 1.0 ],
					"bgmode" : 2,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-3",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "metamorph.trajectory.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 356.0, 100.0, 165.0, 317.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 356.0, 100.0, 165.0, 317.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.905882352941176, 0.905882352941176, 0.905882352941176, 1.0 ],
					"bgmode" : 2,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "metamorph.trajectory.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 189.0, 100.0, 165.0, 317.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 189.0, 100.0, 165.0, 317.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 1 ],
					"bgcolor" : [ 0.905882352941176, 0.905882352941176, 0.905882352941176, 1.0 ],
					"bgmode" : 2,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "metamorph.trajectory.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 22.0, 100.0, 165.0, 317.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.0, 100.0, 165.0, 317.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 22.0, 828.0, 125.0, 22.0 ],
					"text" : "spat5.osc.speedlim 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 22.0, 852.0, 254.0, 22.0 ],
					"text" : "spat5.osc.udpsend @ip 127.0.0.1 @port 4002"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-1::obj-119" : [ "live.numbox[25]", "live.numbox", 0 ],
			"obj-1::obj-140" : [ "live.menu[24]", "live.menu", 0 ],
			"obj-1::obj-46" : [ "live.dial[75]", "scale y", 0 ],
			"obj-1::obj-47" : [ "live.dial[16]", "scale x", 0 ],
			"obj-1::obj-59" : [ "live.toggle", "live.toggle", 0 ],
			"obj-1::obj-78" : [ "live.dial[60]", "scale z", 0 ],
			"obj-1::obj-79" : [ "live.dial[31]", "offset z", 0 ],
			"obj-1::obj-8" : [ "umenu", "umenu", 0 ],
			"obj-1::obj-85" : [ "live.menu[5]", "live.menu", 0 ],
			"obj-1::obj-89" : [ "live.dial[15]", "angle x", 0 ],
			"obj-1::obj-90" : [ "live.dial[17]", "angle y", 0 ],
			"obj-1::obj-91" : [ "live.dial[3]", "offset x", 0 ],
			"obj-1::obj-92" : [ "live.dial[2]", "offset y", 0 ],
			"obj-1::obj-93" : [ "live.dial[18]", "angle z", 0 ],
			"obj-2::obj-119" : [ "live.numbox[5]", "live.numbox", 0 ],
			"obj-2::obj-140" : [ "live.menu[23]", "live.menu", 0 ],
			"obj-2::obj-46" : [ "live.dial[56]", "scale y", 0 ],
			"obj-2::obj-47" : [ "live.dial[59]", "scale x", 0 ],
			"obj-2::obj-59" : [ "live.toggle[1]", "live.toggle", 0 ],
			"obj-2::obj-78" : [ "live.dial[89]", "scale z", 0 ],
			"obj-2::obj-79" : [ "live.dial[7]", "offset z", 0 ],
			"obj-2::obj-8" : [ "umenu[1]", "umenu", 0 ],
			"obj-2::obj-85" : [ "live.menu[6]", "live.menu", 0 ],
			"obj-2::obj-89" : [ "live.dial[4]", "angle x", 0 ],
			"obj-2::obj-90" : [ "live.dial[5]", "angle y", 0 ],
			"obj-2::obj-91" : [ "live.dial[21]", "offset x", 0 ],
			"obj-2::obj-92" : [ "live.dial[20]", "offset y", 0 ],
			"obj-2::obj-93" : [ "live.dial[6]", "angle z", 0 ],
			"obj-3::obj-119" : [ "live.numbox[23]", "live.numbox", 0 ],
			"obj-3::obj-140" : [ "live.menu[22]", "live.menu", 0 ],
			"obj-3::obj-46" : [ "live.dial[87]", "scale y", 0 ],
			"obj-3::obj-47" : [ "live.dial[88]", "scale x", 0 ],
			"obj-3::obj-59" : [ "live.toggle[2]", "live.toggle", 0 ],
			"obj-3::obj-78" : [ "live.dial[86]", "scale z", 0 ],
			"obj-3::obj-79" : [ "live.dial[30]", "offset z", 0 ],
			"obj-3::obj-8" : [ "umenu[2]", "umenu", 0 ],
			"obj-3::obj-85" : [ "live.menu[9]", "live.menu", 0 ],
			"obj-3::obj-89" : [ "live.dial[23]", "angle x", 0 ],
			"obj-3::obj-90" : [ "live.dial[24]", "angle y", 0 ],
			"obj-3::obj-91" : [ "live.dial[26]", "offset x", 0 ],
			"obj-3::obj-92" : [ "live.dial[25]", "offset y", 0 ],
			"obj-3::obj-93" : [ "live.dial[29]", "angle z", 0 ],
			"obj-4::obj-119" : [ "live.numbox[22]", "live.numbox", 0 ],
			"obj-4::obj-140" : [ "live.menu[21]", "live.menu", 0 ],
			"obj-4::obj-46" : [ "live.dial[27]", "scale y", 0 ],
			"obj-4::obj-47" : [ "live.dial[28]", "scale x", 0 ],
			"obj-4::obj-59" : [ "live.toggle[3]", "live.toggle", 0 ],
			"obj-4::obj-78" : [ "live.dial[85]", "scale z", 0 ],
			"obj-4::obj-79" : [ "live.dial[32]", "offset z", 0 ],
			"obj-4::obj-8" : [ "umenu[3]", "umenu", 0 ],
			"obj-4::obj-85" : [ "live.menu[11]", "live.menu", 0 ],
			"obj-4::obj-89" : [ "live.dial[37]", "angle x", 0 ],
			"obj-4::obj-90" : [ "live.dial[38]", "angle y", 0 ],
			"obj-4::obj-91" : [ "live.dial[36]", "offset x", 0 ],
			"obj-4::obj-92" : [ "live.dial[40]", "offset y", 0 ],
			"obj-4::obj-93" : [ "live.dial[39]", "angle z", 0 ],
			"obj-5::obj-119" : [ "live.numbox[19]", "live.numbox", 0 ],
			"obj-5::obj-140" : [ "live.menu[18]", "live.menu", 0 ],
			"obj-5::obj-46" : [ "live.dial[12]", "scale y", 0 ],
			"obj-5::obj-47" : [ "live.dial[13]", "scale x", 0 ],
			"obj-5::obj-59" : [ "live.toggle[7]", "live.toggle", 0 ],
			"obj-5::obj-78" : [ "live.dial[14]", "scale z", 0 ],
			"obj-5::obj-79" : [ "live.dial[77]", "offset z", 0 ],
			"obj-5::obj-8" : [ "umenu[7]", "umenu", 0 ],
			"obj-5::obj-85" : [ "live.menu[19]", "live.menu", 0 ],
			"obj-5::obj-89" : [ "live.dial[68]", "angle x", 0 ],
			"obj-5::obj-90" : [ "live.dial[76]", "angle y", 0 ],
			"obj-5::obj-91" : [ "live.dial[72]", "offset x", 0 ],
			"obj-5::obj-92" : [ "live.dial[71]", "offset y", 0 ],
			"obj-5::obj-93" : [ "live.dial[70]", "angle z", 0 ],
			"obj-6::obj-119" : [ "live.numbox[16]", "live.numbox", 0 ],
			"obj-6::obj-140" : [ "live.menu[16]", "live.menu", 0 ],
			"obj-6::obj-46" : [ "live.dial[49]", "scale y", 0 ],
			"obj-6::obj-47" : [ "live.dial[73]", "scale x", 0 ],
			"obj-6::obj-59" : [ "live.toggle[6]", "live.toggle", 0 ],
			"obj-6::obj-78" : [ "live.dial[48]", "scale z", 0 ],
			"obj-6::obj-79" : [ "live.dial[63]", "offset z", 0 ],
			"obj-6::obj-8" : [ "umenu[6]", "umenu", 0 ],
			"obj-6::obj-85" : [ "live.menu[17]", "live.menu", 0 ],
			"obj-6::obj-89" : [ "live.dial[67]", "angle x", 0 ],
			"obj-6::obj-90" : [ "live.dial[79]", "angle y", 0 ],
			"obj-6::obj-91" : [ "live.dial[66]", "offset x", 0 ],
			"obj-6::obj-92" : [ "live.dial[65]", "offset y", 0 ],
			"obj-6::obj-93" : [ "live.dial[64]", "angle z", 0 ],
			"obj-8::obj-119" : [ "live.numbox[20]", "live.numbox", 0 ],
			"obj-8::obj-140" : [ "live.menu[15]", "live.menu", 0 ],
			"obj-8::obj-46" : [ "live.dial[52]", "scale y", 0 ],
			"obj-8::obj-47" : [ "live.dial[81]", "scale x", 0 ],
			"obj-8::obj-59" : [ "live.toggle[5]", "live.toggle", 0 ],
			"obj-8::obj-78" : [ "live.dial[74]", "scale z", 0 ],
			"obj-8::obj-79" : [ "live.dial[55]", "offset z", 0 ],
			"obj-8::obj-8" : [ "umenu[5]", "umenu", 0 ],
			"obj-8::obj-85" : [ "live.menu[14]", "live.menu", 0 ],
			"obj-8::obj-89" : [ "live.dial[80]", "angle x", 0 ],
			"obj-8::obj-90" : [ "live.dial[53]", "angle y", 0 ],
			"obj-8::obj-91" : [ "live.dial[57]", "offset x", 0 ],
			"obj-8::obj-92" : [ "live.dial[58]", "offset y", 0 ],
			"obj-8::obj-93" : [ "live.dial[54]", "angle z", 0 ],
			"obj-9::obj-119" : [ "live.numbox[21]", "live.numbox", 0 ],
			"obj-9::obj-140" : [ "live.menu[20]", "live.menu", 0 ],
			"obj-9::obj-46" : [ "live.dial[83]", "scale y", 0 ],
			"obj-9::obj-47" : [ "live.dial[84]", "scale x", 0 ],
			"obj-9::obj-59" : [ "live.toggle[4]", "live.toggle", 0 ],
			"obj-9::obj-78" : [ "live.dial[82]", "scale z", 0 ],
			"obj-9::obj-79" : [ "live.dial[41]", "offset z", 0 ],
			"obj-9::obj-8" : [ "umenu[4]", "umenu", 0 ],
			"obj-9::obj-85" : [ "live.menu[13]", "live.menu", 0 ],
			"obj-9::obj-89" : [ "live.dial[45]", "angle x", 0 ],
			"obj-9::obj-90" : [ "live.dial[46]", "angle y", 0 ],
			"obj-9::obj-91" : [ "live.dial[44]", "offset x", 0 ],
			"obj-9::obj-92" : [ "live.dial[43]", "offset y", 0 ],
			"obj-9::obj-93" : [ "live.dial[47]", "angle z", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"parameter_overrides" : 			{
				"obj-1::obj-119" : 				{
					"parameter_longname" : "live.numbox[25]"
				}
,
				"obj-1::obj-140" : 				{
					"parameter_longname" : "live.menu[24]"
				}
,
				"obj-1::obj-46" : 				{
					"parameter_longname" : "live.dial[75]"
				}
,
				"obj-1::obj-47" : 				{
					"parameter_longname" : "live.dial[16]"
				}
,
				"obj-1::obj-59" : 				{
					"parameter_longname" : "live.toggle"
				}
,
				"obj-1::obj-78" : 				{
					"parameter_longname" : "live.dial[60]"
				}
,
				"obj-1::obj-79" : 				{
					"parameter_longname" : "live.dial[31]"
				}
,
				"obj-1::obj-8" : 				{
					"parameter_invisible" : 0,
					"parameter_modmode" : 0,
					"parameter_unitstyle" : 10
				}
,
				"obj-1::obj-85" : 				{
					"parameter_longname" : "live.menu[5]"
				}
,
				"obj-1::obj-89" : 				{
					"parameter_longname" : "live.dial[15]"
				}
,
				"obj-1::obj-90" : 				{
					"parameter_longname" : "live.dial[17]"
				}
,
				"obj-1::obj-91" : 				{
					"parameter_longname" : "live.dial[3]"
				}
,
				"obj-1::obj-92" : 				{
					"parameter_longname" : "live.dial[2]"
				}
,
				"obj-1::obj-93" : 				{
					"parameter_longname" : "live.dial[18]"
				}
,
				"obj-2::obj-119" : 				{
					"parameter_longname" : "live.numbox[5]"
				}
,
				"obj-2::obj-140" : 				{
					"parameter_longname" : "live.menu[23]"
				}
,
				"obj-2::obj-46" : 				{
					"parameter_longname" : "live.dial[56]"
				}
,
				"obj-2::obj-47" : 				{
					"parameter_longname" : "live.dial[59]"
				}
,
				"obj-2::obj-59" : 				{
					"parameter_longname" : "live.toggle[1]"
				}
,
				"obj-2::obj-78" : 				{
					"parameter_longname" : "live.dial[89]"
				}
,
				"obj-2::obj-79" : 				{
					"parameter_longname" : "live.dial[7]"
				}
,
				"obj-2::obj-8" : 				{
					"parameter_invisible" : 0,
					"parameter_modmode" : 0,
					"parameter_unitstyle" : 10
				}
,
				"obj-2::obj-85" : 				{
					"parameter_longname" : "live.menu[6]"
				}
,
				"obj-2::obj-89" : 				{
					"parameter_longname" : "live.dial[4]"
				}
,
				"obj-2::obj-90" : 				{
					"parameter_longname" : "live.dial[5]"
				}
,
				"obj-2::obj-91" : 				{
					"parameter_longname" : "live.dial[21]"
				}
,
				"obj-2::obj-92" : 				{
					"parameter_longname" : "live.dial[20]"
				}
,
				"obj-2::obj-93" : 				{
					"parameter_longname" : "live.dial[6]"
				}
,
				"obj-3::obj-119" : 				{
					"parameter_longname" : "live.numbox[23]"
				}
,
				"obj-3::obj-140" : 				{
					"parameter_longname" : "live.menu[22]"
				}
,
				"obj-3::obj-46" : 				{
					"parameter_longname" : "live.dial[87]"
				}
,
				"obj-3::obj-47" : 				{
					"parameter_longname" : "live.dial[88]"
				}
,
				"obj-3::obj-59" : 				{
					"parameter_longname" : "live.toggle[2]"
				}
,
				"obj-3::obj-78" : 				{
					"parameter_longname" : "live.dial[86]"
				}
,
				"obj-3::obj-79" : 				{
					"parameter_longname" : "live.dial[30]"
				}
,
				"obj-3::obj-8" : 				{
					"parameter_invisible" : 0,
					"parameter_modmode" : 0,
					"parameter_unitstyle" : 10
				}
,
				"obj-3::obj-85" : 				{
					"parameter_longname" : "live.menu[9]"
				}
,
				"obj-3::obj-89" : 				{
					"parameter_longname" : "live.dial[23]"
				}
,
				"obj-3::obj-90" : 				{
					"parameter_longname" : "live.dial[24]"
				}
,
				"obj-3::obj-91" : 				{
					"parameter_longname" : "live.dial[26]"
				}
,
				"obj-3::obj-92" : 				{
					"parameter_longname" : "live.dial[25]"
				}
,
				"obj-3::obj-93" : 				{
					"parameter_longname" : "live.dial[29]"
				}
,
				"obj-4::obj-119" : 				{
					"parameter_longname" : "live.numbox[22]"
				}
,
				"obj-4::obj-140" : 				{
					"parameter_longname" : "live.menu[21]"
				}
,
				"obj-4::obj-46" : 				{
					"parameter_longname" : "live.dial[27]"
				}
,
				"obj-4::obj-47" : 				{
					"parameter_longname" : "live.dial[28]"
				}
,
				"obj-4::obj-59" : 				{
					"parameter_longname" : "live.toggle[3]"
				}
,
				"obj-4::obj-78" : 				{
					"parameter_longname" : "live.dial[85]"
				}
,
				"obj-4::obj-79" : 				{
					"parameter_longname" : "live.dial[32]"
				}
,
				"obj-4::obj-8" : 				{
					"parameter_invisible" : 0,
					"parameter_modmode" : 0,
					"parameter_unitstyle" : 10
				}
,
				"obj-4::obj-85" : 				{
					"parameter_longname" : "live.menu[11]"
				}
,
				"obj-4::obj-89" : 				{
					"parameter_longname" : "live.dial[37]"
				}
,
				"obj-4::obj-90" : 				{
					"parameter_longname" : "live.dial[38]"
				}
,
				"obj-4::obj-91" : 				{
					"parameter_longname" : "live.dial[36]"
				}
,
				"obj-4::obj-92" : 				{
					"parameter_longname" : "live.dial[40]"
				}
,
				"obj-4::obj-93" : 				{
					"parameter_longname" : "live.dial[39]"
				}
,
				"obj-5::obj-119" : 				{
					"parameter_longname" : "live.numbox[19]"
				}
,
				"obj-5::obj-140" : 				{
					"parameter_longname" : "live.menu[18]"
				}
,
				"obj-5::obj-46" : 				{
					"parameter_longname" : "live.dial[12]"
				}
,
				"obj-5::obj-47" : 				{
					"parameter_longname" : "live.dial[13]"
				}
,
				"obj-5::obj-59" : 				{
					"parameter_longname" : "live.toggle[7]"
				}
,
				"obj-5::obj-78" : 				{
					"parameter_longname" : "live.dial[14]"
				}
,
				"obj-5::obj-79" : 				{
					"parameter_longname" : "live.dial[77]"
				}
,
				"obj-5::obj-8" : 				{
					"parameter_invisible" : 0,
					"parameter_modmode" : 0,
					"parameter_unitstyle" : 10
				}
,
				"obj-5::obj-85" : 				{
					"parameter_longname" : "live.menu[19]"
				}
,
				"obj-5::obj-89" : 				{
					"parameter_longname" : "live.dial[68]"
				}
,
				"obj-5::obj-90" : 				{
					"parameter_longname" : "live.dial[76]"
				}
,
				"obj-5::obj-91" : 				{
					"parameter_longname" : "live.dial[72]"
				}
,
				"obj-5::obj-92" : 				{
					"parameter_longname" : "live.dial[71]"
				}
,
				"obj-5::obj-93" : 				{
					"parameter_longname" : "live.dial[70]"
				}
,
				"obj-6::obj-119" : 				{
					"parameter_longname" : "live.numbox[16]"
				}
,
				"obj-6::obj-140" : 				{
					"parameter_longname" : "live.menu[16]"
				}
,
				"obj-6::obj-46" : 				{
					"parameter_longname" : "live.dial[49]"
				}
,
				"obj-6::obj-47" : 				{
					"parameter_longname" : "live.dial[73]"
				}
,
				"obj-6::obj-59" : 				{
					"parameter_longname" : "live.toggle[6]"
				}
,
				"obj-6::obj-78" : 				{
					"parameter_longname" : "live.dial[48]"
				}
,
				"obj-6::obj-79" : 				{
					"parameter_longname" : "live.dial[63]"
				}
,
				"obj-6::obj-8" : 				{
					"parameter_invisible" : 0,
					"parameter_modmode" : 0,
					"parameter_unitstyle" : 10
				}
,
				"obj-6::obj-85" : 				{
					"parameter_longname" : "live.menu[17]"
				}
,
				"obj-6::obj-89" : 				{
					"parameter_longname" : "live.dial[67]"
				}
,
				"obj-6::obj-90" : 				{
					"parameter_longname" : "live.dial[79]"
				}
,
				"obj-6::obj-91" : 				{
					"parameter_longname" : "live.dial[66]"
				}
,
				"obj-6::obj-92" : 				{
					"parameter_longname" : "live.dial[65]"
				}
,
				"obj-6::obj-93" : 				{
					"parameter_longname" : "live.dial[64]"
				}
,
				"obj-8::obj-119" : 				{
					"parameter_longname" : "live.numbox[20]"
				}
,
				"obj-8::obj-140" : 				{
					"parameter_longname" : "live.menu[15]"
				}
,
				"obj-8::obj-46" : 				{
					"parameter_longname" : "live.dial[52]"
				}
,
				"obj-8::obj-47" : 				{
					"parameter_longname" : "live.dial[81]"
				}
,
				"obj-8::obj-59" : 				{
					"parameter_longname" : "live.toggle[5]"
				}
,
				"obj-8::obj-78" : 				{
					"parameter_longname" : "live.dial[74]"
				}
,
				"obj-8::obj-79" : 				{
					"parameter_longname" : "live.dial[55]"
				}
,
				"obj-8::obj-8" : 				{
					"parameter_invisible" : 0,
					"parameter_modmode" : 0,
					"parameter_unitstyle" : 10
				}
,
				"obj-8::obj-85" : 				{
					"parameter_longname" : "live.menu[14]"
				}
,
				"obj-8::obj-89" : 				{
					"parameter_longname" : "live.dial[80]"
				}
,
				"obj-8::obj-90" : 				{
					"parameter_longname" : "live.dial[53]"
				}
,
				"obj-8::obj-91" : 				{
					"parameter_longname" : "live.dial[57]"
				}
,
				"obj-8::obj-92" : 				{
					"parameter_longname" : "live.dial[58]"
				}
,
				"obj-8::obj-93" : 				{
					"parameter_longname" : "live.dial[54]"
				}
,
				"obj-9::obj-119" : 				{
					"parameter_longname" : "live.numbox[21]"
				}
,
				"obj-9::obj-140" : 				{
					"parameter_longname" : "live.menu[20]"
				}
,
				"obj-9::obj-46" : 				{
					"parameter_longname" : "live.dial[83]"
				}
,
				"obj-9::obj-47" : 				{
					"parameter_longname" : "live.dial[84]"
				}
,
				"obj-9::obj-59" : 				{
					"parameter_longname" : "live.toggle[4]"
				}
,
				"obj-9::obj-78" : 				{
					"parameter_longname" : "live.dial[82]"
				}
,
				"obj-9::obj-79" : 				{
					"parameter_longname" : "live.dial[41]"
				}
,
				"obj-9::obj-8" : 				{
					"parameter_invisible" : 0,
					"parameter_modmode" : 0,
					"parameter_unitstyle" : 10
				}
,
				"obj-9::obj-85" : 				{
					"parameter_longname" : "live.menu[13]"
				}
,
				"obj-9::obj-89" : 				{
					"parameter_longname" : "live.dial[45]"
				}
,
				"obj-9::obj-90" : 				{
					"parameter_longname" : "live.dial[46]"
				}
,
				"obj-9::obj-91" : 				{
					"parameter_longname" : "live.dial[44]"
				}
,
				"obj-9::obj-92" : 				{
					"parameter_longname" : "live.dial[43]"
				}
,
				"obj-9::obj-93" : 				{
					"parameter_longname" : "live.dial[47]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "coordinates.js",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/code",
				"patcherrelativepath" : "../code",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "metamorph.trajectories.session.maxpat",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "metamorph.trajectory.maxpat",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "motifExport.js",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/code",
				"patcherrelativepath" : "../code",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "motifImport.js",
				"bootpath" : "/Volumes/SAMSUNG_T5/Archives/Rhône/metamorph/code",
				"patcherrelativepath" : "../code",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "spat5.osc.prepend.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.osc.route.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.osc.speedlim.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.osc.udpsend.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.osc.unslashify.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.rotate.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.scale.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat5.translate.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0
	}

}
