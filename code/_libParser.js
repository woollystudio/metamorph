autowatch = 1;

inlets = 1;
outlets = 1;

var fileName;
var data;
var loaded = false;

// Load data from fileName
function loadData(){
 	// Load data from .json file
 	var dataFile = new File(fileName, 'read');
  	dataFile.open();
  	var dataAsString = '{ "lib" \: ';
  	while (dataFile.position < dataFile.eof) {
    	dataAsString += dataFile.readline(32768);
  	}
  	dataAsString += '}'
  	dataFile.close();

  	data = JSON.parse(dataAsString);

  	// Format 'tags' keys to be arrays
  	for(var i=0; i < data.lib.length; i++){
  		data.lib[i].tags = data.lib[i].tags.split(', ');
  	}

  	loaded = true;
}

function updateData(){
	// Update data from fileName
	if(!loaded) return;
	loadData();
}

// Get files with a key
function newlib(key){
	// Check if data is loaded
	if(!loaded) return;

	// How many files I need?
	var howMany = 8;

	// Array of matched files
	var match = new Array(0);

	// .includes() workaround
	for(var i = 0; i < data.lib.length; i++)
		if (data.lib[i].tags.indexOf(key) !== -1)
			match.push(data.lib[i].name);

	// Clear the buffer
	outlet(0, 'clear');

	// Fill the buffer
	for(var i = 0; i < howMany; i++){
		if(match[i] != null)
			outlet(0, 'append ' + match[i]);
		else
			outlet(0, 'appendempty 0');
	}

	//post('buffer has been filled');
}

// Get library file path
function libPath(name){
	fileName = name;
	loadData();
}

// Example
function bang(){
	if(loaded){
		outlet(0, data.lib[0].name);
		for(var i=0; i < data.lib[0].tags.length; i++){
			outlet(0, data.lib[0].tags[i]);
		}
	}
	else
		post('libParser :: lib is not loaded')
}