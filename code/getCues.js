autowatch = 1;

inlets = 1;
outlets = 1;

var data;
var loaded = false;
var fileName;

function loadData() {
  //
  var dataFile = new File(fileName, 'read');
  dataFile.open();
  var dataAsString = '{ "json" \: ';
  while (dataFile.position < dataFile.eof) {
    dataAsString += dataFile.readline(32768);
  }
  dataAsString += '}'
  dataFile.close();
  data = JSON.parse(dataAsString);

  loaded = true;
}

function read(newName) {
  fileName = newName;
  loadData();
}

function getId(index) {
  if(!loaded) return;

  outlet(0, 'id', data.json[index].cue_id);
}

function getTimeTag(index) {
  if(!loaded) return;

  outlet(0, 'timeTag', data.json[index].time_tag);
}

function getVerticalPos(index) {
  if(!loaded) return;

  outlet(0, 'verticalPos', data.json[index].vertical_pos);
}

function getKeyword(index) {
  if(!loaded) return;

  var arraySize = data.json[index].keywords.length;
  var randomIndex = Math.floor(Math.random() * arraySize);
  var keyword = data.json[index].keywords[randomIndex];

  outlet(0, 'keyword', keyword);
}

function dump(index) {
  getId(index);
  getTimeTag(index);
  getVerticalPos(index);
  getKeyword(index);
}