autowatch = 1;

inlets = 1;
outlets = 1;

var data;
var loaded = false;
var dataScale = new Object();
var fileName;
var axisSetup = 0;  // 0: /xy
                    // 1: /xz
                    // 2: /yz

function loadData() {
  //
  var dataFile = new File(fileName, 'read');
  dataFile.open();
  var dataAsString = '{ "xy" \: ';
  while (dataFile.position < dataFile.eof) {
    dataAsString += dataFile.readline(32768);
  }
  dataAsString += '}'
  dataFile.close();
  data = JSON.parse(dataAsString);

  scale();

  loaded = true;
}

function scale() {
  //
  data.xmin = data.xmax = data.xy[0][0];
  data.ymin = data.ymax = data.xy[0][1];

  for (var i = 1; i < data.xy.length; i++) {
    //
    if (data.xy[i][0] < data.xmin) data.xmin = data.xy[i][0];
    if (data.xy[i][0] > data.xmax) data.xmax = data.xy[i][0];
    //
    if (data.xy[i][1] < data.ymin) data.ymin = data.xy[i][1];
    if (data.xy[i][1] > data.ymax) data.ymax = data.xy[i][1];
  }

  // Init the new scaled array
  dataScale.xy = new Array(1000);

  data.xratio = data.xmax - data.xmin;
  data.yratio = data.ymax - data.ymin;

  // Respect ratio
  if (data.xratio > data.yratio) data.ratio = data.xratio;
  else data.ratio = data.yratio;

  // Fill the scaled array
  for (var i = 0; i < dataScale.xy.length; i++) {
    //
    dataScale.xy[i] = new Array(2);
    //
    dataScale.xy[i][0] = (data.xy[i][0] - data.xmin) / data.ratio * 2 - 1 * (data.xratio / data.ratio);
    dataScale.xy[i][1] = (data.xy[i][1] - data.ymin) / data.ratio * -2 + 1 * (data.yratio / data.ratio);
  }
}

function index(idx) {
  if(!loaded) return;
  //
  var x = dataScale.xy[idx][0];
  var y = dataScale.xy[idx][1];
  //
  switch(axisSetup){
    case 0:
      outlet(0, '/xyz', x, y, 0);
      break;
    case 1:
      outlet(0, '/xyz', x, 0, y);
      break;
    case 2:
      outlet(0, '/xyz', 0, x, y);
      break;
  }

  // outlet(1, y);
  // outlet(0, 'bang');
}

function motif(newName) {
  fileName = newName;
  loadData();
}

function axis(axs){
  axisSetup = axs;
}

function bang() {
  // Update data
  if (data == null) loadData();
  // // Test, post all coordinates
  // for (var i = 0; i < data.xy.length; i++) {
  //  outlet(0, data.xy[i][0]);
  //  outlet(1, data.xy[i][1]);
  // }
  // outlet(0, 'bang');
}
