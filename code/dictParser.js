autowatch = 1;

var myDict;

function parseDict()
{
  var dataAsString = myDict.get("body");
  var parsedDict = new Dict("parsedDict");
  parsedDict.parse(dataAsString);
  outlet(0, parsedDict.name);
}

function anything()
{
	var a = arrayfromargs(messagename, arguments);
	myDict = new Dict(a);
	parseDict();
}