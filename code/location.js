autowatch = 1;

inlets = 1;
outlets = 1;

var data;
var loaded = false;
var fileName;

function loadData() {
  //
  var dataFile = new File(fileName, 'read');
  dataFile.open();
  var dataAsString = '{ "json" \: ';
  while (dataFile.position < dataFile.eof) {
    dataAsString += dataFile.readline(32768);
  }
  dataAsString += '}'
  dataFile.close();
  data = JSON.parse(dataAsString);

  loaded = true;
}

function read(newName) {
  fileName = newName;
  loadData();
}

function name(index) {
	//
	if(!loaded) return;
	else index = processIdx(index);
	//
	outlet(0, 'short', data.json[index].name.short);
	outlet(0, 'display', data.json[index].name.display);
}

function capital(index) {
	//
	if(!loaded) return;
	else index = processIdx(index);
	//
	outlet(0, 'name', 'short', data.json[index].capital.name.short);
	outlet(0, 'name', 'display', data.json[index].capital.name.display);
	outlet(0, 'gps', data.json[index].capital.gps[0], data.json[index].capital.gps[1]);
}

function processIdx(index) {
	if(index < 12) return index;
	else if(index == 24) return 0;
	else return (12 - (index - 12) - 1);
}