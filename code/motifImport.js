autowatch = 1;

inlets = 1;
outlets = 1;

var data;
var loaded = false;

function load(filePath) {
  //
  var dataFile = new File(filePath, 'read');
  dataFile.open();
  var dataAsString = '';
  while (dataFile.position < dataFile.eof) {
    dataAsString += dataFile.readline(32768);
  }
  dataAsString += '';
  dataFile.close();
  data = JSON.parse(dataAsString);

  // post(dataAsString);

  loaded = true;
}

function parse()
{
	if (!loaded) return;

	for(var i = 0; i < data.motif.length; i++)
	{
		outlet(0, '/motif/' + (i+1) + '/active', data.motif[i].active);
		outlet(0, '/motif/' + (i+1) + '/name', data.motif[i].name);
		outlet(0, '/motif/' + (i+1) + '/playDir', data.motif[i].playDir);
		outlet(0, '/motif/' + (i+1) + '/axis', data.motif[i].axis);
		outlet(0, '/motif/' + (i+1) + '/readingTime', data.motif[i].readingTime);
		outlet(0, '/motif/' + (i+1) + '/scaleX', data.motif[i].scaleX);
		outlet(0, '/motif/' + (i+1) + '/scaleY', data.motif[i].scaleY);
		outlet(0, '/motif/' + (i+1) + '/scaleZ', data.motif[i].scaleZ);
		outlet(0, '/motif/' + (i+1) + '/angleX', data.motif[i].angleX);
		outlet(0, '/motif/' + (i+1) + '/angleY', data.motif[i].angleY);
		outlet(0, '/motif/' + (i+1) + '/angleZ', data.motif[i].angleZ);
		outlet(0, '/motif/' + (i+1) + '/offsetX', data.motif[i].offsetX);
		outlet(0, '/motif/' + (i+1) + '/offsetY', data.motif[i].offsetY);
		outlet(0, '/motif/' + (i+1) + '/offsetZ', data.motif[i].offsetZ);
	}
}