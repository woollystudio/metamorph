autowatch = 1;

inlets = 1;
outlets = 0;

var data = new Object();

// How many data.motif
data.motif = new Array(8);

// Init data.motif Array and recursive objects
for(var track = 0; track < data.motif.length; track++)
{
	data.motif[track] = new Object();

	// ON/OFF
	data.motif[track].active = false;

	// Trajectory's name
	data.motif[track].name = 'null';

	// Playing mode
	data.motif[track].playDir = 0;	// 0: Inc
									// 1: Dec
									// 2: Loop

	// Axis mode
	data.motif[track].axis = 0;	// 0: /xy
                    			// 1: /xz
                    			// 2: /yz

	// Reading time 0 - 300s
	data.motif[track].readingTime = 0.0;

	// Scale xyz
	data.motif[track].scaleX = 0.0;
	data.motif[track].scaleY = 0.0;
	data.motif[track].scaleZ = 0.0;

	// Angle xyz
	data.motif[track].angleX = 0.0;
	data.motif[track].angleY = 0.0;
	data.motif[track].angleZ = 0.0;

	// Offset xyz
	data.motif[track].offsetX = 0.0;
	data.motif[track].offsetY = 0.0;
	data.motif[track].offsetZ = 0.0;
}

function set(track, param, value){
	data.motif[track][param] = value;
}

// Testing method
function bang()
{
	//post(data.motif[0].name + '.json');
	post(data);
}

function write(path)
{
	var jase = JSON.stringify(data, null, '\t');
	var dataFile = new File(path, 'write', 'TEXT');
	if (dataFile.isopen)
	{
		dataFile.writeline(jase);
		dataFile.close();

		// post("\nJSON Write",path);
	} else {
		post("\ncould not create json file: " + path);
	}
}