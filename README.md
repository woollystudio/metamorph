# Metamorphoses

[ obsolete :: need to be updated ]

 ![](media/metamorph-interface.png)

## Project architecture

### Audio files

```
audio_files/
audio_files/directout/
audio_files/exports/
audio_files/hoa/
audio_files/librairies/
audio_files/librairies/eau/
audio_files/librairies/general/
audio_files/paquets/
```

Put all media files in these directories.

### Code

```
code/
```

All non-Max algorithms are in there.

### Loudspeakers layouts

```
loudspeakers_layouts/
```

All loudspeakers layouts are archived here.

### Motifs

```
motifs/
motifs/exports/
motifs/json/
motifs/svg/
```

All spat trajectories are saved here. Convert vector-path .svg files to JSON with [Coördinator](https://spotify.github.io/coordinator/).

### Patchers

```
patchers/
```

All Cycling'Max patchers are in there.

### Panoramix sessions

```
sessions/
```

All Panoramix sessions must be saved here.

### Example :

```
├── Metamorphoses.maxproj
├── README.md
├── audio_files
│   ├── directout
│   │   ├── 001-directout.wav
│   │   ├── 002-directout.wav
│   │   ├── 003-directout.wav
│   │   ├── 004-directout.wav
│   │   ├── 005-directout.wav
│   │   ├── 006-directout.wav
│   │   ├── 007-directout.wav
│   │   ├── 008-directout.wav
│   │   └── 009-directout.wav
│   ├── exports
│   │   ├── hoa-02-16-2023-18-19-01.wav
│   │   ├── hoa-02-21-2023-17-16-27.wav
│   │   ├── master-02-16-2023-18-19-01.wav
│   │   └── master-02-21-2023-17-16-27.wav
│   ├── hoa
│   │   ├── 001-hoa.wav
│   │   └── 002-hoa.wav
│   ├── librairies
│   │   ├── eau
│   │   │   ├── 001-eau.wav
│   │   │   ├── 002-eau.wav
│   │   │   ├── 003-eau.wav
│   │   │   ├── 004-eau.wav
│   │   │   ├── 005-eau.wav
│   │   │   ├── 006-eau.wav
│   │   │   ├── 007-eau.wav
│   │   │   ├── 008-eau.wav
│   │   │   ├── 009-eau.wav
│   │   │   ├── 010-eau.wav
│   │   │   ├── 011-eau.wav
│   │   │   ├── 012-eau.wav
│   │   │   ├── 013-eau.wav
│   │   │   ├── 014-eau.wav
│   │   │   └── 015-eau.wav
│   │   └── general
│   │       ├── 120619-0135-rhone-nuit-grenouilles-1-1.wav
│   │       ├── 120619-0135-rhone-nuit-grenouilles-1-2.wav
│   │       ├── 120619-0358-rhone-nuit-grenouilles-2-1.wav
│   │       ├── 120619-0603-rhone-nuit-grenouilles-3-1.wav
│   │       ├── 180930-1722-02-rhone-BBB-moteur-vagues.wav
│   │       ├── 190418-VR003-03-electromagnetic-03.wav
│   │       ├── 190503-2314-VR0076-rhone-vallabregues-km261-night-frogs-05.wav
│   │       ├── 200226-1045-02-vallabregues-champs-solaire-ambiance-02-1.wav
│   │       ├── 200226-1045-02-vallabregues-champs-solaire-ambiance-02-2.wav
│   │       ├── 200226_1204-06-vallabregues-barage-debit-eleve-02-1.wav
│   │       ├── 200227_1046-03-vallabregues-pluie-sur-rhone-03-1.wav
│   │       ├── 200227_1107-06-vallabregues-pluie-sur-rhone-ST-06.wav
│   │       ├── 200612-rhone-stream-bass-2_3.wav
│   │       ├── 200612-rhone-stream-bass-mid-01-8trk_7.wav
│   │       ├── 200612-rhone-stream-hi-mid-02-8trk_6.wav
│   │       ├── 210910-016-midi-glacier-ascension-gros-moulin.wav
│   │       ├── 211216-020-PM-rarogne-eglise-cloches-1.wav
│   │       ├── 211216-022-PM-rarogne-eglise-cloches-2.wav
│   │       ├── 211216-024-PM-train-electromagnet-1.wav
│   │       ├── 220524-STMARIES-016-place-eglise-chants-gitans-flamenco-1.wav
│   │       ├── 220524-STMARIES-016-place-eglise-chants-gitans-flamenco.wav
│   │       └── index.json
│   └── paquets
│       ├── 001-PAQUET-1
│       │   ├── 01-200926-PM-vienne-PIEZO-110.wav
│       │   ├── 02-200708-wind-MBDelay-1_7.wav
│       │   ├── 03-200926-PM-vienne-PIEZO-109.wav
│       │   ├── 04-200926-PM-vienne-PIEZO-109.wav
│       │   ├── 05-200926-PM-vienne-PIEZO-106.wav
│       │   ├── 06-200708-electromagnetic-transpo-DynProc-3_001_5.wav
│       │   ├── 07-200707-pink-flamingo-DynProc-1_7.wav
│       │   ├── 08-200707-pink-flamingo-DynProc-1_8.wav
│       │   ├── 09-200708-wind-MBDelay-1_1.wav
│       │   ├── 10-200708-electromagnetic-transpo-DynProc-3_001_2.wav
│       │   ├── 11-200708-electromagnetic-transpo-DynProc-3_001_3.wav
│       │   ├── 12-200708-wind-MBDelay-1_4.wav
│       │   ├── 13-200926-PM-vienne-PIEZO-107.wav
│       │   ├── 14-200708-electromagnetic-transpo-DynProc-3_001_6.wav
│       │   ├── 15-200926-PM-vienne-PIEZO-110.wav
│       │   └── 16-200708-electromagnetic-transpo-DynProc-3_001_8.wav
│       └── 002-PAQUET-2
│           ├── 01-004_1.wav
│           ├── 02-004_2.wav
│           ├── 03-004_3.wav
│           ├── 04-004_4.wav
│           ├── 05-004_5.wav
│           ├── 06-004_6.wav
│           ├── 07-004_7.wav
│           ├── 08-004_8.wav
│           ├── 09-004_9.wav
│           ├── 10-007_1.wav
│           ├── 11-007_2.wav
│           ├── 12-007_3.wav
│           ├── 13-007_4.wav
│           ├── 14-007_5.wav
│           ├── 15-007_6.wav
│           └── 16-007_7.wav
├── code
│   ├── coordinates.js
│   ├── libParser.js
│   ├── motifExport.js
│   └── motifImport.js
├── motifs
│   ├── exports
│   │   ├── banjo.json
│   │   ├── ceci-est-un-test.json
│   │   └── test.json
│   ├── json
│   │   ├── equilateral.json
│   │   ├── example.json
│   │   └── passage-leger.json
│   └── svg
│       ├── equilateral.svg
│       ├── example.svg
│       └── passage-leger.svg
├── openactions.txt
├── patchers
│   ├── main.maxpat
│   ├── metamorph.player.directout.maxpat
│   ├── metamorph.player.hoa.maxpat
│   ├── metamorph.player.mono.maxpat
│   ├── randomFile.maxpat
│   ├── trajectories-example.maxpat
│   ├── trajectories-sessions.maxpat
│   └── trajectories.maxpat
└── sessions
    ├── default.txt
    └── template.txt

```

## Nomenclature :: format

All audio files are 44.1KHz / 24bits.

```
audio_files/directout/###-tags.wav              |   17ch
audio_files/hoa/###-tags.wav                    |   16ch
audio_files/librairies/eau/###-tags.wav         |   1ch
audio_files/librairies/general/name.wav         |   1ch
audio_files/paquets/###-name/trackid-name.wav    |   1ch
```

All audio_files/librairies/general/ files are indexed by the index.json array (must be updated by hand with [tableconvert](https://tableconvert.com/excel-to-json) for example).

``` json
[
    {
        "name": "name.wav",
        "tags": "tag_1, tag_2, tag_3",
        "motifs": "null"
    }
]
```

## Audio routing

Link : [routing.pdf](media/routing.pdf)

---

Martin Saëz @ [woollystudio](https://woollystud.io)